//
//  ModelManager.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 16.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import CoreData

/// This Class provides the Core Data functionality, stores important data and transfer data to the communicator. It is the model of this application
class ModelManager {
    
    /// Contains an instance of this class to make it available for other classes. It is "static" because it should be instantiated before any other class should need the data or the functions of this class.
    static let sharedInstance = ModelManager()
    
    // MARK: CoreData-based Variables
    
    /// Contains an array of messages saved in CoreData (the message history)
    var history = [DataMessage]()
    
    /// Contains an array with just one object which hold the app settings stored in Core Data
    var dataService = [DataService]()
    
    /// Contains the persistence store. It is necessary to get access to the Core Data storage
    let persistentContainer : NSPersistentContainer = {
        let container = NSPersistentContainer(name: "VoiceLED_SVA")
        container.loadPersistentStores {
            (storeDescription, error) in
            
            if let error = error {
                let nserror = error as NSError
                fatalError("Fehler: \(nserror.localizedDescription)")
            }
        }
        
        return container
    } ()
    
    // MARK: Matrix error handling
    
    /// Contains all signed in matrix error observers
    private var errorObserverArray = [IErrorObserver]()
    
    /// Contains the current matrix error code
    private var _error = 0
    
    /// This is the variable that returns the error code if it is called and if the value is set the observers get notified
    var error : Int {
        
        set {
            
            _error = newValue
            notifyErrorObservers()
            
        }
        
        get {
            
            return _error
            
        }
        
    }
    
    // MARK: TextView
    
    /// Stores the current text input
    var currentTextInput = "" // TODO safe it into CoreData if the App will crash or stopped by the user?
    
    // These are the text message examples the user can pick
    
    /// Example message 1
    let textMessageSuggestion1 = "Immatrikulation"
    
    /// Example message 2
    let textMessageSuggestion2 = "Studium"
    
    /// Example message 3
    let textMessageSuggestion3 = "Vorlesung"
    
    /// Example message 4
    let textMessageSuggestion4 = "Labor"
    
    /// Example message 5
    let textMessageSuggestion5 = "DHBW"
    
    // MARK: FreeView
    
    /// Stores the current text input name
    var freeInputName = ""
    
    /// Stores the current text input
    var freeInput = ""
    
    // MARK: SettingsView
    
    /// If there is no loopVelocity value stored in Core Data this value will be stored there
    let initialLoopVelocity = 2.0
    
    /// Stores the current loop velocity
    var loopVelocity: Double?
    
    
    /// If there is no auto send setting stored in Core Data this value will be stored there
    let initialAutoSend = false
    
    
    // MARK: BoardStateView
    
    /// The matrix width
    let COLS = 32
    
    /// The matrix height
    let ROWS = 7
    
    /// Was a message sent since the current connection was established?
    var messageSentSinceConnected = false
    
    /// Contains all signed in state observers
    private var stateObserverArray = [IStateObserver]()
    
    /// Contains the current connection state
    private var _connectionState = "disconnected"
    
    /// This is the variable that returns the current connection state if it is called and if the value is set the observers get notified
    var connectionState : String {
        
        set {
            
            _connectionState = newValue
            
            // It should be executed in the main thread and this variable will be set in a bluetooth thread
            DispatchQueue.main.async { () -> Void in
                
                self.notifyStateObservers()
                
            }
            
            
        }
        
        get {
            
            return _connectionState
            
        }
        
    }
    
    /// Contains the current temperature
    private var _temperature = 0
    
    /// This is the variable that returns the current temperature if it is called and if the value is set the observers get notified
    var temperature : Int {
        
        set {
            
            _temperature = newValue
            notifyStateObservers()
            
        }
        
        get {
            
            return _temperature
            
        }
        
    }
    
    // MARK: Bluetooth communication
    
    /// Contains the communicator class to interact with the LED matrix
    let communicator : Communicator?
    
    // MARK: Functions
    
    /// Is called if the static instance variable of this class will be instantiate. It is private so no other class can create another instance of this class
    private init() {
        
        // Hold the controller instance which communicate with the LED matrix
        communicator = Communicator.sharedInstance
        
        // To be sure that there is a data service object in Core Data that stores the application settings
        createDataService()
        
        // Update the stored message history to the same history stored in Core Data
        refreshHistory()
        
        // Update the stored settings to the same settings stored in Core Data
        refreshDataService()
        
        // Set the loop velocity to the value stored in Core Data
        loopVelocity = dataService[0].loopVelocity
        
    }
    
    /// Creates a data service object in Core Data if there is not an object like this
    func createDataService() {
        
        if dataService.isEmpty{
            
            let entity = NSEntityDescription.entity(forEntityName: "DataService", in: persistentContainer.viewContext)
            let managedObject = NSManagedObject(entity: entity!, insertInto: persistentContainer.viewContext)
            
            managedObject.setValue(initialLoopVelocity, forKey: "loopVelocity")
            managedObject.setValue(initialAutoSend, forKey: "autoSend")
            
            saveContext()
            
            if let tmpdataService = loadDataService(){
                dataService = tmpdataService
            }
            
        }
        
    }
    
    /// Creates a new text message, save it in Core Data, updates the message history stored in this class instance and send the message using the controller
    func create(TextMessage message: String) {
        
        // Creates the message in Core Data
        let entity = NSEntityDescription.entity(forEntityName: "DataMessage", in: persistentContainer.viewContext)
        let managedObject = NSManagedObject(entity: entity!, insertInto: persistentContainer.viewContext)
        
        managedObject.setValue(message, forKey: "messageName")
        managedObject.setValue(MessageConverter.encode(textMessageToBinaryString: message).encodedMessage, forKey: "messageContent")
        managedObject.setValue(NSDate(), forKey: "timestamp")
        managedObject.setValue("Text", forKey: "messageType")
        
        saveContext()
        
        // Update the stored message history to the same history stored in Core Data
        refreshHistory()
        
        // Send the message
        communicator?.send(textMessage: message, andLoopVelocity: Int(loopVelocity ?? initialLoopVelocity))
        
    }
    
    /// Creates a new free message, save it in Core Data, updates the message history stored in this class instance and send the message using the controller
    func createFreeMessage(withName name: String, andMessage message: String) {
        
        // Createsthe message in Core Data
        let entity = NSEntityDescription.entity(forEntityName: "DataMessage", in: persistentContainer.viewContext)
        let managedObject = NSManagedObject(entity: entity!, insertInto: persistentContainer.viewContext)
        
        managedObject.setValue(name, forKey: "messageName")
        managedObject.setValue(message, forKey: "messageContent")
        managedObject.setValue(NSDate(), forKey: "timestamp")
        managedObject.setValue("Free", forKey: "messageType")
        
        saveContext()
        
        // Update the stored message history to the same history stored in Core Data
        refreshHistory()
        
        // Send the message
        communicator?.send(freeMessage: freeInput, andLoopVelocity: Int(loopVelocity ?? initialLoopVelocity))
        
    }
    
    /// Saves new or edited data into Core Data
    func saveContext() {
        
        let context = persistentContainer.viewContext
        
        if context.hasChanges {
            
            do {
                
                try context.save()
                
            } catch {
                
                let error = error as NSError
                fatalError("Fehler: \(error.localizedDescription)")
                
            }
            
        }
        
    }
    
    /// Returns an array of all DataMessage objects (sent messages) stored in the model
    func loadDataMessages() -> [DataMessage]? {
        
        let request : NSFetchRequest<DataMessage> = DataMessage.fetchRequest()
        do {
            
            let dataMessage = try persistentContainer.viewContext.fetch(request)
            return dataMessage
            
        } catch {
            
            print(error.localizedDescription)
            
        }
        
        return nil
        
    }
    
    /// Returns the first DataService object and not the whole Array because there is just one
    func loadDataService() -> [DataService]? {
    
        let request : NSFetchRequest<DataService> = DataService.fetchRequest()
        do {
            
            let tmpdataService = try persistentContainer.viewContext.fetch(request)
            return tmpdataService
            
        } catch {
            
            print(error.localizedDescription)
            
        }
    
        return nil
        
    }
    
    /// Gets an array of sent messages stored in the model
    func refreshHistory() {
        
        if let tmphistory = loadDataMessages() {
            
            history = tmphistory
            
        }
        
    }
    
    /// Gets an array of the object stored in the model containing the application settings
    func refreshDataService() {
        
        if let tmpdataService = loadDataService() {
            
            dataService = tmpdataService
            
        }
        
    }
    
    /// Removes a message from the history stored in Core Data at a specific index
    func removeMessageFromHistory(atIndex index: Int) {
        
        persistentContainer.viewContext.delete(history[index])
        saveContext()
        
    }

    /// Sets the current text input to a message stored in Core Data at a specific index
    func setCurrentTextInputFromStoredMessage(index: Int) {
        
        currentTextInput = history[index].messageName ?? ""
        
    }
    
    /// Sets the current free input to a message stored in Core Data at a specific index
    func setCurrentFreeInputFromStoredMessage(index: Int) {
        
        freeInputName = history[index].messageName ?? ""
        freeInput = history[index].messageContent ?? ""
        
    }
    
    /// Saves the auto send setting in Core Data
    func save(autoSend state: Bool) {
        
        if (state == true) {
            
            dataService[0].autoSend = true
            
        } else {
            
            dataService[0].autoSend = false
            
        }
        
        saveContext()
        
    }
    
    /// Saves the loop velocity in Core Data
    func save(loopVelocity value: Float) {
        
        dataService[0].loopVelocity = Double(value)
        loopVelocity = Double(value)
        saveContext()
        
    }
    
    /// Adds an observer to the array to get notified if the notifyStateObservers()-function is called
    func attachStateObserver(observer : IStateObserver){
        stateObserverArray.append(observer)
    }
    
    /// Adds an observer to the array to get notified if the notifyErrorObservers()-function is called
    func attachErrorObserver(observer : IErrorObserver){
        errorObserverArray.append(observer)
    }
    
    /// Notifies all signed in observers
    private func notifyStateObservers(){
        
        for observer in stateObserverArray {
            
            observer.update()
            
        }
        
    }
    
    /// Notifies all signed in observers
    private func notifyErrorObservers(){
        
        for observer in errorObserverArray {
            
            observer.update()
            
        }
        
    }
    
}
