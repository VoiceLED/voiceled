//
//  IConnectionStateObserver.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 24.04.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import Foundation

/// This protocol will be implemented by every class which needs a notification about states
protocol IStateObserver {
    
    func update()
    
}
