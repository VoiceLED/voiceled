//
//  Communicator.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 17.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import CoreBluetooth
import UIKit

/// This class provides the bluetooth connection to the LED matrix
class Communicator: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    /// Contains an instance of this class to make it available for other classes. It is "static" because it should be instantiated before any other class should need the data or the functions of this class.
    static let sharedInstance = Communicator()
    
    // Save the UUIDs to search them directly
    let bleMatrixServiceCBUUID              = CBUUID(string: "0xeb77f550-1015-470b-b9be-c42549f1b0a5")
    let bleMatrixHeaderCharacteristicCBUUID = CBUUID(string: "0xeb77f551-1015-470b-b9be-c42549f1b0a5")
    let bleMatrixDataCharacteristicCBUUID   = CBUUID(string: "0xeb77f552-1015-470b-b9be-c42549f1b0a5")
    
    let bleTemperatureServiceCBUUID  = CBUUID(string: "0xeb77f560-1015-470b-b9be-c42549f1b0a5")
    let bleTemperatureCharacteristicCBUUID = CBUUID(string: "0xeb77f561-1015-470b-b9be-c42549f1b0a5")
    
    let bleErrorServiceCBUUID  = CBUUID(string: "0xeb77f570-1015-470b-b9be-c42549f1b0a5")
    let bleErrorCharacteristicCBUUID = CBUUID(string: "0xeb77f571-1015-470b-b9be-c42549f1b0a5")
    
    /// The central manager is the server part of the connection
    var centralManager : CBCentralManager?
    
    /// The peripheral is the client part of the connection and it connects to the central manager
    var matrixPeripheral : CBPeripheral?
    
    /// Store the characteristics to have access to the values in it
    var headerCharacteristic : CBCharacteristic?
    var dataCharacteristic : CBCharacteristic?
    var temperatureCharacteristic : CBCharacteristic?
    var errorCharacteristic : CBCharacteristic?
    
    private override init () {
        super.init()
        
        // Put the central manager action into a new thread
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue(label: "centralQueue", attributes: .concurrent))
        
    }
    
    /// Sends the text message to the peripheral (the LED matrix)
    func send(textMessage message: String, andLoopVelocity velocity: Int) {
        
        let queue = DispatchQueue(label: "sleepingQueue")
        queue.async{
    
            guard let headerChar = self.headerCharacteristic else {
                return
            }
            
            guard let matrixChar = self.dataCharacteristic else {
                return
            }
            
            guard let per = self.matrixPeripheral else {
                return
            }
            
            // Convert a message to data, relevant bits and number of bluetooth messages requiered
            let bleTuple = MessageConverter.encode(textMessageToData: message)
            let data = bleTuple.message!
            let bits = bleTuple.relevantBits
            let requiredMessages = bleTuple.numberOfMessages
            
            // Build the Header
            
            var u16Bits = UInt16(bits) // 2Byte relevant bits
            var u8Msgs = UInt8(requiredMessages) // 1Byte number of bluetooth messages requiered
            var u8MsgV = UInt8(velocity) // 1Byte message velocity
            
            // Convert the header relevant values to data and print the header
            var bitsData = Data(buffer: UnsafeBufferPointer(start: &u16Bits, count: 1))
            let msgsData = Data(buffer: UnsafeBufferPointer(start: &u8Msgs, count: 1))
            let msgVData = Data(buffer: UnsafeBufferPointer(start: &u8MsgV, count: 1))
            bitsData.reverse() // reverses bytes in header to conform to little endian format
            bitsData.append(msgsData) // append message count as third byte
            bitsData.append(msgVData) // append message count as third byte
            
            print("Header: "+bitsData.hexadecimal+" Data: \(data) "+data.hexadecimal)
            
            // Send the header
            per.writeValue(bitsData, for: headerChar, type: CBCharacteristicWriteType.withoutResponse)
            
            // Send the message but split it if it is to long
            for n in 1...requiredMessages {
                
                // Get the range of the data object to send in 20s steps
                var range = (n-1)*20..<20*n
                
                // Modify range to not exceed the data bounds
                if n == requiredMessages {
                    
                    range = (n-1)*20..<data.endIndex
                    
                }
                
                // Print the part of the message
                print("Message \(n): "+data.subdata(in: range).hexadecimal)
                
                // Send 20 byte chunk message
                per.writeValue(data.subdata(in: range), for: matrixChar, type: CBCharacteristicWriteType.withoutResponse)
                
                if n % 4 == 0 {
                    
                    usleep(400000)
                    
                }
            
            }
            
            // Store in the model that a message was sent since this connection
            ModelManager.sharedInstance.messageSentSinceConnected = true
            
        }
    }
        
    func send(freeMessage message: String, andLoopVelocity velocity: Int) {
        
        guard let headerChar = headerCharacteristic else {
            return
        }
        
        guard let matrixChar = dataCharacteristic else {
            return
        }
        
        guard let per = matrixPeripheral else {
            return
        }
        
        // Convert a message to data, relevant bits and number of bluetooth messages requiered
        let bleTuple = MessageConverter.encode(freeMessageToData: message)
        let data = bleTuple.message!
        let bits = bleTuple.relevantBits
        let requiredMessages = bleTuple.numberOfMessages
        
        // Build the Header
        
        var u16Bits = UInt16(bits) // 2Byte relevant bits
        var u8Msgs = UInt8(requiredMessages) // 1Byte number of bluetooth messages requiered
        var u8MsgV = UInt8(velocity) // 1Byte message velocity
        
        // Convert the header relevant values to data and print the header
        var bitsData = Data(buffer: UnsafeBufferPointer(start: &u16Bits, count: 1))
        let msgsData = Data(buffer: UnsafeBufferPointer(start: &u8Msgs, count: 1))
        let msgVData = Data(buffer: UnsafeBufferPointer(start: &u8MsgV, count: 1))
        bitsData.reverse() // reverses bytes in header to conform to little endian format
        bitsData.append(msgsData) // append message count as third byte
        bitsData.append(msgVData) // append message count as third byte
        
        print("Header: "+bitsData.hexadecimal+" Data: \(data) "+data.hexadecimal)
        
        // Send the header
        per.writeValue(bitsData, for: headerChar, type: CBCharacteristicWriteType.withoutResponse)
        
        // Send the message but split it if it is to long
        for n in 1...requiredMessages {
            
            // Get the range of the data object to send in 20s steps
            var range = (n-1)*20..<20*n
            
            // Modify range to not exceed data bounds
            if n == requiredMessages {
                range = (n-1)*20..<data.endIndex
            }
            
            // Print the part of the message
            print("Message \(n): " + data.subdata(in: range).hexadecimal)
            
            // send 20byte chunk messages to data char
            // per.writeValue(data.subdata(in: range), for: matrixChar, type: CBCharacteristicWriteType.withoutResponse)
            
            // Prevent a Bluetooth stack overflow on the matrix
            //usleep(1500000)
            
        }
        
        // Store in the model that a message was sent since this connection
        ModelManager.sharedInstance.messageSentSinceConnected = true
    
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        // Check the state the central manager is
        switch central.state {
            
        case .unknown:
            
            print("Bluetooth status is UNKNOWN")
            
        case .resetting:
            
            print("Bluetooth status is RESETTING")
            
        case .unsupported:
            
            print("Bluetooth status is UNSUPPORTED")
            
        case .unauthorized:
            
            print("Bluetooth status is UNAUTHORIZED")
            
        case .poweredOff:
            
            print("Bluetooth status is POWERED OFF")
            
        case .poweredOn:
            
            print("Bluetooth status is POWERED ON")
            
            // Scan for the LED matrix if the state is powered on
            centralManager?.scanForPeripherals(withServices: [bleMatrixServiceCBUUID])
            
        @unknown default:
            fatalError()
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        // Check the peripheral state
        decodePeripheralState(peripheralState: peripheral.state)
        
        // Store the peripheral
        matrixPeripheral = peripheral
        
        // Set this class to the delegate of the peripheral
        matrixPeripheral?.delegate = self
        
        // Stop scanning for the LED matrix because it is found
        centralManager?.stopScan()
        
        // Connect to the peripheral
        centralManager?.connect(matrixPeripheral!)
        
        // Check the peripheral state again because know it should be connected
        decodePeripheralState(peripheralState: peripheral.state)
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        print("Services: \(String(describing: peripheral.services))")
        
        // Discover the needed Services for the full app functionality
        matrixPeripheral?.discoverServices([bleMatrixServiceCBUUID, bleTemperatureServiceCBUUID, bleErrorServiceCBUUID])
        
        // Check the peripheral state
        decodePeripheralState(peripheralState: peripheral.state)
        
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        // If an established connection breaks the central manager is looking for a peripheral with the needed services. Basically it is looking for the LED matrix again
        centralManager?.scanForPeripherals(withServices: [bleMatrixServiceCBUUID, bleTemperatureServiceCBUUID, bleErrorServiceCBUUID])
        
        // Check the peripheral state
        decodePeripheralState(peripheralState: peripheral.state)
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            
            // If a service is discovered check which service it is and discover its characteristics
            switch service.uuid {
                
            case bleMatrixServiceCBUUID:
                
                print("Service: \(service)")
            peripheral.discoverCharacteristics([bleMatrixHeaderCharacteristicCBUUID,bleMatrixDataCharacteristicCBUUID], for: service)
                
            case bleTemperatureServiceCBUUID:
                
                print("Service: \(service)")
                
                peripheral.discoverCharacteristics([bleTemperatureCharacteristicCBUUID], for: service)
                
            case bleErrorServiceCBUUID:
                
                print("Service: \(service)")
                
                peripheral.discoverCharacteristics([bleErrorCharacteristicCBUUID], for: service)
                
            default:
                
                print("Unknown service: \(service)")
                
            }
            
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        // Iterate through the characteristics and save them
        for characteristic in service.characteristics! {
            
            print("Characteristic: \(characteristic)")
            
            switch characteristic.uuid {
                
            case bleMatrixHeaderCharacteristicCBUUID:
                
                headerCharacteristic = characteristic
                
            case bleMatrixDataCharacteristicCBUUID:
                
                dataCharacteristic = characteristic
                
            case bleTemperatureCharacteristicCBUUID:
                
                temperatureCharacteristic = characteristic
                
                peripheral.readValue(for: characteristic)
                
            case bleErrorCharacteristicCBUUID:
                
                errorCharacteristic = characteristic
                
                peripheral.readValue(for: characteristic)
                
            default:
                
                print("Unknown characteristic: \(characteristic)")
                
            }
            
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        // If the characteristic value gets updated look which one it is
        switch characteristic.uuid {
            
        case bleTemperatureCharacteristicCBUUID:
            
            peripheral.readValue(for: characteristic)
            
            ModelManager.sharedInstance.temperature = Int(UInt64(characteristic.value!.hexadecimal, radix:16) ?? 0)
            
            print("Temperature: \(ModelManager.sharedInstance.temperature)")
            
        case bleErrorCharacteristicCBUUID:
            
            peripheral.readValue(for: characteristic)
            
            print(characteristic.value!)
            ModelManager.sharedInstance.error = Int(UInt64(characteristic.value!.hexadecimal, radix:16) ?? 0)
            
            print("Err: \(ModelManager.sharedInstance.error)")
            
        default:
            
            print("Unknown characteristic: \(characteristic)")
            
        }
        
    }
    
    /// Write result
    private func peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic characteristic: CBCharacteristic, error: NSError?)
    {
        
        if let error = error {
            
            print("error: \(error)")
            return
            
        }
        
        print("Successfully written to Characteristic!")
        
    }
    
    /// Checks the peripheral state and save is into the model
    func decodePeripheralState(peripheralState: CBPeripheralState) {
        
        switch peripheralState {
            
        case .disconnected:
            
            print("Peripheral state: disconnected")
            ModelManager.sharedInstance.connectionState = "disconnected"
            
        case .connected:
            
            print("Peripheral state: connected")
            
            // Wait for connection processes
            sleep(1)
            
            ModelManager.sharedInstance.connectionState = "connected"
            
            // If the auto send setting is active send the last message (of the history)
            if (ModelManager.sharedInstance.dataService[0].autoSend == true) {
                
                if (ModelManager.sharedInstance.history.last?.messageType == "Text") {
                    
                    send(textMessage: ModelManager.sharedInstance.history.last?.messageName ?? ModelManager.sharedInstance.history.first?.messageName ?? "", andLoopVelocity: Int(ModelManager.sharedInstance.loopVelocity ?? ModelManager.sharedInstance.initialLoopVelocity))
                    
                } else if (ModelManager.sharedInstance.history.last?.messageType == "Free") {
                    
                    send(freeMessage: ModelManager.sharedInstance.history.last?.messageContent ?? "", andLoopVelocity: Int(ModelManager.sharedInstance.loopVelocity ?? ModelManager.sharedInstance.initialLoopVelocity))
                    
                }
                
            }
            
        case .connecting:
            
            print("Peripheral state: connecting")
            ModelManager.sharedInstance.connectionState = "connecting"
            
        case .disconnecting:
            
            print("Peripheral state: disconnecting")
            ModelManager.sharedInstance.connectionState = "disconnecting"
            
        @unknown default:
            
            fatalError()
            
        }
        
    }
    
}


// pragma mark - Hexstring Data
// Conversion Stackoverflow

extension Data {
    
    /// Hexadecimal string representation of `Data` object.
    
    var hexadecimal: String {
        return map { String(format: "%02x", $0) }
            .joined()
    }
}

