//
//  MessageConverter.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 16.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import Foundation

class MessageConverter {
    
    static let PAYLOAD_LENGTH = 160 // 20bytes in bits
    static let ROW_LENGTH = ModelManager.sharedInstance.COLS // available LEDs on one row of the matrix
    
    /// Convert a message to data, relevant bits and number of bluetooth messages requiered
    static func encode(textMessageToData message: String) -> (message: Data?, relevantBits: Int, numberOfMessages: Int) {
        
        // get the message as binary string and get the length of it
        let encodedMessageAndLength = encode(textMessageToBinaryString: message)
        let encodedMessage = encodedMessageAndLength.encodedMessage
        let relevantBits = encodedMessageAndLength.length
        
        // convert the binary string into a data object
        let messageAsData = convert(hexToData: convert(binaryToHex: pad(StringToFullByteSize: encodedMessage)))
        
        // calculate the number of bluetooth messages requiered
        var numberOfMessages = relevantBits / PAYLOAD_LENGTH
        if  relevantBits % PAYLOAD_LENGTH != 0 {
            numberOfMessages += 1
        }
        
        return (messageAsData, relevantBits, numberOfMessages)
    }
    
    static func encode(freeMessageToData message: String) -> (message: Data?, relevantBits: Int, numberOfMessages: Int) {
        
        // get the message as binary string and get the length of it
        let relevantBits = message.count

        // convert the binary string into a data object
        let messageAsData = convert(hexToData: convert(binaryToHex: pad(StringToFullByteSize: message)))

        // calculate the number of bluetooth messages requiered
        var numberOfMessages = relevantBits / PAYLOAD_LENGTH
        if  relevantBits % PAYLOAD_LENGTH != 0 {
            numberOfMessages += 1
        }

        return (messageAsData, relevantBits, numberOfMessages)
    }
    
    /// Show a preview how the binary string would be visualized on the VoiceLED board
    static func printMatrixPreview(_messageToPrint: String){
        
        // to know how long every row is (how many columns the message has)
        let lengthOfEachRow = _messageToPrint.count / ModelManager.sharedInstance.ROWS
        
        // if the message is empty there are nothing to show (and it prevents a division by 0)
        if lengthOfEachRow != 0 {
            
            // goes step by step through every row and print the whole row
            for n in 0..<_messageToPrint.count / lengthOfEachRow { // should be 7 every time but safe is safe ;)
                
                // calculates the range that should be printed next
                let range = _messageToPrint.index(_messageToPrint.startIndex, offsetBy: n * lengthOfEachRow)..<_messageToPrint.index(_messageToPrint.endIndex, offsetBy: -_messageToPrint.count + (n + 1) * lengthOfEachRow)
                
                // takes the binary symbols in the described range and replace all zeros with a space ( "0" -> " " ) and all ones with xs ( "1" -> "x")
                let messageToPrint = String(String(_messageToPrint[range]).replacingOccurrences(of: "0", with: " ", options: .literal, range: nil)).replacingOccurrences(of: "1", with: "x", options: .literal, range: nil)
                
                // prints the row
                print(messageToPrint)
            }
        }
    }
    
    /// Fills up a binary string to fill payload (with zeros behind the last digit)
    static func pad(StringToFullByteSize message: String) -> String {
        
        var filledUpString = message
        
        // writes a zero behind the last digit until the last byte of the payload is filled by zeros
        while (filledUpString.count % 8 != 0) {
            filledUpString.append("0")
        }
        
        
        //        while (filledUpString.count/8 % 20 != 0) {
        //            filledUpString.append("0")
        //        }
        //
        //        print("Filled String Bytes: \(filledUpString.count/8)")
        
        return filledUpString
    }
    
    /// Convert a string of binary digits to a string of hex digits
    static func convert(binaryToHex string: String) -> String {
        
        var hexString = ""
        
        // takes 8 binary digits from the given string, converts it into a substring of hex digits, append it to the string that contains the whole hex digits and do this procedure over and over again
        for n in 0..<string.count / 4 {
            
            // calculates the range that should be printed next
            let range = string.index(string.startIndex, offsetBy: n * 4)..<string.index(string.endIndex, offsetBy: -string.count + (n + 1) * 4)
            
            // takes the binary symbols in the described range
            let partOfStringToConvert = String(string[range])
            
            // binary string digitates to hex string!
            let tmp = Int(partOfStringToConvert, radix: 2)!
            let partOfStringToConvertInHex = String(tmp, radix: 16)
            hexString.append(partOfStringToConvertInHex)
        }
        return hexString
    }
    
    /// Create `Data` from hexadecimal string representation
    static func convert(hexToData message: String) -> Data? {
        return message.hexadecimal
    }
    
    /// Takes a string and converts it to a string of binary symbols which can then be converted into raw data
    static func encode(textMessageToBinaryString message: String) -> (encodedMessage: String, length: Int) {
        
        let messageToEncode = message.uppercased()
        var encodedMessage = ""
        var rowsFromEncodedMessage:[String] = ["","","","","","",""]
        var charFromMessageToEncode = ""
        
        if (messageToEncode == "") {
            // send a empty message
            rowsFromEncodedMessage[0].append("0") //
            rowsFromEncodedMessage[1].append("0") //
            rowsFromEncodedMessage[2].append("0") //
            rowsFromEncodedMessage[3].append("0") //
            rowsFromEncodedMessage[4].append("0") //
            rowsFromEncodedMessage[5].append("0") //
            rowsFromEncodedMessage[6].append("0") //
        } else {
        
            // interate through the message to encode
            for n in 0..<messageToEncode.count{
                
                // get a single char
                let range = messageToEncode.index(messageToEncode.startIndex, offsetBy: n)..<messageToEncode.index(messageToEncode.endIndex, offsetBy: -messageToEncode.count+n+1)
                charFromMessageToEncode = String(messageToEncode[range])
                
                // encode the char
                switch charFromMessageToEncode {
                case "A":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("10001")   //  x   x
                    break
                case "B":
                    rowsFromEncodedMessage[0].append("11110")   //  xxxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("11110")   //  xxxx
                    break
                case "C":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("10000")   //  x
                    rowsFromEncodedMessage[4].append("10000")   //  x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "D":
                    rowsFromEncodedMessage[0].append("11110")   //  xxxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("10001")   //  x   x
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("11110")   //  xxxx
                    break
                case "E":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("10000")   //  x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("10000")   //  x
                    rowsFromEncodedMessage[5].append("10000")   //  x
                    rowsFromEncodedMessage[6].append("11111")   //  xxxxx
                    break
                case "F":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("10000")   //  x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("10000")   //  x
                    rowsFromEncodedMessage[5].append("10000")   //  x
                    rowsFromEncodedMessage[6].append("10000")   //  x
                    break
                case "G":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("10000")   //  x
                    rowsFromEncodedMessage[4].append("10011")   //  x  xx
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "H":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("10001")   //  x   x
                    break
                case "I":
                    rowsFromEncodedMessage[0].append("111")     //   xxx
                    rowsFromEncodedMessage[1].append("010")     //    x
                    rowsFromEncodedMessage[2].append("010")     //    x
                    rowsFromEncodedMessage[3].append("010")     //    x
                    rowsFromEncodedMessage[4].append("010")     //    x
                    rowsFromEncodedMessage[5].append("010")     //    x
                    rowsFromEncodedMessage[6].append("111")     //   xxx
                    break
                case "J":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("00001")   //      x
                    rowsFromEncodedMessage[2].append("00001")   //      x
                    rowsFromEncodedMessage[3].append("00001")   //      x
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "K":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("10010")   //  x  x
                    rowsFromEncodedMessage[2].append("10100")   //  x x
                    rowsFromEncodedMessage[3].append("11000")   //  xx
                    rowsFromEncodedMessage[4].append("10100")   //  x x
                    rowsFromEncodedMessage[5].append("10010")   //  x  x
                    rowsFromEncodedMessage[6].append("10001")   //  x   x
                    break
                case "L":
                    rowsFromEncodedMessage[0].append("10000")   //  x
                    rowsFromEncodedMessage[1].append("10000")   //  x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("10000")   //  x
                    rowsFromEncodedMessage[4].append("10000")   //  x
                    rowsFromEncodedMessage[5].append("10000")   //  x
                    rowsFromEncodedMessage[6].append("11111")   //  xxxxx
                    break
                case "M":
                    rowsFromEncodedMessage[0].append("100001")  //  x    x
                    rowsFromEncodedMessage[1].append("110011")  //  xx  xx
                    rowsFromEncodedMessage[2].append("101101")  //  x xx x
                    rowsFromEncodedMessage[3].append("100001")  //  x    x
                    rowsFromEncodedMessage[4].append("100001")  //  x    x
                    rowsFromEncodedMessage[5].append("100001")  //  x    x
                    rowsFromEncodedMessage[6].append("100001")  //  x    x
                    break
                case "N":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("11001")   //  xx  x
                    rowsFromEncodedMessage[2].append("10101")   //  x x x
                    rowsFromEncodedMessage[3].append("10011")   //  x  xx
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("10001")   //  x   x
                    break
                case "O":
                    rowsFromEncodedMessage[0].append("011110")  //   xxxx
                    rowsFromEncodedMessage[1].append("100001")  //  x    x
                    rowsFromEncodedMessage[2].append("100001")  //  x    x
                    rowsFromEncodedMessage[3].append("100001")  //  x    x
                    rowsFromEncodedMessage[4].append("100001")  //  x    x
                    rowsFromEncodedMessage[5].append("100001")  //  x    x
                    rowsFromEncodedMessage[6].append("011110")  //   xxxx
                    break
                case "P":
                    rowsFromEncodedMessage[0].append("11110")   //  xxxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("10000")   //  x
                    rowsFromEncodedMessage[5].append("10000")   //  x
                    rowsFromEncodedMessage[6].append("10000")   //  x
                    break
                case "Q":
                    rowsFromEncodedMessage[0].append("011110")  //   xxxx
                    rowsFromEncodedMessage[1].append("100001")  //  x    x
                    rowsFromEncodedMessage[2].append("100001")  //  x    x
                    rowsFromEncodedMessage[3].append("100001")  //  x    x
                    rowsFromEncodedMessage[4].append("100101")  //  x  x x
                    rowsFromEncodedMessage[5].append("100010")  //  x   x
                    rowsFromEncodedMessage[6].append("011101")  //   xxx x
                    break
                case "R":
                    rowsFromEncodedMessage[0].append("11110")   //  xxxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("10100")   //  x x
                    rowsFromEncodedMessage[5].append("10010")   //  x  x
                    rowsFromEncodedMessage[6].append("10001")   //  x   x
                    break
                case "S":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("01000")   //   x
                    rowsFromEncodedMessage[3].append("00100")   //    x
                    rowsFromEncodedMessage[4].append("00010")   //     x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "T":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("00100")   //    x
                    rowsFromEncodedMessage[2].append("00100")   //    x
                    rowsFromEncodedMessage[3].append("00100")   //    x
                    rowsFromEncodedMessage[4].append("00100")   //    x
                    rowsFromEncodedMessage[5].append("00100")   //    x
                    rowsFromEncodedMessage[6].append("00100")   //    x
                    break
                case "U":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("10001")   //  x   x
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "V":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("10001")   //  x   x
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("01010")   //   x x
                    rowsFromEncodedMessage[6].append("00100")   //    x
                    break
                case "W":
                    rowsFromEncodedMessage[0].append("100001")  //  x    x
                    rowsFromEncodedMessage[1].append("100001")  //  x    x
                    rowsFromEncodedMessage[2].append("100001")  //  x    x
                    rowsFromEncodedMessage[3].append("100001")  //  x    x
                    rowsFromEncodedMessage[4].append("101101")  //  x xx x
                    rowsFromEncodedMessage[5].append("110011")  //  xx  xx
                    rowsFromEncodedMessage[6].append("100001")  //  x    x
                    break
                case "X":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("01010")   //   x x
                    rowsFromEncodedMessage[3].append("00100")   //    x
                    rowsFromEncodedMessage[4].append("01010")   //   x x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("10001")   //  x   x
                    break
                case "Y":
                    rowsFromEncodedMessage[0].append("10001")   //  x   x
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("01010")   //   x x
                    rowsFromEncodedMessage[4].append("00100")   //    x
                    rowsFromEncodedMessage[5].append("00100")   //    x
                    rowsFromEncodedMessage[6].append("00100")   //    x
                    break
                case "Z":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("00001")   //      x
                    rowsFromEncodedMessage[2].append("00010")   //     x
                    rowsFromEncodedMessage[3].append("00100")   //    x
                    rowsFromEncodedMessage[4].append("01000")   //   x
                    rowsFromEncodedMessage[5].append("10000")   //  x
                    rowsFromEncodedMessage[6].append("11111")   //  xxxxx
                    break
                case "0":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("10001")   //  x   x
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "1":
                    rowsFromEncodedMessage[0].append("001")     //      x
                    rowsFromEncodedMessage[1].append("011")     //     xx
                    rowsFromEncodedMessage[2].append("101")     //    x x
                    rowsFromEncodedMessage[3].append("001")     //      x
                    rowsFromEncodedMessage[4].append("001")     //      x
                    rowsFromEncodedMessage[5].append("001")     //      x
                    rowsFromEncodedMessage[6].append("001")     //      x
                    break
                case "2":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("00001")   //      x
                    rowsFromEncodedMessage[3].append("00010")   //     x
                    rowsFromEncodedMessage[4].append("00100")   //    x
                    rowsFromEncodedMessage[5].append("01000")   //   x
                    rowsFromEncodedMessage[6].append("11111")   //  xxxxx
                    break
                case "3":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("00001")   //      x
                    rowsFromEncodedMessage[3].append("00110")   //    xx
                    rowsFromEncodedMessage[4].append("00001")   //      x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "4":
                    rowsFromEncodedMessage[0].append("10000")   //  x
                    rowsFromEncodedMessage[1].append("10010")   //  x
                    rowsFromEncodedMessage[2].append("10010")   //  x  x
                    rowsFromEncodedMessage[3].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[4].append("00010")   //     x
                    rowsFromEncodedMessage[5].append("00010")   //     x
                    rowsFromEncodedMessage[6].append("00010")   //     x
                    break
                case "5":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("10000")   //  x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("00001")   //      x
                    rowsFromEncodedMessage[5].append("00001")   //      x
                    rowsFromEncodedMessage[6].append("11110")   //  xxxx
                    break
                case "6":
                    rowsFromEncodedMessage[0].append("01111")   //   xxxx
                    rowsFromEncodedMessage[1].append("10000")   //  x
                    rowsFromEncodedMessage[2].append("10000")   //  x
                    rowsFromEncodedMessage[3].append("11110")   //  xxxx
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "7":
                    rowsFromEncodedMessage[0].append("11111")   //  xxxxx
                    rowsFromEncodedMessage[1].append("00001")   //      x
                    rowsFromEncodedMessage[2].append("00010")   //     x
                    rowsFromEncodedMessage[3].append("00100")   //    x
                    rowsFromEncodedMessage[4].append("01000")   //   x
                    rowsFromEncodedMessage[5].append("10000")   //  x
                    rowsFromEncodedMessage[6].append("10000")   //  x
                    break
                case "8":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("01110")   //   xxx
                    rowsFromEncodedMessage[4].append("10001")   //  x   x
                    rowsFromEncodedMessage[5].append("10001")   //  x   x
                    rowsFromEncodedMessage[6].append("01110")   //   xxx
                    break
                case "9":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("10001")   //  x   x
                    rowsFromEncodedMessage[3].append("01111")   //   xxxx
                    rowsFromEncodedMessage[4].append("00001")   //      x
                    rowsFromEncodedMessage[5].append("00001")   //      x
                    rowsFromEncodedMessage[6].append("11110")   //  xxxx
                    break
                case "!":
                    rowsFromEncodedMessage[0].append("010")     //    x
                    rowsFromEncodedMessage[1].append("010")     //    x
                    rowsFromEncodedMessage[2].append("010")     //    x
                    rowsFromEncodedMessage[3].append("010")     //    x
                    rowsFromEncodedMessage[4].append("010")     //    x
                    rowsFromEncodedMessage[5].append("000")     //
                    rowsFromEncodedMessage[6].append("010")     //    x
                    break
                case "%":
                    rowsFromEncodedMessage[0].append("0100001") //   x    x
                    rowsFromEncodedMessage[1].append("1010010") //  x x  x
                    rowsFromEncodedMessage[2].append("0100100") //   x  x
                    rowsFromEncodedMessage[3].append("0001000") //     x
                    rowsFromEncodedMessage[4].append("0010010") //    x  x
                    rowsFromEncodedMessage[5].append("0100101") //   x  x x
                    rowsFromEncodedMessage[6].append("1000010") //  x    x
                    break
                case "/":
                    rowsFromEncodedMessage[0].append("0000001") //        x
                    rowsFromEncodedMessage[1].append("0000010") //       x
                    rowsFromEncodedMessage[2].append("0000100") //      x
                    rowsFromEncodedMessage[3].append("0001000") //     x
                    rowsFromEncodedMessage[4].append("0010000") //    x
                    rowsFromEncodedMessage[5].append("0100000") //   x
                    rowsFromEncodedMessage[6].append("1000000") //  x
                    break
                case "(":
                    rowsFromEncodedMessage[0].append("001")     //    x
                    rowsFromEncodedMessage[1].append("010")     //   x
                    rowsFromEncodedMessage[2].append("100")     //  x
                    rowsFromEncodedMessage[3].append("100")     //  x
                    rowsFromEncodedMessage[4].append("100")     //  x
                    rowsFromEncodedMessage[5].append("010")     //   x
                    rowsFromEncodedMessage[6].append("001")     //    x
                    break
                case ")":
                    rowsFromEncodedMessage[0].append("100")     //  x
                    rowsFromEncodedMessage[1].append("010")     //   x
                    rowsFromEncodedMessage[2].append("001")     //    x
                    rowsFromEncodedMessage[3].append("001")     //    x
                    rowsFromEncodedMessage[4].append("001")     //    x
                    rowsFromEncodedMessage[5].append("010")     //   x
                    rowsFromEncodedMessage[6].append("100")     //  x
                    break
                case "=":
                    rowsFromEncodedMessage[0].append("0000")    //
                    rowsFromEncodedMessage[1].append("0000")    //
                    rowsFromEncodedMessage[2].append("1111")    //   xxxx
                    rowsFromEncodedMessage[3].append("0000")    //
                    rowsFromEncodedMessage[4].append("1111")    //   xxxx
                    rowsFromEncodedMessage[5].append("0000")    //
                    rowsFromEncodedMessage[6].append("0000")    //
                    break
                case "?":
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("00001")   //      x
                    rowsFromEncodedMessage[3].append("00010")   //     x
                    rowsFromEncodedMessage[4].append("00100")   //    x
                    rowsFromEncodedMessage[5].append("00000")   //
                    rowsFromEncodedMessage[6].append("00100")   //    x
                    break
                case ":":
                    rowsFromEncodedMessage[0].append("0")       //
                    rowsFromEncodedMessage[1].append("1")       //    x
                    rowsFromEncodedMessage[2].append("0")       //
                    rowsFromEncodedMessage[3].append("0")       //
                    rowsFromEncodedMessage[4].append("0")       //
                    rowsFromEncodedMessage[5].append("1")       //    x
                    rowsFromEncodedMessage[6].append("0")       //
                    break
                case ".":
                    rowsFromEncodedMessage[0].append("0")       //
                    rowsFromEncodedMessage[1].append("0")       //
                    rowsFromEncodedMessage[2].append("0")       //
                    rowsFromEncodedMessage[3].append("0")       //
                    rowsFromEncodedMessage[4].append("0")       //
                    rowsFromEncodedMessage[5].append("0")       //
                    rowsFromEncodedMessage[6].append("1")       //   x
                    break
                case ";":
                    rowsFromEncodedMessage[0].append("00")      //
                    rowsFromEncodedMessage[1].append("01")      //
                    rowsFromEncodedMessage[2].append("00")      //   x
                    rowsFromEncodedMessage[3].append("00")      //
                    rowsFromEncodedMessage[4].append("00")      //
                    rowsFromEncodedMessage[5].append("01")      //   x
                    rowsFromEncodedMessage[6].append("10")      //  x
                    break
                case ",":
                    rowsFromEncodedMessage[0].append("00")      //
                    rowsFromEncodedMessage[1].append("00")      //
                    rowsFromEncodedMessage[2].append("00")      //
                    rowsFromEncodedMessage[3].append("00")      //
                    rowsFromEncodedMessage[4].append("00")      //
                    rowsFromEncodedMessage[5].append("11")      //   x
                    rowsFromEncodedMessage[6].append("01")      //  x
                    break
                case " ":
                    rowsFromEncodedMessage[0].append("00")      //
                    rowsFromEncodedMessage[1].append("00")      //
                    rowsFromEncodedMessage[2].append("00")      //
                    rowsFromEncodedMessage[3].append("00")      //
                    rowsFromEncodedMessage[4].append("00")      //
                    rowsFromEncodedMessage[5].append("00")      //
                    rowsFromEncodedMessage[6].append("00")      //
                    break
                case "-":
                    rowsFromEncodedMessage[0].append("00000")   //
                    rowsFromEncodedMessage[1].append("00000")   //
                    rowsFromEncodedMessage[2].append("00000")   //
                    rowsFromEncodedMessage[3].append("11111")   // xxxxx
                    rowsFromEncodedMessage[4].append("00000")   //
                    rowsFromEncodedMessage[5].append("00000")   //
                    rowsFromEncodedMessage[6].append("00000")   //
                    break
                case "+":
                    rowsFromEncodedMessage[0].append("00000")   //
                    rowsFromEncodedMessage[1].append("00100")   //   x
                    rowsFromEncodedMessage[2].append("00100")   //   x
                    rowsFromEncodedMessage[3].append("11111")   // xxxxx
                    rowsFromEncodedMessage[4].append("00100")   //   x
                    rowsFromEncodedMessage[5].append("00100")   //   x
                    rowsFromEncodedMessage[6].append("00000")   //
                    break
                case "*":
                    rowsFromEncodedMessage[0].append("00000")   //
                    rowsFromEncodedMessage[1].append("00100")   // x   x
                    rowsFromEncodedMessage[2].append("00100")   //  x x
                    rowsFromEncodedMessage[3].append("11111")   //   x
                    rowsFromEncodedMessage[4].append("00100")   //  x x
                    rowsFromEncodedMessage[5].append("00100")   // x   x
                    rowsFromEncodedMessage[6].append("00000")   //
                    break
                case "÷":
                    rowsFromEncodedMessage[0].append("00000")   //
                    rowsFromEncodedMessage[1].append("00100")   //   x
                    rowsFromEncodedMessage[2].append("00100")   //
                    rowsFromEncodedMessage[3].append("11111")   // xxxxx
                    rowsFromEncodedMessage[4].append("00100")   //
                    rowsFromEncodedMessage[5].append("00100")   //   x
                    rowsFromEncodedMessage[6].append("00000")   //
                    break
                case "_":
                    rowsFromEncodedMessage[0].append("0000")    //
                    rowsFromEncodedMessage[1].append("0000")    //
                    rowsFromEncodedMessage[2].append("0000")    //
                    rowsFromEncodedMessage[3].append("0000")    //
                    rowsFromEncodedMessage[4].append("0000")    //
                    rowsFromEncodedMessage[5].append("0000")    //
                    rowsFromEncodedMessage[6].append("1111")    // xxxx
                    break
                case "^":
                    rowsFromEncodedMessage[0].append("010")     //   x
                    rowsFromEncodedMessage[1].append("101")     //  x x
                    rowsFromEncodedMessage[2].append("000")     //
                    rowsFromEncodedMessage[3].append("000")     //
                    rowsFromEncodedMessage[4].append("000")     //
                    rowsFromEncodedMessage[5].append("000")     //
                    rowsFromEncodedMessage[6].append("000")     //
                    break
                case "<":
                    rowsFromEncodedMessage[0].append("000")     //
                    rowsFromEncodedMessage[1].append("001")     //   x
                    rowsFromEncodedMessage[2].append("010")     //  x
                    rowsFromEncodedMessage[3].append("100")     // x
                    rowsFromEncodedMessage[4].append("010")     //  x
                    rowsFromEncodedMessage[5].append("001")     //   x
                    rowsFromEncodedMessage[6].append("000")     //
                    break
                case ">":
                    rowsFromEncodedMessage[0].append("000")     //
                    rowsFromEncodedMessage[1].append("100")     // x
                    rowsFromEncodedMessage[2].append("010")     //  x
                    rowsFromEncodedMessage[3].append("001")     //   x
                    rowsFromEncodedMessage[4].append("010")     //  x
                    rowsFromEncodedMessage[5].append("100")     // x
                    rowsFromEncodedMessage[6].append("000")     //
                    break
                case "´":
                    rowsFromEncodedMessage[0].append("01")      //   x
                    rowsFromEncodedMessage[1].append("10")      //  x
                    rowsFromEncodedMessage[2].append("00")      //
                    rowsFromEncodedMessage[3].append("00")      //
                    rowsFromEncodedMessage[4].append("00")      //
                    rowsFromEncodedMessage[5].append("00")      //
                    rowsFromEncodedMessage[6].append("00")      //
                    break
                case "`":
                    rowsFromEncodedMessage[0].append("10")      //  x
                    rowsFromEncodedMessage[1].append("01")      //   x
                    rowsFromEncodedMessage[2].append("00")      //
                    rowsFromEncodedMessage[3].append("00")      //
                    rowsFromEncodedMessage[4].append("00")      //
                    rowsFromEncodedMessage[5].append("00")      //
                    rowsFromEncodedMessage[6].append("00")      //
                    break
                case "'":
                    rowsFromEncodedMessage[0].append("1")       //  x
                    rowsFromEncodedMessage[1].append("1")       //  x
                    rowsFromEncodedMessage[2].append("0")       //
                    rowsFromEncodedMessage[3].append("0")       //
                    rowsFromEncodedMessage[4].append("0")       //
                    rowsFromEncodedMessage[5].append("0")       //
                    rowsFromEncodedMessage[6].append("0")       //
                    break
                default:
                    rowsFromEncodedMessage[0].append("01110")   //   xxx
                    rowsFromEncodedMessage[1].append("10001")   //  x   x
                    rowsFromEncodedMessage[2].append("00001")   //      x
                    rowsFromEncodedMessage[3].append("00010")   //     x
                    rowsFromEncodedMessage[4].append("00100")   //    x
                    rowsFromEncodedMessage[5].append("00000")   //
                    rowsFromEncodedMessage[6].append("00100")   //    x
                }
                
                // enters the if-statement if the last iteration / last letter is NOT reached
                if (n != messageToEncode.count - 1) {
                    // seperate the chars with an empty column
                    rowsFromEncodedMessage[0].append("0")       //
                    rowsFromEncodedMessage[1].append("0")       //
                    rowsFromEncodedMessage[2].append("0")       //
                    rowsFromEncodedMessage[3].append("0")       //
                    rowsFromEncodedMessage[4].append("0")       //
                    rowsFromEncodedMessage[5].append("0")       //
                    rowsFromEncodedMessage[6].append("0")       //
                }
            }
            
            // enters the if-statement if the message representation on the matrix need more pixels as available (short: if the message is shown in a loop)
            if (rowsFromEncodedMessage[0].count >= ROW_LENGTH) {
                // add an empty column to seperate the first and the last letter
                rowsFromEncodedMessage[0].append("00")           //
                rowsFromEncodedMessage[1].append("00")           //
                rowsFromEncodedMessage[2].append("00")           //
                rowsFromEncodedMessage[3].append("00")           //
                rowsFromEncodedMessage[4].append("00")           //
                rowsFromEncodedMessage[5].append("00")           //
                rowsFromEncodedMessage[6].append("00")           //
            }
        }
        // build the message with its single parts
        for n in 0..<rowsFromEncodedMessage.count {
            encodedMessage += rowsFromEncodedMessage[n]
        }
        
        let numberOfBits = encodedMessage.count
        
        return (encodedMessage, numberOfBits)
    }
    
}

extension String {
    
    /// Create `Data` from hexadecimal string representation
    ///
    /// This creates a `Data` object from hex string. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.
    
    var hexadecimal: Data? {
        var data = Data(capacity: self.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }
    
}
