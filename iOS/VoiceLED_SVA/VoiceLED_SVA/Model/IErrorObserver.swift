//
//  IErrorObserver.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 09.05.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import Foundation

/// This protocol will be implemented by every class which needs a notification about matrix errors
protocol IErrorObserver {
    
    func update()
    
}
