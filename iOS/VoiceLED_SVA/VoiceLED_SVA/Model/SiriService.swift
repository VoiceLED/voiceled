//
//  SiriService.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 17.04.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import Foundation

/// Make this
let siriService = SiriService()

/// Enables Siri Shortcut to transfer its message input into the app
class SiriService {
    
    /// Contains the controller that handles the message from Siri
    var viewController : TextViewController?
    
    /// Sets the viewcontroller to update (smelly code)
    public func set(viewcontroller controller: TextViewController) {
        
        self.viewController = controller
        
    }
    
    /// Creates a new text message if there is a established connection
    public func new(textMessage message: String) {
        
        // Wait until a connection established
        while ModelManager.sharedInstance.connectionState != "connected" {}
        
        // Wait for some connection actions
        sleep(1) // auf hintergrundprozesse warten
        
        // Now there is an established connection so the the text message can be created
        ModelManager.sharedInstance.create(TextMessage: message)
        
    }
    
}
