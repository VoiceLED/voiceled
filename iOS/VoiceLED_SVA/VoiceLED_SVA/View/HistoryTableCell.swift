//
//  HistoryTableCell.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 26.04.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import UIKit

/// Each entry of the history table ("Verlauf"-tab) is an object of this class. It defines what the each cell basically contains
class HistoryTableCell: UITableViewCell {
    
    @IBOutlet var cellView: UIView!
    
    // To display the message name
    @IBOutlet var name: UILabel!
    
    // To display more message information like send date or message type
    @IBOutlet var information: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
