//
//  FreeViewCell.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 28.04.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import UIKit

/// Each cell of the free input tab is an object of this class. It defines what the each cell basically contains and provides
class FreeViewCell: UICollectionViewCell {
    
    /// (Un-)Selects the cell
    @IBAction func selectedLEDAction(_ sender: Any) {
        
        // If the cell is already selected (red background) turn it into white
        if self.backgroundColor == UIColor.red {
            
            // Turn the background into white
            self.backgroundColor = UIColor.white
            
        }
        // Or select the cell
        else {
            
            // Turn the cell into red
            self.backgroundColor = UIColor.red
            
        }
    }
}
