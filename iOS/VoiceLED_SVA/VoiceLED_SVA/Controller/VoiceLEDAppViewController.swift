//
//  VoiceLEDAppViewController.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 25.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import Foundation
import UIKit

/// This controller holds all tabs the user can see and use. Normally this view element do not need a controller file but in this file the controller handles the errors the LED matrix sent to the app
class VoiceLEDAppViewController: UITabBarController, IErrorObserver {
    
    /// Is an Alert appeared
    var errorAlertDidAppear = false
    
    override func viewDidAppear(_ animated: Bool) {
        
        // To get notified if the error code is changed
        ModelManager.sharedInstance.attachErrorObserver(observer: self)
        
    }
    
    /// The observer function which is called if the error code changed
    func update() {
        
        // The code 0 means everything is fine. If its another code the alert can appear
        if ModelManager.sharedInstance.error != 0 {
            
            // If there is no appeared error alert
            if !errorAlertDidAppear {
                
                // To be sure that no multiple error alerts can be present
                errorAlertDidAppear = true
                
                // Create the alert to inform the user about the received error
                let alert = UIAlertController(title: "Matrixfehler", message: String(ModelManager.sharedInstance.error), preferredStyle: UIAlertController.Style.alert)
                
                // Add a button to hide the alert
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                    self.errorAlertDidAppear = false
                }))
                
                // make the alert visible
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
}
