//
//  BoardStateViewController.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 16.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import UIKit
import CoreData

/// This controller provides the functionality behind the view tab the user can see and edit the message history
class BoardStateViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, IStateObserver {
    
    /// Holds the instance of the ModelManager which stores all important data
    var model = ModelManager.sharedInstance
    
    /// Represents the number of matrix columns stored in the model. Zero (0) is just a default value and will be updated in viewDidLoad()
    var cols = 0
    
    /// Represents the number of matrix rows stored in the model. Zero (0) is just a default value and will be updated in viewDidLoad()
    var rows = 0
    
    /// The number of available matrix cells or LEDs. Zero (0) is just a default value. The right value will be calculated in viewDidLoad(): rows * cols
    var numberOfCells = 0
    
    /// Is this tab currently selected or not to cancel the loop preview animation if it is not visible because another app tab is selected
    var showPreview = false
    
    /// Contains a sequence of cells (LEDs) over several lines and looks like a matrix but is not. Shows the  message that is currently visible at the led matrix
    @IBOutlet var previewCollectionView: UICollectionView!
    
    /// Displays the name of the message
    @IBOutlet var previewLabel: UILabel!
    
    /// Displays more message information
    @IBOutlet var previewInfoLabel: UILabel!
    
    /// Displays the currently connection state to the matrix board
    @IBOutlet var connectionStateLabel: UILabel!
    
    /// Shows the temperature of the connected matrix board
    @IBOutlet var temperatureLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save the values stored in the model for quicker accessibility
        cols = model.COLS
        rows = model.ROWS
        
        // Calculate the number of cells (LEDs) the collection view (matrix) should have
        numberOfCells = cols * rows
        
        // To get notified if the connection state changed
        model.attachStateObserver(observer: self)
        
        // Update the displayed connection state
        update()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Show the preview
        loadPreviewFromModel()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        // Stop showing the preview
        showPreview = false
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // Set number of cells the collection view have to the number of cells (LEDs) the matrix have
        return (numberOfCells)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Add one cell to the collection view
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
    }
    
    /// The observer function which is called if the connection state changed
    func update() {
        
        // If the new state is different to the displayed state
        if connectionStateLabel.text != model.connectionState {
            
            // Display the new state
            connectionStateLabel.text = model.connectionState
            
            if model.connectionState == "connected" {
                
                // Loads the last message in the history and display it in the preview
                loadPreviewFromModel()
                
            } else  {
                
                // If the preview is visible the preview stops
                showPreview = false
                
                // Do not show a preview if no message has been sent since the current connection was established
                model.messageSentSinceConnected = false
                
                // Reset the temperature in the model
                model.temperature = 0
                
                // Reset the labels
                previewLabel.text = ""
                previewInfoLabel.text = ""
                temperatureLabel.text = "-"
                
                // Reset the preview. Set the background color back to white (unselected) if it is red (selected)
                for n in 0..<numberOfCells {
                    
                    let cell = previewCollectionView!.cellForItem(at: IndexPath(row: n, section: 0))
                    
                    if (cell?.backgroundColor == UIColor.red) {
                        
                        cell?.backgroundColor = UIColor.white
                        
                    }
                    
                }
                
            }
            
        }
        
        // Update the displayed temperature only if it is changed (or is reset to 0)
        if temperatureLabel.text != String(model.temperature) {
            
            // If there is no established connection do not display any temperature value
            if model.connectionState != "connected" {
                
                temperatureLabel.text = "-"
                
            } else { // if there is a established connection
                
                // Display the current stored temperature of the LED matrix
                temperatureLabel.text = String(model.temperature)
                
            }
            
        }
        
    }
    
    /// Loads the last message in the history and display it in the preview collection view
    func loadPreviewFromModel() {
        
        // Show the preview only if a connection is established, a message as been sent since this connection but no preview is active
        if (model.connectionState == "connected" && model.messageSentSinceConnected && !showPreview) {
            
            // get the last sent message (if there is only one element get the first entry because there is no last entry)
            let lastMessage = model.history.last ?? model.history.first
            
            // Create a formatter to make the message timestamp more understandable
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            
            // A preview will be displayed
            showPreview = true
            
            // Display the message name or a default text
            previewLabel.text = lastMessage?.messageName ?? "keine Nachricht"
            
            // If a message name is shown display more information of the message
            if previewLabel.text != "keine Nachricht" {
                
                previewInfoLabel.text = "\(lastMessage?.messageType ?? "") \(formatter.string(from: lastMessage?.timestamp ?? Date()))"
                
            }
            
            // If the content of the last message is not nil (null)
            if lastMessage?.messageContent != nil {
            
                // If the last message is a free message proceed just this part of the function
                if lastMessage?.messageType == "Free" {
                    
                    // Get the message in its binary representation
                    let binaryMessage = lastMessage!.messageContent!
                    
                    // Iterate over all binary digits digit by digit and if it is selected (1) the cell background on the equivalent position to gets red
                    for n in 0..<binaryMessage.count {
                        
                        // The range of the binary sequence to check
                        let range = binaryMessage.index(binaryMessage.startIndex, offsetBy: n)..<binaryMessage.index(binaryMessage.startIndex, offsetBy: n + 1)
                        
                        // Get the binary digit
                        let partOfBinaryMessageToConvert = String(binaryMessage[range])
                        
                        // Get the cell
                        let cell = previewCollectionView!.cellForItem(at: IndexPath(row: n, section: 0))
                        
                        // If its selected (1) set the background color of the cell to red
                        if (partOfBinaryMessageToConvert == "1") {
                            
                            cell?.backgroundColor = UIColor.red
                            
                        } else {
                            
                            cell?.backgroundColor = UIColor.white
                            
                        }
                        
                    }
                    
                }
            
                // If the last message is a text message proceed this part of the function
                else if lastMessage?.messageType == "Text" {
                
                    // Reset the whole preview to clear the old preview
                    for n in 0..<numberOfCells {
                        
                        previewCollectionView!.cellForItem(at: IndexPath(row: n, section: 0))?.backgroundColor = UIColor.white
                        
                    }
                    
                    // Get the message content
                    let binaryMessage = lastMessage!.messageContent!
                    
                    // Number of cols per row to decide if the message (preview) is shown a loop or in the center of the LED matrix
                    let cellsPerRow = binaryMessage.count / rows
                    
                    // in the matrix center not in a loop
                    if binaryMessage.count <= numberOfCells {
                        
                        // Iterate over all binary digits digit by digit and if it is selected (1) the cell background on the equivalent position to gets red
                        for n in 0..<binaryMessage.count {
                            
                            // The range of the binary sequence to check
                            let range = binaryMessage.index(binaryMessage.startIndex, offsetBy: n)..<binaryMessage.index(binaryMessage.startIndex, offsetBy: n + 1)
                            
                            // Get the binary digit
                            let partOfBinaryMessageToConvert = String(binaryMessage[range])
                            
                            // Center the message
                            let cellNumber = (cols * (n / cellsPerRow)) + (n % cellsPerRow) + ((cols - cellsPerRow) / 2)
                            
                            // Get the cell
                            let cell = previewCollectionView!.cellForItem(at: IndexPath(row: cellNumber, section: 0))
                            
                            // If its selected (1) set the background color of the cell to red
                            if (partOfBinaryMessageToConvert == "1") {
                                
                                cell?.backgroundColor = UIColor.red
                                
                            } else {
                                
                                cell?.backgroundColor = UIColor.white
                                
                            }
                            
                        }
                        
                    }
                        
                    // in a loop not in the center
                    else {
                        
                        // Without creating a thread the loop animation would freeze the app this way the user can not interact with the app anymore
                        let queue = DispatchQueue(label: "previewQueue")
                        queue.async {
                            
                            // Will contain a binary string which is 7 by 32 binary digits long
                            var adaptedMessage = ""
                            
                            // While the tab is open and a connection is established
                            while self.showPreview { // while the preview is on the screen
                                
                                // offset means the number of colums the shown message is shifted to the left on the (preview) matrix. This for-loop provides a full loop cycle.
                                for offset in 0..<cellsPerRow {
                                    
                                    // If the connection to the LED matrix breaks or another app tab is selected stop the loop animation
                                    if !self.showPreview {
                                        break
                                    }
                                    
                                    // The idea is to cut off digits at the beginning and at the end and put some start digits to the end depending on the offset to emulate a endless message cycle
                                    
                                    // Adapt row for row
                                    for row in 0..<self.rows {
                                        
                                        // The range of the next row to adapt
                                        let rowRange = binaryMessage.index(binaryMessage.startIndex, offsetBy: row*cellsPerRow)..<binaryMessage.index(binaryMessage.startIndex, offsetBy: (row + 1)*cellsPerRow)
                                        
                                        // Get the row to adapt
                                        let unadaptedRow = String(binaryMessage[rowRange])
                                        
                                        // Will contain the adapted row
                                        var adaptedRow = ""
                                        
                                        // Calculate the number of digits to get removed at the end depending on the number of excess columns and the offset. If the offset is still small enough so some end digits have to get removed this value will be negative
                                        let numberOfDigitsToRemoveAtTheEnd = self.cols - cellsPerRow + offset
                                        
                                        // If some columns at the end have to be cut off
                                        if numberOfDigitsToRemoveAtTheEnd < 0 {
                                            
                                            // The range of the columns that are preserved
                                            let rowRangeUntilCut = unadaptedRow.index(unadaptedRow.startIndex, offsetBy: 0)..<unadaptedRow.index(unadaptedRow.endIndex, offsetBy: numberOfDigitsToRemoveAtTheEnd)
                                            
                                            // Set the adapted row to the cutted off row
                                            adaptedRow.append(contentsOf: unadaptedRow[rowRangeUntilCut])
                                            
                                        }
                                        
                                        // If the last column (a digit of the row) should be displayed set the unadapted row to the adapted row
                                        else {
                                            
                                            adaptedRow.append(contentsOf: unadaptedRow)
                                            
                                        }
                                        
                                        // Now the adapted row contains the right message end digits that should get displayed - BUT maybe it is still to long and some start digits have to be cut off or have to get putted behind the last digits to emulate a continous message loop
                                        
                                        // If the offset is greater than 0 some start digits have to be removed or get putted behind the last digits to emulate a continous message loop
                                        if offset > 0 {
                                            
                                            // If the offset is big enough so some start digits have to get displayed at the end this value shows how much start digits should get append at the end
                                            if numberOfDigitsToRemoveAtTheEnd > 0 {
                                                
                                                // The range of start digits that should get displayed at the end
                                                let rowRangeAppendAtTheEnd = unadaptedRow.index(unadaptedRow.startIndex, offsetBy: 0)..<unadaptedRow.index(unadaptedRow.startIndex, offsetBy: numberOfDigitsToRemoveAtTheEnd)
                                                
                                                // Append the corresponding digits at the end
                                                adaptedRow.append(contentsOf: unadaptedRow[rowRangeAppendAtTheEnd])
                                                
                                            }
                                            
                                            // Now the adapted row contains the right message end digits that should get displayed AND the start digits that should get display behind the end digit to emulate a continous message loop - BUT maybe it is still to long and some start digits have to get cut off
                                            
                                            // Remove the start digits that have to get cut off
                                            for _ in 0..<offset {
                                                adaptedRow.removeFirst()
                                            }
                                            
                                        }
                                        
                                        // Now a new row got adapted and it can be added to the variable containing the adapted message
                                        adaptedMessage.append(contentsOf: adaptedRow)
                                        
                                    }
                                    
                                    // Display the adapted message which fits exactly the LED matrix width. Show the preview
                                    // Checks digit by digits if the equivalent cell should turns into red or not
                                    for cellNumber in 0..<adaptedMessage.count {
                                        
                                        // If the cell should be turned into red turn it into red
                                        if (adaptedMessage[adaptedMessage.index(adaptedMessage.startIndex, offsetBy: cellNumber)] == "1") {
                                            
                                            // UI changes are allowed in the main thread only
                                            DispatchQueue.main.async { () -> Void in
                                                
                                                self.previewCollectionView!.cellForItem(at: IndexPath(row: cellNumber, section: 0))?.backgroundColor = UIColor.red
                                                
                                            }
                                            
                                        }
                                        
                                        // If the cell should not be turned into red turn it into white to be sure that is really white. Normally it should be white at the moment anyway
                                        else {
                                            
                                            // UI changes are allowed in the main thread only
                                            DispatchQueue.main.async { () -> Void in
                                                
                                                self.previewCollectionView!.cellForItem(at: IndexPath(row: cellNumber, section: 0))?.backgroundColor = UIColor.white
                                                
                                            }
                                        }
                                    }
                                    
                                    // Reset the adapted message to be ready for the next offset increment
                                    adaptedMessage = ""
                                    
                                    // Realize the loop velocity
                                    usleep(useconds_t(1000000 / (Int(self.model.loopVelocity ?? self.model.initialLoopVelocity) + 1)) )
                                    
                                } // end for offset
                                
                            } // end while showPreview
                            
                        } // end logic of the new thread
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
