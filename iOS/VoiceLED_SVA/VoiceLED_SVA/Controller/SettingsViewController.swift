//
//  SettingsViewController.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 16.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import UIKit
import CoreData

/// This controller provides the functionality behind the view tab the user can change the settings
class SettingsViewController: UIViewController {
    
    /// Holds the instance of the ModelManager which stores all important data
    let model = ModelManager.sharedInstance
    
    /// Enables the user to edit the loop velocity of a text message
    @IBOutlet var loopVelocitySlider: UISlider!
    
    /// Shows the velicity set on the slider
    @IBOutlet var loopVelocityLabel: UILabel!
    
    /// Enables the user to cancel the unsaved velocity changes
    @IBOutlet var loopVelocityCancelButton: UIButton!
    
    /// Enables the user to reset the velocity
    @IBOutlet var loopVelocityResetButton: UIButton!
    
    /// Enables the user to save the unsaved velicity changes
    @IBOutlet var loopVelocitySaveButton: UIButton!
    
    /// The user can activate this switch to send the last message automatically after connect to the matrix
    @IBOutlet var autoSendButton: UISwitch!
    
    /// Saves the button state in the model / core data
    @IBAction func valueChangedActionAutoSendButton(_ sender: UISwitch) {
        
        if (sender.isOn == true) {
            
            model.save(autoSend: true)
            
        } else {
            
            model.save(autoSend: false)
            
        }
        
    }
    
    /// Updates the loop velocity label and the shown buttons and provides the sliding steps
    @IBAction func valueChangedActionLoopVelocitySlider(_ sender: Any) {
        
        updateLoopVelocityValue()
        
    }
    
    /// Sets the loop velocity back to the last saved velocity saved in the model / Core Data
    @IBAction func touchUpInsideActionLoopVelocityCancelButton(_ sender: Any) {
        
        loadCoreDataLoopVelocity()
        
    }
    
    /// Set the unsaved loop velocity value back to an initial/default value stored in the model
    @IBAction func touchUpInsideActionLoopVelocityReset(_ sender: Any) {
        
        loadInitialLoopVelocity()
        
    }
    
    /// Saves the selected value into the model / Core Data and updates the shown buttons
    @IBAction func touchUpInsideActionLoopVelocitySave(_ sender: Any) {
        
        // Saves the selected value into the model / Core Data
        model.save(loopVelocity: loopVelocitySlider.value)
        
        // Updates the shown buttons
        checkButtonAvailibility()
        
        // Send the last message with the new velocity if it is a text message
        if (model.history.last?.messageType ?? model.history.first?.messageType == "Text") {
            
            model.communicator?.send(textMessage: model.history.last?.messageName ?? model.history.first?.messageName ?? "", andLoopVelocity: Int(model.loopVelocity ?? model.initialLoopVelocity))
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // load all settings stored in the model / Core Data
        loadCoreDataLoopVelocity()
        loadCoreDataAutoSend()
        
    }
    
    /// Updates the loop velocity label and the shown buttons and provides the sliding steps
    func updateLoopVelocityValue(){
        
        // Update the loop velocity label
        switch Int(round(loopVelocitySlider.value)) {
        case 0:
            loopVelocityLabel.text = "sehr langsam"
        case 1:
            loopVelocityLabel.text = "langsam"
        case 3:
            loopVelocityLabel.text = "schnell"
        case 4:
            loopVelocityLabel.text = "sehr schnell"
        default:
            loopVelocityLabel.text = "normal"
        }
        
        // Provide the sliding steps
        loopVelocitySlider.setValue(round(loopVelocitySlider.value), animated: false)
        
        // Update the shown buttons
        checkButtonAvailibility()
        
    }
    
    /// Updates the shown buttons
    func checkButtonAvailibility(){
        
        // If the value is not equal to saved velocity show the "save" and "cancel" button
        if Double(loopVelocitySlider.value) == model.dataService[0].loopVelocity {
            setLoopVelocitySaveAndCancelEnabled(enable: false)
            setLoopVelocitySaveAndCancelHidden(hide: true)
        } else {
            setLoopVelocitySaveAndCancelEnabled(enable: true)
            setLoopVelocitySaveAndCancelHidden(hide: false)
        }
        
        // If the value is not equal to initial / default velocity value show the "reset" button
        if Double(loopVelocitySlider.value) == model.initialLoopVelocity {
            
            setLoopVelocityResetEnabled(enable: false)
            setLoopVelocityResetHidden(hide: true)
            
        } else {
            
            setLoopVelocityResetEnabled(enable: true)
            setLoopVelocityResetHidden(hide: false)
            
        }
        
    }
    
    /// Loads the in Core Data saved loop velocity and updates the view
    func loadCoreDataLoopVelocity(){
        
        loopVelocitySlider.setValue(Float(model.dataService[0].loopVelocity), animated: true)
        updateLoopVelocityValue()
        
    }
    
    /// Set the unsaved loop velocity value back to an initial/default value stored in the model
    func loadInitialLoopVelocity(){
        
        loopVelocitySlider.setValue(Float(model.initialLoopVelocity), animated: true)
        updateLoopVelocityValue()
        
    }
    
    /// Loads the in Core Data saved auto send setting and updates the view
    func loadCoreDataAutoSend() {
        
        autoSendButton.setOn(model.dataService[0].autoSend, animated: false)
        
    }
    
    /// Enables the "save" and "cancel" button. These are combined because they are enabled or hidden together everytime to save the new value or cancel the change
    func setLoopVelocitySaveAndCancelEnabled(enable: Bool){
        
        loopVelocitySaveButton.isEnabled = enable
        loopVelocityCancelButton.isEnabled = enable
        
    }
    
    /// Enables the "reset" button
    func setLoopVelocityResetEnabled(enable: Bool){
        
        loopVelocityResetButton.isEnabled = enable
        
    }
    
    /// Hides the "save" and "cancel" button. These are combined because they are enabled or hidden together everytime to save the new value or cancel the change
    func setLoopVelocitySaveAndCancelHidden(hide: Bool){
        
        loopVelocitySaveButton.isHidden = hide
        loopVelocityCancelButton.isHidden = hide
        
    }
    
    /// Hides the "reset" button
    func setLoopVelocityResetHidden(hide: Bool){
        
        loopVelocityResetButton.isHidden = hide
        
    }
    
}
