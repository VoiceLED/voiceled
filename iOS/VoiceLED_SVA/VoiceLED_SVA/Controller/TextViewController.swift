//
//  TextViewController.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 18.11.18.
//  Copyright © 2018 Alexander Wiemer. All rights reserved.
//

import UIKit
import CoreData

/// This controller provides the functionality behind the view tab the user can edit and send text messages
class TextViewController: UIViewController, UITextFieldDelegate {
    
    /// Holds the instance of the ModelManager which stores all important data
    let model = ModelManager.sharedInstance
    
    /// There the message can be typed
    @IBOutlet var textMessageInputTextField: UITextField!
    
    /// Shows an example message which is selectable with a click on the button
    @IBOutlet var textMessageSuggestion1: UIButton!
    /// Shows an example message which is selectable with a click on the button
    @IBOutlet var textMessageSuggestion2: UIButton!
    /// Shows an example message which is selectable with a click on the button
    @IBOutlet var textMessageSuggestion3: UIButton!
    /// Shows an example message which is selectable with a click on the button
    @IBOutlet var textMessageSuggestion4: UIButton!
    /// Shows an example message which is selectable with a click on the button
    @IBOutlet var textMessageSuggestion5: UIButton!
    
    /// Copies the example message into the user input text field and updates the current input message in the model
    @IBAction func touchUpInsideActionTextMessageSuggestion1Button(_ sender: Any) {
        textMessageInputTextField.text = model.textMessageSuggestion1
        setCurrentTextInput()
    }
    /// Copies the example message into the user input text field and updates the current input message in the model
    @IBAction func touchUpInsideActionTextMessageSuggestion2Button(_ sender: Any) {
        textMessageInputTextField.text = model.textMessageSuggestion2
        setCurrentTextInput()
    }
    /// Copies the example message into the user input text field and updates the current input message in the model
    @IBAction func touchUpInsideActionTextMessageSuggestion3Button(_ sender: Any) {
        textMessageInputTextField.text = model.textMessageSuggestion3
        setCurrentTextInput()
    }
    /// Copies the example message into the user input text field and updates the current input message in the model
    @IBAction func touchUpInsideActionTextMessageSuggestion4Button(_ sender: Any) {
        textMessageInputTextField.text = model.textMessageSuggestion4
        setCurrentTextInput()
    }
    /// Copies the example message into the user input text field and updates the current input message in the model
    @IBAction func touchUpInsideActionTextMessageSuggestion5Button(_ sender: Any) {
        textMessageInputTextField.text = model.textMessageSuggestion5
        setCurrentTextInput()
    }
    /// Call the method which sends the user input
    @IBAction func touchUpInsideActionSendTextMessageButton(_ sender: Any) {
        sendTextMessage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // It's necessary for leaving the keyboard with "Return"
        textMessageInputTextField.delegate = self
        
        // Set the button titles to the example messages
        loadSuggestionButtons()
        
        // Enables Siri Shortcut to transfer its message input into the app
        siriService.set(viewcontroller: self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Set the user input text field to the current input value stored in the model
        refreshTextInput()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Store the current user input in the model (name and free input)
        setCurrentTextInput()
        
    }
    
    /// Creates a new text message in the model and reset the user input text field so the user could type the next message without deleting the old one
    func sendTextMessage() {
        
        model.create(TextMessage: textMessageInputTextField.text ?? "")
        textMessageInputTextField.text = ""
        
    }
    
    /// Called with "Return" on the keyboard. Stores the current user input in the model and remove the keyboard from the screen
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        setCurrentTextInput()
        return true
        
    }
    
    /// Sets the user input text field text to the latest user input text stored in the model
    func refreshTextInput() {
        
        textMessageInputTextField.text = model.currentTextInput
        
    }
    
    /// Stores the current user input text field text in the model
    func setCurrentTextInput() {
        
        model.currentTextInput = textMessageInputTextField.text ?? ""
        
    }
    
    /// Sets the button titles to the example messages stored in the model
    func loadSuggestionButtons() {
        
        textMessageSuggestion1.setTitle(model.textMessageSuggestion1, for: .normal)
        textMessageSuggestion2.setTitle(model.textMessageSuggestion2, for: .normal)
        textMessageSuggestion3.setTitle(model.textMessageSuggestion3, for: .normal)
        textMessageSuggestion4.setTitle(model.textMessageSuggestion4, for: .normal)
        textMessageSuggestion5.setTitle(model.textMessageSuggestion5, for: .normal)
        
    }
    
}
