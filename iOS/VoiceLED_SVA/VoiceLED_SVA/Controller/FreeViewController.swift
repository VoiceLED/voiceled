//
//  FreeViewController.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 16.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import UIKit
import CoreData

/// This controller provides the functionality behind the view tab the user can edit and send free messages
class FreeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    
    /// Holds the instance of the ModelManager which stores all important data
    let model = ModelManager.sharedInstance
    
    /// Represents the number of matrix columns stored in the model. Zero (0) is just a default value and will be updated in viewDidLoad()
    var cols = 0
    
    /// Represents the number of matrix rows stored in the model. Zero (0) is just a default value and will be updated in viewDidLoad()
    var rows = 0
    
    /// The number of available matrix cells or LEDs. Zero (0) is just a default value. The right value will be calculated in viewDidLoad(): rows * cols
    var numberOfCells = 0
    
    /// Contains the free user input in the same representation the messages will be sent. A sequence of the rows with binary digits as a string
    var messageArrayAsBinaryString = ""
    
    /// There the name of the free input can be typed
    @IBOutlet var freeMessageNameLabel: UITextField!
    
    /// Contains a sequence of cells (LEDs) over several lines and looks like a matrix but is not. The cells contain invisible buttons which change the background color of the specific cell to show which cells are selected
    @IBOutlet var collectionView: UICollectionView!
    
    /// Creates a new free message in the model and reset the user inputs so the user could create the next message without deleting the old one
    @IBAction func touchUpInsideActionSendFreeMessageButton(_ sender: Any) {
        
        // Store the current user input in the model (name and free input)
        transferDataIntoModel()
        
        // Create the free message
        model.createFreeMessage(withName: freeMessageNameLabel.text ?? "", andMessage: messageArrayAsBinaryString)
        
        // Reset the view
        resetUI()
        
    }
    
    /// Resets the view
    @IBAction func touchUpInsideActionClearButton(_ sender: Any) {
        
        // Reset the view
        resetUI()
        
    }
    
    override func viewDidLoad() {
        
        // It's necessary for leaving the keyboard with "Return"
        freeMessageNameLabel.delegate = self
        
        // Save the values stored in the model for quicker accessibility
        cols = model.COLS
        rows = model.ROWS
        
        // Calculate the number of cells (LEDs) the collection view (matrix) should have
        numberOfCells = cols * rows
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Set the user input to the current input value stored in the model
        loadDataFromModel()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Store the current user input in the model (name and free input) and reset the view to be ready for another free input to show / load if the view appears again
        transferDataIntoModel()
        resetUI()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // Set number of cells the collection view have to the number of cells (LEDs) the matrix have
        return (numberOfCells)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Add one cell to the collection view
        return collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FreeViewCell
        
    }
    
    /// Called with "Return" on the keyboard. Removes the keyboard from the screen
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    /// Stores the current user input in the model (name and free input)
    func transferDataIntoModel() {
        
        // Convert the collection view cell sequence to a specification conform binary string
        convertCollectionViewToBinaryString()
        
        // Store the current user input in the model (name and free input)
        model.freeInput = messageArrayAsBinaryString
        model.freeInputName = freeMessageNameLabel.text ?? ""
    }
    
    /// Reset the view to be prepared for a new next input
    func resetUI() {
        
        // Clear the free input name
        freeMessageNameLabel.text = ""
        
        // Set the background color back to white (unselected) if it is red (selected)
        for cellnumber in 0..<numberOfCells {
            
            let cell = collectionView!.cellForItem(at: IndexPath(row: cellnumber, section: 0))
            
            if (cell?.backgroundColor == UIColor.red) {
                
                cell?.backgroundColor = UIColor.white
                
            }
        }
        
    }
    
    /// Set the user input to the current input value stored in the model (name and free input)
    func loadDataFromModel(){
        
        freeMessageNameLabel.text = model.freeInputName
        let binaryMessage = model.freeInput
        
        for n in 0..<binaryMessage.count {
            
            let range = binaryMessage.index(binaryMessage.startIndex, offsetBy: n)..<binaryMessage.index(binaryMessage.startIndex, offsetBy: n + 1)
            
            let partOfBinaryMessageToConvert = String(binaryMessage[range])
            
            let cellNumber = ((((n % cols) + 1) * rows ) - (1 + (n / cols)))
            
            if (partOfBinaryMessageToConvert == "1") {
                
                let cell = collectionView!.cellForItem(at: IndexPath(row: cellNumber, section: 0))
                cell?.backgroundColor = UIColor.red
                
            }
        }
        
        // Convert the collection view cell sequence to a specification conform binary string
        convertCollectionViewToBinaryString()
        
    }
    
    /// Convert the collection view cell sequence to a specification conform binary string
    func convertCollectionViewToBinaryString() {
        
        messageArrayAsBinaryString = ""
        
        /// Represents the free user input in a horizantal matrix like the real world matrix (for example 7 rows and 32 columns)
        var messageArray = [[Int]]()
        
        // fill up the free user input representing matrix depending on the number of rows and columns the matrix have
        for _ in 0..<rows {
            var messageRow = [Int]()
            for _ in 0..<cols {
                messageRow.append(0)
            }
            messageArray.append(messageRow)
        }
        
        // Convert the the collection view with the free user input cell by cell into a two dimonsional array which is like the matrix (messageArray)
        for cellnumber in 0..<numberOfCells {
            
            // get the cell and check if its selected. If its selected the corresponding array element will be set from 0 to 1 (from unselected to selected)
            if (collectionView!.cellForItem(at: IndexPath(row: cellnumber, section: 0))?.backgroundColor == UIColor.red) {
                messageArray[(rows - 1) - (cellnumber % rows)][cellnumber / rows] = 1
            }
            
        }
        
        // Convert the two dimensional array (matrix) into a specification conform binary string
        for row in 0..<messageArray.count {
            
            // Will be equal to one row of the two dimensional array (matrix)
            var partOfmessageArrayAsBinaryString = ""
            
            // Read the matrix row and build the binary row string
            for col in 0..<messageArray[row].count {
                if messageArray[row][col] != 1 {
                    partOfmessageArrayAsBinaryString.append("0")
                } else {
                    partOfmessageArrayAsBinaryString.append("1")
                }
            }
            
            // Add the binary row to the row sequence
            messageArrayAsBinaryString += partOfmessageArrayAsBinaryString
        }
        
    }
    
}
