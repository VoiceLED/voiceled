//
//  HistoryViewController.swift
//  VoiceLED_SVA
//
//  Created by Alexander Wiemer on 16.01.19.
//  Copyright © 2019 Alexander Wiemer. All rights reserved.
//

import UIKit
import CoreData

/// This controller provides the functionality behind the view tab the user can see and edit the message history
class HistoryViewController: UITableViewController {
    
    /// Holds the instance of the ModelManager which stores all important data
    let model = ModelManager.sharedInstance
    
    /// Contains message entries (cells with sent messages)
    @IBOutlet var historyTable: UITableView!
    
    /// Delete all history entries
    @IBAction func touchUpInsideActionClearHistoryButton(_ sender: Any) {
        clearHistory()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Set the number of table entries to the number of stored messages
        return model.history.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        // Create a new cell to add it to the table at the end of this function
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableCell", for: indexPath) as! HistoryTableCell
        
        // Create a formatter to make the message timestamp more understandable
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        // The messages are stored in a kind of array. A new stored message will be added at the end of the history array. To show the last message at first in this view the messages must be read out the other way round.
        let message = model.history[model.history.count - indexPath.row - 1]
        
        // Add the message name to the cell
        cell.name.text = message.messageName
        
        // Add the message type and sent date to the cell
        cell.information.text = "\(message.messageType ?? "") \(formatter.string(from: message.timestamp ?? Date()))"
        
        // Add the cell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        // Make an entry editable with a swipe
        return true
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        // Begin to update the table (entry)
        historyTable.beginUpdates()
        
        // Remove the chosen message from the stored message history
        model.removeMessageFromHistory(atIndex: model.history.count - indexPath.row - 1)
        
        // Refresh the history table data
        refreshHistory()
        
        // Remove the cell in the UI
        historyTable.deleteRows(at: [indexPath], with: .fade)
        
        // Finish updating
        historyTable.endUpdates()
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Get the index of a selected/clicked message
        let storedMessage = model.history.count - indexPath.row - 1
        
        // Check if it is a text or free input message
        if (model.history[storedMessage].messageType == "Text") {
            
            // Set the selected message to the current text input to be able to edit or send the message in the text view
            model.setCurrentTextInputFromStoredMessage(index: storedMessage)
            
            // Open the text view
            self.tabBarController?.selectedIndex = 0
            
        } else if (model.history[storedMessage].messageType == "Free"){
            
            // Set the selected message to the current free input to be able to edit or send the message in the free view
            model.setCurrentFreeInputFromStoredMessage(index: storedMessage)
            
            // Open the free view
            self.tabBarController?.selectedIndex = 1
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Make sure that the table is up to date everytime it appears
        refreshHistory()
        
    }
    
    /// Updates the message history in the model and the history in the HistoryViewController
    func refreshHistory() {
        
        // Refresh the history stored in the model
        model.refreshHistory()
        
        // Update the table view
        historyTable.reloadData()
        
    }
    
    /// Deletes the whole history
    func clearHistory() {
        
        // Begin to update the table
        historyTable.beginUpdates()
        
        // To know how many message have to be removed
        let oldHistoryLength = model.history.count
        
        // Remove the messages in the model
        for n in 0..<oldHistoryLength {
            model.removeMessageFromHistory(atIndex: n)
        }
        
        // Refresh the history table data
        refreshHistory()
        
        // Remove the cells in the UI
        for n in 0..<oldHistoryLength {
            historyTable.deleteRows(at: [IndexPath(row: n, section: 0)], with: .fade)
        }
        
        // Finish updating
        historyTable.endUpdates()
        
    }

}
