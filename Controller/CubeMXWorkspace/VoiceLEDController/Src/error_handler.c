/*
 * error_handler.c
 *
 *  Created on: 30.04.2019
 *      Author: Joshua
 */

#include "error_handler.h"

// ===========================
// CONSTANTS
// ===========================



// ===========================
// GLOBAL VARIABLES
// ===========================

// default error id is no error
uint16_t error_id = 0;
uint16_t last_id = 0;
int id_has_changed = 0;

// ===========================
// FUNCTIONS
// ===========================


/*
 * Sets the current error to error id
 */
void setErrorId(uint16_t id)
{

	if(id == last_id)
	{
		id_has_changed = 0;
	}
	else
	{
		id_has_changed = 1;
	}
	last_id = error_id;
	error_id = id;
}

/*
 * Reset the current error to no error (zero)
 */
void resetErrorId()
{
	setErrorId(ERR_NO_ERROR);
}

/*
 * Returns error id
 */
uint16_t getErrorId()
{
	return error_id;
}

/*
 * Returns if the current error id is different from the last one
 */
int hasErrorIdChanged()
{
	return id_has_changed;
}
