/*
 * dwt_stm32_delay.c
 *
 *  Created on: 30.04.2019
 *      Author: Joshua
 */

#include "dwt_stm32_delay.h"

uint32_t getUs(void) {

	uint32_t usTicks = HAL_RCC_GetSysClockFreq() / 1000000;


	register uint32_t ms, cycle_cnt;


	do {

		ms = HAL_GetTick();
		cycle_cnt = SysTick->VAL;


	} while (ms != HAL_GetTick());


return (ms * 1000) + (usTicks * 1000 - cycle_cnt) / usTicks;

}

/**
 * Delays cpu for microseconds
 * Code from here: https://community.st.com/s/question/0D50X00009XkeRYSAZ/delay-in-us
 */
void delayUS(uint16_t micros) {


	uint32_t start = getUs();


	while (getUs()-start < (uint32_t) micros) {
		asm("nop");
	}

}
