/*
 * ble_handler.c
 *
 *  Created on: 25.04.2019
 *      Author: Joshua
 */

#include "ble_handler.h"

// ===========================
// CONSTANTS
// ===========================

// UUIDs
const uint8_t MATRIX_SERVICE_UUID[16] = 			{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x50,0xF5,0x77,0xEB};
const uint8_t MATRIX_HEADER_CHAR_UUID[16] = 		{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x51,0xF5,0x77,0xEB};
const uint8_t MATRIX_DATA_CHAR_UUID[16] = 			{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x52,0xF5,0x77,0xEB};
const uint8_t META_CONTROLLER_SERVICE_UUID[16] =	{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x60,0xF5,0x77,0xEB};
const uint8_t META_CONTROLLER_TEMP_CHAR_UUID[16] = 	{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x61,0xF5,0x77,0xEB};
const uint8_t ERR_SERVICE_UUID[16] =	{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x70,0xF5,0x77,0xEB};
const uint8_t ERR_ID_CHAR_UUID[16] = 	{0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x71,0xF5,0x77,0xEB};

// for temperature calculation
#define TEMP110_CAL_VALUE	((uint16_t*)((uint32_t)0x1FFF7A2E))
#define TEMP30_CAL_VALUE    ((uint16_t*)((uint32_t)0x1FFF7A2C))
#define TEMP110		110.0f
#define TEMP30		30.0f

// matrix default properties
#define DEFAULT_ARRAY_SIZE 336
#define DEFAULT_VELOCITY 500
#define SLOWEST_VELOCITY 1000
uint8_t DEFAULT_MATRIX[DEFAULT_ARRAY_SIZE] = {
	1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0,
	1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
	1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
	1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0,
	1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
	0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
	0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0
};
// ===========================
// GLOBAL VARIABLES
// ===========================

// variables for ble connection and notification
uint32_t connected = FALSE;
uint8_t set_connectable = 1;
uint16_t connection_handle = 0;
uint8_t notification_enabled = FALSE;

// ble message properties
int msg_received = 0;
uint8_t matrix_msgnum = 0;

// ble handles for services and characteristics
uint16_t matrixServHandle, matrixHeaderCharHandle, matrixDataCharHandle,
	metaControllerHandle, metaControllerTempCharHandle,
	errHandle, errIdCharHandle;

// handle for the analog digital converter (used for temperature sensor data
ADC_HandleTypeDef hadc1;
// from adc read in value (is global because if adc does not return ok, the old value will be used)
double v_sense = 0;
int8_t last_temperature = 0;

// VARIABLES FOR THE MATRIX:
// defaults
uint8_t* matrix = DEFAULT_MATRIX;
uint16_t matrix_len = DEFAULT_ARRAY_SIZE;
uint32_t matrixVelocity = DEFAULT_VELOCITY;
int isDefaultMatrix = 1;
// in building matrix (to not overwrite current matrix)
uint8_t* new_matrix;
uint16_t new_matrix_len = 0;
uint32_t new_matrixVelocity = 0;
int shouldResetOffset = 1;

// ===========================
// FUNCTIONS
// ===========================

/**
 * Add the ble services:
 * - Matrix Service:
 *    - Matrix Data Characteristic
 *    - Matrix Header Characteristic
 * - Meta Controller Service:
 *    - Meta Controller Temp Characteristic
 *
 */
tBleStatus Add_Services(void)
{
	tBleStatus ret;

	// add matrix service
	ret = aci_gatt_add_serv(UUID_TYPE_128,  MATRIX_SERVICE_UUID, PRIMARY_SERVICE, 10,
			&matrixServHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;

	// add matrix header characteristic
    ret =  aci_gatt_add_char(matrixServHandle, UUID_TYPE_128, MATRIX_HEADER_CHAR_UUID, 4,
    		CHAR_PROP_WRITE | CHAR_PROP_WRITE_WITHOUT_RESP, ATTR_PERMISSION_NONE,
  			GATT_NOTIFY_ATTRIBUTE_WRITE,
  			16, 0, &matrixHeaderCharHandle);
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    // add matrix data characteristic
    ret =  aci_gatt_add_char(matrixServHandle, UUID_TYPE_128, MATRIX_DATA_CHAR_UUID, 20,
    		CHAR_PROP_WRITE | CHAR_PROP_WRITE_WITHOUT_RESP, ATTR_PERMISSION_NONE,
			GATT_NOTIFY_ATTRIBUTE_WRITE,
			16, 0, &matrixDataCharHandle);
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    // add meta controller service
    ret = aci_gatt_add_serv(UUID_TYPE_128,  META_CONTROLLER_SERVICE_UUID, PRIMARY_SERVICE, 10,
    		&metaControllerHandle);
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    // add meta controller temperature characteristic
    ret =  aci_gatt_add_char(metaControllerHandle, UUID_TYPE_128, META_CONTROLLER_TEMP_CHAR_UUID, 1,
            CHAR_PROP_READ, ATTR_PERMISSION_NONE,
			GATT_DONT_NOTIFY_EVENTS,
                             16, 0, &metaControllerTempCharHandle);
    if (ret != BLE_STATUS_SUCCESS) goto fail;

    // add err service
	ret = aci_gatt_add_serv(UUID_TYPE_128,  ERR_SERVICE_UUID, PRIMARY_SERVICE, 10,
			&errHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;

    // add err id characteristic
	ret =  aci_gatt_add_char(errHandle, UUID_TYPE_128, ERR_ID_CHAR_UUID, 2,
			CHAR_PROP_READ, ATTR_PERMISSION_NONE,
			GATT_DONT_NOTIFY_EVENTS,
							 16, 0, &errIdCharHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;

	return BLE_STATUS_SUCCESS;

	fail:
		PRINTF("Error while adding services.\n");
		return BLE_STATUS_ERROR ;

}

/**
 * Update temperature characteristic value
 */
tBleStatus Temp_Update()
{
	// get temperature with ADC
	HAL_ADC_Start(&hadc1);

	// poll for 1000 millisec
	HAL_StatusTypeDef ret = HAL_ADC_PollForConversion(&hadc1, 1000);
	// only use values if adc does not error
	if(ret == HAL_OK)
	{
		v_sense = HAL_ADC_GetValue(&hadc1);
	}
	else
	{
		setErrorId(ERR_HAL_ADC_POLL);
	}

	HAL_ADC_Stop(&hadc1);

	// calculate temperature
	int8_t temperature = (int8_t)((TEMP110 - TEMP30) / ((float)(*TEMP110_CAL_VALUE) - (float)(*TEMP30_CAL_VALUE)) *
			 (v_sense - (float)(*TEMP30_CAL_VALUE)) + TEMP30);

	// update ble temperature characteristic if it changed

	if(temperature!=last_temperature)
	{
		tBleStatus ret;

		  ret = aci_gatt_update_char_value(metaControllerHandle, metaControllerTempCharHandle,
				  // offset
				  0,
				  // length
				  1,
				  // data
				  (uint16_t*)&temperature);

		  last_temperature = temperature;

		  if (ret != BLE_STATUS_SUCCESS){
		    PRINTF("Error while updating TEMP characteristic.\n") ;
		    return BLE_STATUS_ERROR ;
		  }

		  return BLE_STATUS_SUCCESS;
	}
	else
	{
		return BLE_STATUS_SUCCESS;
	}
}

/**
 * Update error id characteristic value
 */
tBleStatus Update_Error_Id_Characteristic()
{
  tBleStatus ret;

  if(hasErrorIdChanged())
  {
	  uint16_t id = getErrorId();

	  // 2 bytes of data
	  uint8_t data[2];
	  data[0] = id>>8;
	  data[1] = (uint8_t) id;
	  ret = aci_gatt_update_char_value(errHandle, errIdCharHandle,
	  		  // offset
	  		  0,
	  		  // length
	  		  2,
	  		  // data
	  		  (uint16_t*)&data);

	    if (ret != BLE_STATUS_SUCCESS){
	      PRINTF("Error while updating Err Id characteristic.\n") ;
	      return BLE_STATUS_ERROR ;
	    }
	    return BLE_STATUS_SUCCESS;
  }
  else
  {
	  return BLE_STATUS_SUCCESS;
  }

}

/**
 * @brief  Puts the device in connectable mode.
 *         If you want to specify a UUID list in the advertising data, those data can
 *         be specified as a parameter in aci_gap_set_discoverable().
 *         For manufacture data, aci_gap_update_adv_data must be called.
 * @param  None
 * @retval None
 */
/* Ex.:
 *
 *  tBleStatus ret;
 *  const char local_name[] = {AD_TYPE_COMPLETE_LOCAL_NAME,'B','l','u','e','N','R','G'};
 *  const uint8_t serviceUUIDList[] = {AD_TYPE_16_BIT_SERV_UUID,0x34,0x12};
 *  const uint8_t manuf_data[] = {4, AD_TYPE_MANUFACTURER_SPECIFIC_DATA, 0x05, 0x02, 0x01};
 *
 *  ret = aci_gap_set_discoverable(ADV_DATA_TYPE, ADV_INTERV_MIN, ADV_INTERV_MAX, PUBLIC_ADDR,
 *                                 NO_WHITE_LIST_USE, 8, sizeof(local_name), 3, serviceUUIDList, 0, 0);
 *  ret = aci_gap_update_adv_data(5, manuf_data);
 *
 */
void setConnectable(void)
{

	tBleStatus ret;

  	// disable scan response
    hci_le_set_scan_resp_data(0,NULL);
    PRINTF("General Discoverable Mode.\n");

    // to be advertised services (inverse uuid)
    uint8_t serviceUUIDList[] = {AD_TYPE_128_BIT_SERV_UUID_CMPLT_LIST, 0xA5,0xB0,0xF1,0x49,0x25,0xC4, 0xBE,0xB9, 0x0B,0x47, 0x15,0x10, 0x50,0xF5,0x77,0xEB};

    ret = aci_gap_delete_ad_type(AD_TYPE_TX_POWER_LEVEL);

    ret = aci_gap_set_discoverable(ADV_IND, 0, 0, PUBLIC_ADDR, NO_WHITE_LIST_USE,
                                   0, NULL, 17, serviceUUIDList, 0, 0);

    if (ret != BLE_STATUS_SUCCESS) {
      PRINTF("Error while setting discoverable mode (%d)\n", ret);
    }

}

/**
 * This function is called when there is a LE Connection Complete event
 */
void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle)
{
  connected = TRUE;
  connection_handle = handle;

  PRINTF("Connected to device:");
  for(uint32_t i = 5; i > 0; i--){
    PRINTF("%02X-", addr[i]);
  }
  PRINTF("%02X\n", addr[0]);
}

/**
 * This function is called when the peer device gets disconnected
 */
void GAP_DisconnectionComplete_CB(void)
{
  connected = FALSE;
  PRINTF("Disconnected\n");
  // make the device connectable again
  set_connectable = TRUE;
  notification_enabled = FALSE;
}

/**
 * Read request callback
 */
void Read_Request_CB(uint16_t handle)
{
  if(connection_handle != 0)
    aci_gatt_allow_read(connection_handle);
}

/*
 * The Bluetooth Low Energy Matrix Protocol (BLEMP)
 *
 * By the matrix service there are two characteristics provided:
 *  - matrix data characteristic (MDC)
 *  - matrix header characteristic (MHC)
 *
 * Those have to be modified in a specific way to send valid data to the controller:
 *
 * First header data is received using the MHC, which contains:
 * 	- 2 Bytes data specifying the length of the matrix to be received in MDC next
 * 	- 1 Byte data specifying the number of messages n (writes) to be received in MDC next
 * 	- 1 Byte data specifying the velocity for the running lights
 *
 * Second raw binary matrix data is received using the MDC:
 *  - n messages (writes) are received containing 20 Bytes of raw binary data
 *    to be displayed in each message
 *
 */

/*
 * Process a write in ble matrix header characteristic as specified by the BLEMP
 */
void Matrix_Header_Modified(uint16_t handle, uint8_t data_length, uint8_t *att_data)
{
	// more significant byte (+  less significant byte as 0x00)
	uint16_t msb = att_data[0]<<8;
	// less significant byte
	uint8_t lsb = att_data[1];
	// add msbyte and lsbyte with logical or to length of matrix
	new_matrix_len = msb | lsb;

	// allocate new matrix
	new_matrix = (uint8_t*) calloc(new_matrix_len, sizeof(uint8_t));

	// number of data messages to be received
	matrix_msgnum = att_data[2];

	// reset counter for received messages
	msg_received = 0;

	// get velocity mode
	uint8_t velocityMode = att_data[3];

	// only 5 velocity modes supported
	if(velocityMode < 5)
	{
		// slowest velocity is 1000 (1 sec) so devide by received value (0 to 4) +1 were bigger value means faster velocity
		new_matrixVelocity = SLOWEST_VELOCITY / (velocityMode+1);
	}
	// else default
	else
	{
		new_matrixVelocity = DEFAULT_VELOCITY;
	}

}

/*
 * Process a write in ble matrix data characteristic as specified by the BLEMP
 */
void Matrix_Data_Modified(uint16_t handle, uint8_t data_length, uint8_t *att_data)
{

	// increase counter of received messages
	msg_received++;

	// number of bytes and bits to read
	int numBytes = 20;

	if(msg_received == matrix_msgnum)
	{
		// calculate the number of bytes:
		// for each received msg subtract 160 bit from the length of the matrix
		// divide that by 8 to get bytes
		// ceil because the last byte will be padded
		numBytes = ceil((new_matrix_len-(matrix_msgnum-1)*160)/8.0);
	}

	// fill new_matrix
	// per full call filled with 20 Bytes -> 20*8 = 160 Bits
	for (int c = 0; c < numBytes; ++c) {

		int padding = 0;
		// only if last message padding
		if(msg_received == matrix_msgnum)
		{
			if(c == (numBytes-1))
			{
				padding = 8-(new_matrix_len-(matrix_msgnum-1)*160-c*8);
			}
		}

		// 8 bits
		uint8_t bit1 = (att_data[c]) & 0x01;
		uint8_t bit2 = (att_data[c]>>1) & 0x01;
		uint8_t bit3 = (att_data[c]>>2) & 0x01;
		uint8_t bit4 = (att_data[c]>>3) & 0x01;
		uint8_t bit5 = (att_data[c]>>4) & 0x01;
		uint8_t bit6 = (att_data[c]>>5) & 0x01;
		uint8_t bit7 = (att_data[c]>>6) & 0x01;
		uint8_t bit8 = (att_data[c]>>7) & 0x01;

		new_matrix[c*8 + 160*(msg_received-1)] = bit8;
		if(padding<7)
		{
			new_matrix[c*8 + 160*(msg_received-1) +1] = bit7;
		}
		if(padding<6)
		{
			new_matrix[c*8 + 160*(msg_received-1) +2] = bit6;
		}
		if(padding<5)
		{
			new_matrix[c*8 + 160*(msg_received-1) +3] = bit5;
		}
		if(padding<4)
		{
			new_matrix[c*8 + 160*(msg_received-1) +4] = bit4;
		}
		if(padding<3)
		{
			new_matrix[c*8 + 160*(msg_received-1) +5] = bit3;
		}
		if(padding<2)
		{
			new_matrix[c*8 + 160*(msg_received-1) +6] = bit2;
		}
		if(padding<1)
		{
			new_matrix[c*8 + 160*(msg_received-1) +7] = bit1;
		}
	}

	// if last msg then the new matrix becomes the matrix to be drawn
	if(msg_received == matrix_msgnum)
	{
		// deallocate old matrix
		if(!isDefaultMatrix)
		{
			free(matrix);
		}
		else
		{
			isDefaultMatrix = 0;
		}

		matrix = new_matrix;
		matrixVelocity = new_matrixVelocity;
		matrix_len = new_matrix_len;

		//reset running light
		shouldResetOffset = 1;
	}
}

/**
 * Callback processing the ACI events.
 */
void user_notify(void * pData)
{
  hci_uart_pckt *hci_pckt = pData;

  // obtain event packet
  hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;

  if(hci_pckt->type != HCI_EVENT_PKT)
    return;

  switch(event_pckt->evt){

  case EVT_DISCONN_COMPLETE:
    {
      GAP_DisconnectionComplete_CB();
    }
    break;

  case EVT_LE_META_EVENT:
    {
      evt_le_meta_event *evt = (void *)event_pckt->data;

      switch(evt->subevent){
      case EVT_LE_CONN_COMPLETE:
        {
          evt_le_connection_complete *cc = (void *)evt->data;
          GAP_ConnectionComplete_CB(cc->peer_bdaddr, cc->handle);
        }
        break;
      }
    }
    break;

  case EVT_VENDOR:
    {
      evt_blue_aci *blue_evt = (void*)event_pckt->data;
      switch(blue_evt->ecode){

		  case EVT_BLUE_GATT_READ_PERMIT_REQ:
			{
			  evt_gatt_read_permit_req *pr = (void*)blue_evt->data;
			  Read_Request_CB(pr->attr_handle);
			}
			break;
		  case EVT_BLUE_GATT_ATTRIBUTE_MODIFIED:
		  {
			  /* this callback is invoked when a GATT attribute is modified
			  extract callback data and pass to suitable handler function */
			  /*
			   * evt->conn_handle: the connection handle which modified the attribute;
			   * evt->attr_handle: handle of the attribute that was modified;
			   * evt->data_length: the length of the data;
			   * evt->att_data: pointer to the new value (length is data_length).
			   */
			  evt_gatt_attr_modified_IDB05A1 *evt = (evt_gatt_attr_modified_IDB05A1*)blue_evt->data;
			  PRINTF("Char written: ", evt->att_data);

			  if(evt->attr_handle == matrixHeaderCharHandle+1)
			  {
				  Matrix_Header_Modified(evt->attr_handle, evt->data_length, evt->att_data);
			  }
			  else if(evt->attr_handle == matrixDataCharHandle+1)
			  {
				  Matrix_Data_Modified(evt->attr_handle, evt->data_length, evt->att_data);
			  }
		  }
      }
      default:
      {
    	  PRINTF("Unknown event");
      }
    }
    break;
  }
}

/**
* Returns matrix
*/
uint8_t* getMatrix()
{
  return matrix;
}

/**
* Returns matrix velocity
*/
uint32_t getMatrixDisplayTime()
{
  return matrixVelocity;
}

/**
* Returns length of matrix
*/
uint16_t getMatrixLen()
{
  return matrix_len;
}

/**
 * Sets the ADC handle
 */
void setHadc(ADC_HandleTypeDef h)
{
	hadc1 = h;
}

/**
 * For matrix handler to check if the offset should be reseted
 */
int getShouldResetOffset()
{
	return shouldResetOffset;
}

/**
 * For matrix handler to notify that the offset was reseted
 */
void offsetReseted()
{
	shouldResetOffset = 0;
}
