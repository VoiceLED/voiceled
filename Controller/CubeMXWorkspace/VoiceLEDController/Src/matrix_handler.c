/*
 * matrix_handler.c
 *
 *  Created on: 24.04.2019
 *      Author: Joshua
 */

#include "matrix_handler.h"

// ===========================
// CONSTANTS
// ===========================

// delay between letter in microseconds
const int DELAY = 20;

// columns and rows of physical matrix
const int NUMBER_OF_COLS = 32;//16;//10;
const int NUMBER_OF_ROWS = 7;

// matrix error properties
#define ERROR_ARRAY_SIZE 161
uint8_t ERROR_MATRIX[ERROR_ARRAY_SIZE] = {
	1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0,
	1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0,
	1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0,
	1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0
};

// digits for error code
#define DIG_ZERO_SIZE 42
uint8_t DIG_ZERO[DIG_ZERO_SIZE] = {
		0, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		0, 1, 1, 1, 0, 0
};

#define DIG_ONE_SIZE 28
uint8_t DIG_ONE[DIG_ONE_SIZE] = {
		0, 0, 1, 0,
		0, 1, 1, 0,
		1, 0, 1, 0,
		0, 0, 1, 0,
		0, 0, 1, 0,
		0, 0, 1, 0,
		0, 0, 1, 0
};

#define DIG_TWO_SIZE 42
uint8_t DIG_TWO[DIG_TWO_SIZE] = {
		0, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 1, 0, 0,
		0, 0, 1, 0, 0, 0,
		0, 1, 0, 0, 0, 0,
		1, 1, 1, 1, 1, 0,
};
#define DIG_THREE_SIZE 42
uint8_t DIG_THREE[DIG_THREE_SIZE] = {
		0, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 1, 1, 0, 0,
		0, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		0, 1, 1, 1, 0, 0,
};

#define DIG_FOUR_SIZE 42
uint8_t DIG_FOUR[DIG_FOUR_SIZE] = {
		1, 0, 0, 0, 0, 0,
		1, 0, 0, 0, 0, 0,
		1, 0, 0, 1, 0, 0,
		1, 1, 1, 1, 1, 0,
		0, 0, 0, 1, 0, 0,
		0, 0, 0, 1, 0, 0,
		0, 0, 0, 1, 0, 0,
};

#define DIG_FIVE_SIZE 42
uint8_t DIG_FIVE[DIG_FIVE_SIZE] = {
		1, 1, 1, 1, 1, 0,
		1, 0, 0, 0, 0, 0,
		1, 0, 0, 0, 0, 0,
		1, 1, 1, 1, 0, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 1, 0,
		1, 1, 1, 1, 0, 0,
};

#define DIG_SIX_SIZE 42
uint8_t DIG_SIX[DIG_SIX_SIZE] = {
		0, 1, 1, 1, 1, 0,
		1, 0, 0, 0, 0, 0,
		1, 0, 0, 0, 0, 0,
		1, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		0, 1, 1, 1, 0, 0,
};

#define DIG_SEVEN_SIZE 42
uint8_t DIG_SEVEN[DIG_SEVEN_SIZE] = {
		1, 1, 1, 1, 1, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 1, 0, 0,
		0, 0, 1, 0, 0, 0,
		0, 1, 0, 0, 0, 0,
		1, 0, 0, 0, 0, 0,
		1, 0, 0, 0, 0, 0,
};

#define DIG_EIGHT_SIZE 42
uint8_t DIG_EIGHT[DIG_EIGHT_SIZE] = {
		0, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		0, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		0, 1, 1, 1, 0, 0,
};

#define DIG_NINE_SIZE 42
uint8_t DIG_NINE[DIG_NINE_SIZE] = {
		0, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 0,
		1, 0, 0, 0, 1, 0,
		0, 1, 1, 1, 1, 0,
		0, 0, 0, 0, 1, 0,
		0, 0, 0, 0, 1, 0,
		1, 1, 1, 1, 0, 0,
};

// ===========================
// GLOBAL VARIABLES
// ===========================

// offset used to get current picture in running light
int offset = 0;

I2C_HandleTypeDef hi2c1;
int isBright = 0;

// ===========================
// FUNCTIONS
// ===========================

/**
 * Set the I2C_HandleTypeDef hi2c1
 * Has to be called before Matrix_Process() to have a valid i2c handle
 */
void setHi2c(I2C_HandleTypeDef i)
{
	hi2c1 = i;
}

/*
 *
 */
void setBrightMode(int i)
{
	isBright = i;
}

/*
 *
 */
int getBrightMode()
{
	return isBright;
}

/**
 *	Main function that calls drawArray(...) to display an array
 */
void Matrix_Process()
{
	uint16_t id = getErrorId();
	// check if no error exists
	if(id == 0)
	{
		drawArray(getMatrix(), getMatrixLen());
	}
	// else display error matrix
	else
	{
		drawErrorArray(id);
	}

}

/*
 * Draws the error error: "Error" +  id
 */
void drawErrorArray(uint16_t id)
{
	uint16_t arrayLength = 0;
	// works because id wont be zero
	int numberOfDigits = floor (log10 (abs (id))) + 1;
	uint16_t digits[numberOfDigits];

	// access every digit of id
	for (int i = numberOfDigits-1; i >= 0; --i)
	{
		uint16_t digit = id % 10;
		digits[i] = digit;
		arrayLength += getLenOfDigitArry(digit);
	    id /= 10;
	}


	// create array for digits
	uint8_t digitsArray[arrayLength];
	uint16_t offset = 0;

	for (int i = 0; i < numberOfDigits; ++i)
	{
		uint16_t digit = digits[i];
		uint16_t digitArrayLen = getLenOfDigitArry(digit);
		uint8_t* digitArray = getDigitArry(digit);

		int currPos = 0;

		for (int row = 0; row < NUMBER_OF_ROWS; ++row)
		{
			for (int col = 0; col < digitArrayLen/NUMBER_OF_ROWS; ++col)
			{
				digitsArray[col + row*arrayLength/NUMBER_OF_ROWS + offset] = digitArray[currPos];
				currPos++;
			}
		}

		offset += digitArrayLen/NUMBER_OF_ROWS;
	}

	// combine error and digit array

	// create array for digits
	uint16_t combinedLength = arrayLength+ERROR_ARRAY_SIZE;
	uint8_t combinedArray[combinedLength];

	int currPos = 0;

	for (int row = 0; row < NUMBER_OF_ROWS; ++row)
	{
		for (int col = 0; col < combinedLength/NUMBER_OF_ROWS; ++col)
		{

			if(col < ERROR_ARRAY_SIZE/NUMBER_OF_ROWS)
			{
				combinedArray[currPos] = ERROR_MATRIX[col + row*ERROR_ARRAY_SIZE/NUMBER_OF_ROWS];
			}
			else
			{
				combinedArray[currPos] = digitsArray[col + row*arrayLength/NUMBER_OF_ROWS - ERROR_ARRAY_SIZE/NUMBER_OF_ROWS];
			}

			currPos++;
		}
	}

	drawArray(combinedArray, combinedLength);
}

/*
 * Returns the length of the array of the digit
 */
uint16_t getLenOfDigitArry(uint16_t digit)
{
	switch (digit) {
		case 0:
			return DIG_ZERO_SIZE;
		case 1:
			return DIG_ONE_SIZE;
		case 2:
			return DIG_TWO_SIZE;
		case 3:
			return DIG_THREE_SIZE;
		case 4:
			return DIG_FOUR_SIZE;
		case 5:
			return DIG_FIVE_SIZE;
		case 6:
			return DIG_SIX_SIZE;
		case 7:
			return DIG_SEVEN_SIZE;
		case 8:
			return DIG_EIGHT_SIZE;
		case 9:
			return DIG_NINE_SIZE;
		default:
			return 0;
	}
}

/*
 * Returns the the array of the digit
 */
uint8_t* getDigitArry(uint16_t digit)
{
	switch (digit) {
		case 0:
			return DIG_ZERO;
		case 1:
			return DIG_ONE;
		case 2:
			return DIG_TWO;
		case 3:
			return DIG_THREE;
		case 4:
			return DIG_FOUR;
		case 5:
			return DIG_FIVE;
		case 6:
			return DIG_SIX;
		case 7:
			return DIG_SEVEN;
		case 8:
			return DIG_EIGHT;
		case 9:
			return DIG_NINE;
		default:
			return 0;
	}
}

/**
 * Draw an picture of size (NUMBER_OF_ROWS * n) on LED matrix
 * if (array_cols > NUMBER_OF_COLS) picture in running light
 * else (centered) static picture will be drawn
 */
void drawArray(uint8_t* array, uint16_t arrayLength)
{
	// check if array is drawable
	if(arrayLength % NUMBER_OF_ROWS == 0)
	{
		// calculate number of cols in array
		int array_cols = arrayLength/NUMBER_OF_ROWS;

		// RUNNING LIGHT
		if(array_cols > NUMBER_OF_COLS)
		{
			// reset offset
			if(offset == array_cols)
			{
				offset = 0;
			}
			if(getShouldResetOffset())
			{
				offset = 0;
				offsetReseted();
			}

			// picture of running light to be calculated and displayed on physical hardware
			uint8_t picture[NUMBER_OF_COLS*NUMBER_OF_ROWS];
			// counter for current position in picture
			int currPos = 0;
			// index in logical array
			int posInArray = 0;

			// get current picture:
			// iterate over physical matrix
			for (int row = 0; row < NUMBER_OF_ROWS; ++row) {
				for (int col = 0; col < NUMBER_OF_COLS; ++col) {

					// calculate the position in the (bigger) logical array
					if(offset+col<array_cols)
					{
						posInArray = col+offset+row*array_cols;
					}
					else
					{
						// if offset and current col are greater than array cols
						// then the index would be beyond the matrix e.g. in the front
						// so we have the offset of -1
						posInArray = col+offset+array_cols*(row-1);
					}

					// get values from logical array
					picture[currPos] = array[posInArray];

					// next position in picture
					currPos++;
				}
			}

			displayCalculatedPicture(picture);

			// next offset for running light (left shift)
			offset++;
		}
		// STATIC PICTURE
		else if (array_cols == NUMBER_OF_COLS)
		{
			// no calculation to be done

			displayCalculatedPicture(array);
		}
		// CENTERED STATIC PICTURE
		else //array_cols < NUMBER_OF_COLS
		{
			// picture to be calculated and displayed on physical hardware
			uint8_t picture[NUMBER_OF_COLS*NUMBER_OF_ROWS];

			// calculate critical section of physical array to be overwritten by shorter array
			int crit = floor((NUMBER_OF_COLS-array_cols)/2.0);

			int offset = 0;
			// offset for odd column number
			if ((NUMBER_OF_COLS-array_cols)%2 != 0)
			{
				offset = 1;
			}

			int currPos = 0;
			// iterate over physical array
			for (int row = 0; row < NUMBER_OF_ROWS; ++row) {
				for (int col = 0; col < NUMBER_OF_COLS; ++col)
				{
					// if it is in the critical section
					if((col >= crit) && (col < NUMBER_OF_COLS-crit-offset))
					{
						// use value of given (shorter) array
						picture[currPos] = array[col-crit+row*array_cols];
					}
					else
					{
						// no values to be displayed
						picture[currPos] = 0;
					}
					currPos++;
				}
			}

			displayCalculatedPicture(picture);
		}
	}
	else // Array not drawable
	{
		setErrorId(ERR_MAT_ARR_NOT_DRAWABLE);
	}
}

/*
 * Displays an picture for display time
 */
void displayCalculatedPicture(uint8_t* picture)
{
	uint32_t start = HAL_GetTick();
	uint32_t elapsed = 0;
	do{
		if (isBright) {
			drawPictureBright(picture);
		}
		else
		{
			drawPicture(picture);
		}
		elapsed = HAL_GetTick();
	}
	while(elapsed < getMatrixDisplayTime() + start);
}

/**
 * Draw a picture of size (NUMBER_OF_ROWS *  NUMBER_OF_COLS) on LED matrix
 * with row multiplexing
 * Magic Numbers: #Rows=7, #Column-I2C-Chips=4, #Pins-Per-Chip=8
 */
void drawPictureBright(uint8_t* picture) {
    
    // for every row
    for (int row = 0; row < 7 ; row++) {
        
        // calculate and set columns - chip by chip
        for (int chip = 0; chip < 4; chip++) {
            
            // i2c adress of column chip
            uint8_t colAddr = 0x72 + (2*chip);
            
            // init data storing variable to send later
            uint8_t dataByte = 0;
            
            // loop over part of picture and add up databyte
            for (int part = 0; part < 8; part++){
                
                // byte position value (power of two)
                uint8_t byteVal = 1;
                for(int i=1; i<=part; i++)
                    byteVal = byteVal * 2;
                
                // picture pos value = offset 32 for each row, offset 8 for each col chip, add current offset in 8bit part
                // picture position value (0/1) * int position value (1,2,4,8,16,32,64,128)
                dataByte += picture[(32*row)+(chip*8)+part] * byteVal;
                
            }
            
            // send data after a chip has been processed
            writeI2c(colAddr, dataByte);
        }
        
        // once all column chips are set for a single row, activate the mass side (+1 because of MPin func)
        writeMPin(0x70, 1, row+1);
        //wait and hold that subframe (row)
        delayUS(DELAY*150);
        //turn off mass chip to prevent false led lightup
        writeI2c(0x70,0x00);
        
        
    }

}

/**
 * Draw a picture of size (NUMBER_OF_ROWS *  NUMBER_OF_COLS) on LED matrix
 */
void drawPicture(uint8_t* picture) {

	int currSlaveCol = 0;
	// high side slave address
	uint8_t slaveAddress = 0x72;
	int firstTime = 1;

	for (int col = 0; col < NUMBER_OF_COLS; ++col) {
		// slave address
		currSlaveCol = col % 8;

		// vpin slave check
		if(currSlaveCol == 0)
		{
			if(firstTime == 1)
			{
				firstTime = 0;
			}
			else
			{
				// reset current slave
				writeI2c(slaveAddress, 0x00);
				// increase slave address
				slaveAddress = slaveAddress + 2;
			}
		}

		// set v pins
		writeVPin(slaveAddress, currSlaveCol+1);

		// set mass pins
		for (int i = 0; i < NUMBER_OF_ROWS; ++i) {
			writeMPin(0x70, picture[i*NUMBER_OF_COLS + col], i+1);
			delayUS(DELAY);
			writeI2c(0x70, 0x00);
		}

		// reset v pins
		writeVPin(slaveAddress, 0);

		// reset last i2c
		if(col == NUMBER_OF_COLS-1)
		{
			writeI2c(slaveAddress, 0x00);
		}
	}
}

/**
 * Sets a single output pin of an i2c chip with the given address
 */
void writeVPin(uint8_t slaveAddress, int col) {
	switch(col)
	{
		case 1:
			writeI2c(slaveAddress, 0x01);
			break;
		case 2:
			writeI2c(slaveAddress, 0x02);
			break;
		case 3:
			writeI2c(slaveAddress, 0x04);
			break;
		case 4:
			writeI2c(slaveAddress, 0x08);
			break;
		case 5:
			writeI2c(slaveAddress, 0x10);
			break;
		case 6:
			writeI2c(slaveAddress, 0x20);
			break;
		case 7:
			writeI2c(slaveAddress, 0x40);
			break;
		case 8:
			writeI2c(slaveAddress, 0x80);
			break;
		default:
			writeI2c(slaveAddress, 0x00);
			break;
	}
}

/**
 * Sets a single output pin of an i2c chip with the given address
 */
void writeMPin(uint8_t slaveAddress, uint8_t isOn, int row) {
	if(isOn)
	{
		switch(row)
		{
			case 1:
				writeI2c(slaveAddress, 0x01);
				break;
			case 2:
				writeI2c(slaveAddress, 0x02);
				break;
			case 3:
				writeI2c(slaveAddress, 0x04);
				break;
			case 4:
				writeI2c(slaveAddress, 0x08);
				break;
			case 5:
				writeI2c(slaveAddress, 0x10);
				break;
			case 6:
				writeI2c(slaveAddress, 0x20);
				break;
			case 7:
				writeI2c(slaveAddress, 0x40);
				break;
			case 8:
				writeI2c(slaveAddress, 0x80);
				break;
			default:
				writeI2c(slaveAddress, 0x00);
				break;
		}
	}
}

/**
 * Write data into i2c slave adress
 * slave_addr: Address of the i2c chip that should receive this message
 * data: the data transmitted
 */
void writeI2c(uint8_t slave_addr, uint8_t data)
{
    uint8_t in[1];

    in[0] = data;

    HAL_I2C_Master_Transmit(&hi2c1, slave_addr, in, 1, 100);
}
