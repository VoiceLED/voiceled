/*
 * matrix_handler.h
 *
 *  Created on: 24.04.2019
 *      Author: Joshua
 */

#ifndef MATRIX_HANDLER_H_
#define MATRIX_HANDLER_H_

// ===========================
// INCLUDES
// ===========================
#include "stm32f4xx_hal.h"
#include <math.h>
#include "ble_handler.h"
#include "dwt_stm32_delay.h"
#include "error_handler.h"

// ===========================
// FUNCTIONS
// ===========================
void setHi2c(I2C_HandleTypeDef i);
void Matrix_Process(void);
void displayCalculatedPicture(uint8_t* picture);
void drawArray(uint8_t* array, uint16_t arrayLength);
void drawPicture(uint8_t* picture);
void writeVPin(uint8_t slaveAddress ,int col);
void writeI2c(uint8_t slave_addr, uint8_t data);
void Matrix_Process();
void drawErrorArray(uint16_t id);
uint16_t getLenOfDigitArry(uint16_t id);
uint8_t* getDigitArry(uint16_t id);
void drawPictureBright(uint8_t* picture);
void setBrightMode(int i);
int getBrightMode();

void writeMPin(uint8_t slaveAddress, uint8_t isOn, int row);

#endif /* MATRIX_HANDLER_H_ */
