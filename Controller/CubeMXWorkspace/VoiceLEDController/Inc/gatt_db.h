/*
 * gatt_db.h
 *
 *  Created on: 02.02.2019
 *      Author: Joshua
 */

#ifndef GATT_DB_H_
#define GATT_DB_H_

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
//#include "cube_hal.h"
//#include "bluenrg_types.h"
//#include "bluenrg_gatt_server.h"
#include "bluenrg_gap.h"
#include "string.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
#include "bluenrg_conf.h"

#include <stdlib.h>

/* Exported defines ----------------------------------------------------------*/
#define IDB04A1 0
#define IDB05A1 1

/**
 * @brief Instantiate two new services:
 *        1. Timer Service with two characteristics
 *           - Seconds characteristic (Readable only)
 *           - Minutes characteristics (Readable and Notifiable)
 *        2. LED Button Service with one characteristic
 *           - LED characteristic (Readable and Writable)
 */
//#define NEW_SERVICES 0

tBleStatus Add_Services(void);
void       setConnectable(void);
void       enableNotification(void);
void       GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle);
void       GAP_DisconnectionComplete_CB(void);
void       User_Process_Notification_Request(void);
void	   SetUserProcessRequest(BOOL userProcessValue);
void 	   user_notify(void * pData);
void Matrix_Header_Modified(uint16_t handle, uint8_t data_length, uint8_t *att_data);
void Matrix_Data_Modified(uint16_t handle, uint8_t data_length, uint8_t *att_data);
uint8_t* getMatrix();
uint16_t getMatrixLen();
int getMatrixVelocity();
void setHadc(ADC_HandleTypeDef h);

//#if NEW_SERVICES
//  tBleStatus Add_Time_Service(void);
//  tBleStatus Seconds_Update(void);
//  tBleStatus Minutes_Notify(void);
//  void       Update_Time_Characteristics(void);
//
//  tBleStatus Add_LED_Service(void);
//  void       Attribute_Modified_CB(uint16_t handle, uint8_t data_length,
//                                   uint8_t *att_data);
//#endif

#ifdef __cplusplus
}
#endif


#endif /* GATT_DB_H_ */
