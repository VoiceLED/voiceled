/*
 * ble_handler.h
 *
 *  Created on: 25.04.2019
 *      Author: Joshua
 */

#ifndef BLE_HANDLER_H_
#define BLE_HANDLER_H_

// ===========================
// INCLUDES
// ===========================

#include <math.h>
#include "bluenrg_gap.h"
#include "string.h"
#include "bluenrg_gap_aci.h"
#include "bluenrg_gatt_aci.h"
#include "hci_const.h"
#include "bluenrg_hal_aci.h"
#include "bluenrg_aci_const.h"
#include "hci.h"
#include "hci_le.h"
#include "sm.h"
#include "bluenrg_conf.h"
#include "error_handler.h"

#include <stdlib.h>

/* Exported defines ----------------------------------------------------------*/
#define IDB04A1 0
#define IDB05A1 1

// ===========================
// FUNCTIONS
// ===========================
tBleStatus Add_Services(void);
void setConnectable(void);
void enableNotification(void);
void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle);
void GAP_DisconnectionComplete_CB(void);
void User_Process_Notification_Request(void);
void SetUserProcessRequest(BOOL userProcessValue);
void user_notify(void * pData);
void Matrix_Header_Modified(uint16_t handle, uint8_t data_length, uint8_t *att_data);
void Matrix_Data_Modified(uint16_t handle, uint8_t data_length, uint8_t *att_data);
uint8_t* getMatrix();
uint16_t getMatrixLen();
uint32_t getMatrixDisplayTime();
void setHadc(ADC_HandleTypeDef h);
int getShouldResetOffset();
void offsetReseted();
tBleStatus Update_Error_Id_Characteristic();
tBleStatus Temp_Update();

#endif /* BLE_HANDLER_H_ */
