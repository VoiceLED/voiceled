/*
 * error_handler.h
 *
 *  Created on: 30.04.2019
 *      Author: Joshua
 */

#ifndef ERROR_HANDLER_H_
#define ERROR_HANDLER_H_

#include <stdint.h>

void setErrorId(uint16_t id);
void resetErrorId();
uint16_t getErrorId();
int hasErrorIdChanged();

// error ids

// 0xx: general errors
#define ERR_NO_ERROR 0
#define ERR_UNKNOWN_ERROR 1

// 1xx: HAL
#define ERR_HAL_RCC_OSC_CONF 110
#define ERR_HAL_RCC_CLK_CONF 111
#define ERR_HAL_ADC_INIT 120
#define ERR_HAL_ADC_CHANNEL_CNF 121
#define ERR_HAL_ADC_POLL 122
#define ERR_HAL_UART_INIT 130
#define ERR_HAL_I2C_INIT 140
#define ERR_HAL_I2C_NOT_RDY 141

// 2xx: Matrix
#define ERR_MAT_ARR_NOT_DRAWABLE 200

// 3xx: BLE
#define ERR_BLE_ACI_BDADDR 300
#define ERR_BLE_ACI_GATT_INIT 301
#define ERR_BLE_ACI_GAP_INIT 302
#define ERR_BLE_ACI_GATT_UPDATE 303
#define ERR_BLE_ACI_SET_AUTH 304
#define ERR_BLE_ADD_SERVICES 310

#endif /* ERROR_HANDLER_H_ */
