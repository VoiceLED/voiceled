/*
 * dwt_stm32_delay.h
 *
 *  Created on: 30.04.2019
 *      Author: Joshua
 */

#ifndef DWT_STM32_DELAY_H_
#define DWT_STM32_DELAY_H_

#include <stdint.h>
#include "stm32f4xx_hal.h"

void delayUS(uint16_t micros);

#endif /* DWT_STM32_DELAY_H_ */
