<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="no"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED-1206">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
<package name="LED-0603">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.2032" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED-1206-HIDDENSILK">
<wire x1="0.6474" y1="0.6825" x2="0.6474" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-RED" prefix="D" uservalue="yes">
<description>&lt;h3&gt;Red SMD LED&lt;/h3&gt;
&lt;p&gt;Used in manufacturing of most products at SparkFun&lt;/p&gt;

&lt;p&gt;&lt;b&gt;Packages:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;b&gt;0603&lt;/b&gt; - SMD 0603 package &lt;a href="http://docs.avagotech.com/docs/AV02-0551EN"&gt;[Product Link]&lt;/li&gt;
&lt;li&gt;&lt;b&gt;0603 SMART&lt;/b&gt; - SMD 0603 SMART package ( more efficient and expensive)&lt;a href="http://www.osram-os.com/Graphics/XPic2/00077099_0.pdf"&gt;[Product Link]&lt;/li&gt;
&lt;li&gt;&lt;b&gt; 1206&lt;/b&gt; - SMD1206  package &lt;a href="https://www.sparkfun.com/datasheets/DevTools/LilyPad/Q150OVS4.pdff"&gt;[Product Link]&lt;/li&gt;
&lt;ul&gt;&lt;/p&gt;

&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/821”&gt;Pro Mini 328 -5V&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12757”&gt;RedBoard&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13613”&gt;IOIO-OTG&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-00819"/>
<attribute name="VALUE" value="RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09912"/>
<attribute name="VALUE" value="RED" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-SMART" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11154" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
</technologies>
</device>
<device name="_HIDDENSILK" package="LED-1206-HIDDENSILK">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09912" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="voiceled">
<packages>
<package name="FLAT-HEAD-16">
<pad name="P$1" x="0" y="0" drill="1" diameter="2" shape="square"/>
<pad name="P$2" x="0" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$3" x="0" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$4" x="0" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$5" x="0" y="-10.16" drill="1" diameter="2" shape="square"/>
<pad name="P$6" x="0" y="-12.7" drill="1" diameter="2" shape="square"/>
<pad name="P$7" x="0" y="-15.24" drill="1" diameter="2" shape="square"/>
<pad name="P$8" x="0" y="-17.78" drill="1" diameter="2" shape="square"/>
<pad name="P$9" x="2.54" y="-17.78" drill="1" diameter="2" shape="square"/>
<pad name="P$10" x="2.54" y="-15.24" drill="1" diameter="2" shape="square"/>
<pad name="P$11" x="2.54" y="-12.7" drill="1" diameter="2" shape="square"/>
<pad name="P$12" x="2.54" y="-10.16" drill="1" diameter="2" shape="square"/>
<pad name="P$13" x="2.54" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$14" x="2.54" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$15" x="2.54" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$16" x="2.54" y="0" drill="1" diameter="2" shape="square"/>
<wire x1="-3.81" y1="-22.86" x2="6.35" y2="-22.86" width="0.127" layer="21"/>
<wire x1="6.35" y1="-22.86" x2="6.35" y2="5.08" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.08" x2="-3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-22.86" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="104">&gt;NAME</text>
</package>
<package name="FLAT-HEAD-10">
<pad name="P$1" x="0" y="0" drill="1" diameter="2" shape="square"/>
<pad name="P$6" x="2.54" y="0" drill="1" diameter="2" shape="square"/>
<pad name="P$2" x="0" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$7" x="2.54" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$3" x="0" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$8" x="2.54" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$4" x="0" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$9" x="2.54" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$5" x="0" y="-10.16" drill="1" diameter="2" shape="square"/>
<pad name="P$10" x="2.54" y="-10.16" drill="1" diameter="2" shape="square"/>
<wire x1="-3.81" y1="-15.24" x2="6.35" y2="-15.24" width="0.127" layer="21"/>
<wire x1="6.35" y1="-15.24" x2="6.35" y2="5.08" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.08" x2="-3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-15.24" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="104">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="FLAT-HEAD-16">
<pin name="P$1" x="-10.16" y="8.89" visible="pin" length="short"/>
<wire x1="-8.89" y1="10.16" x2="8.89" y2="10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="10.16" x2="8.89" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="-8.89" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-10.16" x2="-8.89" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<pin name="P$2" x="-10.16" y="6.35" visible="pin" length="short"/>
<pin name="P$3" x="-10.16" y="3.81" visible="pin" length="short"/>
<pin name="P$4" x="-10.16" y="1.27" visible="pin" length="short"/>
<pin name="P$5" x="-10.16" y="-1.27" visible="pin" length="short"/>
<pin name="P$6" x="-10.16" y="-3.81" visible="pin" length="short"/>
<pin name="P$7" x="-10.16" y="-6.35" visible="pin" length="short"/>
<pin name="P$8" x="-10.16" y="-8.89" visible="pin" length="short"/>
<pin name="P$9" x="10.16" y="8.89" visible="pin" length="short" rot="R180"/>
<pin name="P$10" x="10.16" y="6.35" visible="pin" length="short" rot="R180"/>
<pin name="P$11" x="10.16" y="3.81" visible="pin" length="short" rot="R180"/>
<pin name="P$12" x="10.16" y="1.27" visible="pin" length="short" rot="R180"/>
<pin name="P$13" x="10.16" y="-1.27" visible="pin" length="short" rot="R180"/>
<pin name="P$14" x="10.16" y="-3.81" visible="pin" length="short" rot="R180"/>
<pin name="P$15" x="10.16" y="-6.35" visible="pin" length="short" rot="R180"/>
<pin name="P$16" x="10.16" y="-8.89" visible="pin" length="short" rot="R180"/>
<text x="-3.81" y="11.43" size="1.778" layer="104">&gt;NAME</text>
</symbol>
<symbol name="FLAT-HEAD-10">
<pin name="P$1" x="-3.81" y="5.08" visible="off" length="short"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<pin name="P$10" x="3.81" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="P$2" x="-3.81" y="2.54" visible="off" length="short"/>
<pin name="P$9" x="3.81" y="2.54" visible="off" length="short" rot="R180"/>
<text x="-3.81" y="7.62" size="1.778" layer="104">&gt;NAME</text>
<pin name="P$3" x="-3.81" y="0" visible="off" length="short"/>
<pin name="P$8" x="3.81" y="0" visible="off" length="short" rot="R180"/>
<pin name="P$4" x="-3.81" y="-2.54" visible="off" length="short"/>
<pin name="P$7" x="3.81" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="P$5" x="-3.81" y="-5.08" visible="off" length="short"/>
<pin name="P$6" x="3.81" y="-5.08" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M3-FH-16">
<gates>
<gate name="G$1" symbol="FLAT-HEAD-16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FLAT-HEAD-16">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M3-FH-10">
<gates>
<gate name="PH$1" symbol="FLAT-HEAD-10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FLAT-HEAD-10">
<connects>
<connect gate="PH$1" pin="P$1" pad="P$1"/>
<connect gate="PH$1" pin="P$10" pad="P$6"/>
<connect gate="PH$1" pin="P$2" pad="P$2"/>
<connect gate="PH$1" pin="P$3" pad="P$3"/>
<connect gate="PH$1" pin="P$4" pad="P$4"/>
<connect gate="PH$1" pin="P$5" pad="P$5"/>
<connect gate="PH$1" pin="P$6" pad="P$10"/>
<connect gate="PH$1" pin="P$7" pad="P$9"/>
<connect gate="PH$1" pin="P$8" pad="P$8"/>
<connect gate="PH$1" pin="P$9" pad="P$7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D1" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D2" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D3" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D4" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D5" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D6" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D7" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D8" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D9" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D10" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D11" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D12" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D13" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D14" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D15" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D16" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D17" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D18" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D19" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D20" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D21" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D22" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D23" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D24" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D25" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D26" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D27" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D28" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D29" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D30" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D31" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D32" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D33" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D34" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D35" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D36" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D37" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D38" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D39" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D40" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D41" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D42" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D43" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D44" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D45" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D46" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D47" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D48" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D49" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D50" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D51" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D52" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D53" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D54" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D55" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D56" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D57" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D58" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D59" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D60" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D61" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D62" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D63" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D64" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D65" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D66" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D67" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D68" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D69" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D70" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D71" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D72" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D73" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D74" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D75" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D76" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D77" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D78" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D79" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D80" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D81" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D82" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D83" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D84" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D85" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D86" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D87" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D88" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D89" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D90" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D91" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D92" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D93" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D94" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D95" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D96" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D97" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D98" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D99" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D100" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D101" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D102" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D103" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D104" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D105" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D106" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D107" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D108" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D109" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D110" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D111" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="D112" library="SparkFun-LED" deviceset="LED-RED" device="_HIDDENSILK"/>
<part name="SOCKET16" library="voiceled" deviceset="M3-FH-16" device=""/>
<part name="SOCKET10" library="voiceled" deviceset="M3-FH-10" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-190.5" y="60.96" size="1.778" layer="97" rot="R90">P1</text>
<text x="-187.96" y="60.96" size="1.778" layer="97" rot="R90">P2</text>
<text x="-185.42" y="60.96" size="1.778" layer="97" rot="R90">P3</text>
<text x="-182.88" y="60.96" size="1.778" layer="97" rot="R90">P4</text>
<text x="-180.34" y="60.96" size="1.778" layer="97" rot="R90">P5</text>
<text x="-180.34" y="48.26" size="1.778" layer="97" rot="R90">P10</text>
<text x="-182.88" y="48.26" size="1.778" layer="97" rot="R90">P9</text>
</plain>
<instances>
<instance part="D1" gate="G$1" x="-162.56" y="99.06" smashed="yes">
<attribute name="NAME" x="-165.989" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D2" gate="G$1" x="-162.56" y="86.36" smashed="yes">
<attribute name="NAME" x="-165.989" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D3" gate="G$1" x="-162.56" y="73.66" smashed="yes">
<attribute name="NAME" x="-165.989" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D4" gate="G$1" x="-162.56" y="60.96" smashed="yes">
<attribute name="NAME" x="-165.989" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D5" gate="G$1" x="-162.56" y="48.26" smashed="yes">
<attribute name="NAME" x="-165.989" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D6" gate="G$1" x="-162.56" y="35.56" smashed="yes">
<attribute name="NAME" x="-165.989" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D7" gate="G$1" x="-162.56" y="22.86" smashed="yes">
<attribute name="NAME" x="-165.989" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-160.655" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D8" gate="G$1" x="-152.4" y="99.06" smashed="yes">
<attribute name="NAME" x="-155.829" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D9" gate="G$1" x="-152.4" y="86.36" smashed="yes">
<attribute name="NAME" x="-155.829" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D10" gate="G$1" x="-152.4" y="73.66" smashed="yes">
<attribute name="NAME" x="-155.829" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D11" gate="G$1" x="-152.4" y="60.96" smashed="yes">
<attribute name="NAME" x="-155.829" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D12" gate="G$1" x="-152.4" y="48.26" smashed="yes">
<attribute name="NAME" x="-155.829" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D13" gate="G$1" x="-152.4" y="35.56" smashed="yes">
<attribute name="NAME" x="-155.829" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D14" gate="G$1" x="-152.4" y="22.86" smashed="yes">
<attribute name="NAME" x="-155.829" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-150.495" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D15" gate="G$1" x="-142.24" y="99.06" smashed="yes">
<attribute name="NAME" x="-145.669" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D16" gate="G$1" x="-142.24" y="86.36" smashed="yes">
<attribute name="NAME" x="-145.669" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D17" gate="G$1" x="-142.24" y="73.66" smashed="yes">
<attribute name="NAME" x="-145.669" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D18" gate="G$1" x="-142.24" y="60.96" smashed="yes">
<attribute name="NAME" x="-145.669" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D19" gate="G$1" x="-142.24" y="48.26" smashed="yes">
<attribute name="NAME" x="-145.669" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D20" gate="G$1" x="-142.24" y="35.56" smashed="yes">
<attribute name="NAME" x="-145.669" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D21" gate="G$1" x="-142.24" y="22.86" smashed="yes">
<attribute name="NAME" x="-145.669" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-140.335" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D22" gate="G$1" x="-132.08" y="99.06" smashed="yes">
<attribute name="NAME" x="-135.509" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D23" gate="G$1" x="-132.08" y="86.36" smashed="yes">
<attribute name="NAME" x="-135.509" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D24" gate="G$1" x="-132.08" y="73.66" smashed="yes">
<attribute name="NAME" x="-135.509" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D25" gate="G$1" x="-132.08" y="60.96" smashed="yes">
<attribute name="NAME" x="-135.509" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D26" gate="G$1" x="-132.08" y="48.26" smashed="yes">
<attribute name="NAME" x="-135.509" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D27" gate="G$1" x="-132.08" y="35.56" smashed="yes">
<attribute name="NAME" x="-135.509" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D28" gate="G$1" x="-132.08" y="22.86" smashed="yes">
<attribute name="NAME" x="-135.509" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-130.175" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D29" gate="G$1" x="-121.92" y="99.06" smashed="yes">
<attribute name="NAME" x="-125.349" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D30" gate="G$1" x="-121.92" y="86.36" smashed="yes">
<attribute name="NAME" x="-125.349" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D31" gate="G$1" x="-121.92" y="73.66" smashed="yes">
<attribute name="NAME" x="-125.349" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D32" gate="G$1" x="-121.92" y="60.96" smashed="yes">
<attribute name="NAME" x="-125.349" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D33" gate="G$1" x="-121.92" y="48.26" smashed="yes">
<attribute name="NAME" x="-125.349" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D34" gate="G$1" x="-121.92" y="35.56" smashed="yes">
<attribute name="NAME" x="-125.349" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D35" gate="G$1" x="-121.92" y="22.86" smashed="yes">
<attribute name="NAME" x="-125.349" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-120.015" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D36" gate="G$1" x="-111.76" y="99.06" smashed="yes">
<attribute name="NAME" x="-115.189" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D37" gate="G$1" x="-111.76" y="86.36" smashed="yes">
<attribute name="NAME" x="-115.189" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D38" gate="G$1" x="-111.76" y="73.66" smashed="yes">
<attribute name="NAME" x="-115.189" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D39" gate="G$1" x="-111.76" y="60.96" smashed="yes">
<attribute name="NAME" x="-115.189" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D40" gate="G$1" x="-111.76" y="48.26" smashed="yes">
<attribute name="NAME" x="-115.189" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D41" gate="G$1" x="-111.76" y="35.56" smashed="yes">
<attribute name="NAME" x="-115.189" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D42" gate="G$1" x="-111.76" y="22.86" smashed="yes">
<attribute name="NAME" x="-115.189" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-109.855" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D43" gate="G$1" x="-101.6" y="99.06" smashed="yes">
<attribute name="NAME" x="-105.029" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D44" gate="G$1" x="-101.6" y="86.36" smashed="yes">
<attribute name="NAME" x="-105.029" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D45" gate="G$1" x="-101.6" y="73.66" smashed="yes">
<attribute name="NAME" x="-105.029" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D46" gate="G$1" x="-101.6" y="60.96" smashed="yes">
<attribute name="NAME" x="-105.029" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D47" gate="G$1" x="-101.6" y="48.26" smashed="yes">
<attribute name="NAME" x="-105.029" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D48" gate="G$1" x="-101.6" y="35.56" smashed="yes">
<attribute name="NAME" x="-105.029" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D49" gate="G$1" x="-101.6" y="22.86" smashed="yes">
<attribute name="NAME" x="-105.029" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-99.695" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D50" gate="G$1" x="-91.44" y="99.06" smashed="yes">
<attribute name="NAME" x="-94.869" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D51" gate="G$1" x="-91.44" y="86.36" smashed="yes">
<attribute name="NAME" x="-94.869" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D52" gate="G$1" x="-91.44" y="73.66" smashed="yes">
<attribute name="NAME" x="-94.869" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D53" gate="G$1" x="-91.44" y="60.96" smashed="yes">
<attribute name="NAME" x="-94.869" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D54" gate="G$1" x="-91.44" y="48.26" smashed="yes">
<attribute name="NAME" x="-94.869" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D55" gate="G$1" x="-91.44" y="35.56" smashed="yes">
<attribute name="NAME" x="-94.869" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D56" gate="G$1" x="-91.44" y="22.86" smashed="yes">
<attribute name="NAME" x="-94.869" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-89.535" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D57" gate="G$1" x="-81.28" y="99.06" smashed="yes">
<attribute name="NAME" x="-84.709" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D58" gate="G$1" x="-81.28" y="86.36" smashed="yes">
<attribute name="NAME" x="-84.709" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D59" gate="G$1" x="-81.28" y="73.66" smashed="yes">
<attribute name="NAME" x="-84.709" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D60" gate="G$1" x="-81.28" y="60.96" smashed="yes">
<attribute name="NAME" x="-84.709" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D61" gate="G$1" x="-81.28" y="48.26" smashed="yes">
<attribute name="NAME" x="-84.709" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D62" gate="G$1" x="-81.28" y="35.56" smashed="yes">
<attribute name="NAME" x="-84.709" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D63" gate="G$1" x="-81.28" y="22.86" smashed="yes">
<attribute name="NAME" x="-84.709" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-79.375" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D64" gate="G$1" x="-71.12" y="99.06" smashed="yes">
<attribute name="NAME" x="-74.549" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D65" gate="G$1" x="-71.12" y="86.36" smashed="yes">
<attribute name="NAME" x="-74.549" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D66" gate="G$1" x="-71.12" y="73.66" smashed="yes">
<attribute name="NAME" x="-74.549" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D67" gate="G$1" x="-71.12" y="60.96" smashed="yes">
<attribute name="NAME" x="-74.549" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D68" gate="G$1" x="-71.12" y="48.26" smashed="yes">
<attribute name="NAME" x="-74.549" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D69" gate="G$1" x="-71.12" y="35.56" smashed="yes">
<attribute name="NAME" x="-74.549" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D70" gate="G$1" x="-71.12" y="22.86" smashed="yes">
<attribute name="NAME" x="-74.549" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-69.215" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D71" gate="G$1" x="-60.96" y="99.06" smashed="yes">
<attribute name="NAME" x="-64.389" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D72" gate="G$1" x="-60.96" y="86.36" smashed="yes">
<attribute name="NAME" x="-64.389" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D73" gate="G$1" x="-60.96" y="73.66" smashed="yes">
<attribute name="NAME" x="-64.389" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D74" gate="G$1" x="-60.96" y="60.96" smashed="yes">
<attribute name="NAME" x="-64.389" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D75" gate="G$1" x="-60.96" y="48.26" smashed="yes">
<attribute name="NAME" x="-64.389" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D76" gate="G$1" x="-60.96" y="35.56" smashed="yes">
<attribute name="NAME" x="-64.389" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D77" gate="G$1" x="-60.96" y="22.86" smashed="yes">
<attribute name="NAME" x="-64.389" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-59.055" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D78" gate="G$1" x="-50.8" y="99.06" smashed="yes">
<attribute name="NAME" x="-54.229" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D79" gate="G$1" x="-50.8" y="86.36" smashed="yes">
<attribute name="NAME" x="-54.229" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D80" gate="G$1" x="-50.8" y="73.66" smashed="yes">
<attribute name="NAME" x="-54.229" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D81" gate="G$1" x="-50.8" y="60.96" smashed="yes">
<attribute name="NAME" x="-54.229" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D82" gate="G$1" x="-50.8" y="48.26" smashed="yes">
<attribute name="NAME" x="-54.229" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D83" gate="G$1" x="-50.8" y="35.56" smashed="yes">
<attribute name="NAME" x="-54.229" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D84" gate="G$1" x="-50.8" y="22.86" smashed="yes">
<attribute name="NAME" x="-54.229" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-48.895" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D85" gate="G$1" x="-40.64" y="99.06" smashed="yes">
<attribute name="NAME" x="-44.069" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D86" gate="G$1" x="-40.64" y="86.36" smashed="yes">
<attribute name="NAME" x="-44.069" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D87" gate="G$1" x="-40.64" y="73.66" smashed="yes">
<attribute name="NAME" x="-44.069" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D88" gate="G$1" x="-40.64" y="60.96" smashed="yes">
<attribute name="NAME" x="-44.069" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D89" gate="G$1" x="-40.64" y="48.26" smashed="yes">
<attribute name="NAME" x="-44.069" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D90" gate="G$1" x="-40.64" y="35.56" smashed="yes">
<attribute name="NAME" x="-44.069" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D91" gate="G$1" x="-40.64" y="22.86" smashed="yes">
<attribute name="NAME" x="-44.069" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-38.735" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D92" gate="G$1" x="-30.48" y="99.06" smashed="yes">
<attribute name="NAME" x="-33.909" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D93" gate="G$1" x="-30.48" y="86.36" smashed="yes">
<attribute name="NAME" x="-33.909" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D94" gate="G$1" x="-30.48" y="73.66" smashed="yes">
<attribute name="NAME" x="-33.909" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D95" gate="G$1" x="-30.48" y="60.96" smashed="yes">
<attribute name="NAME" x="-33.909" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D96" gate="G$1" x="-30.48" y="48.26" smashed="yes">
<attribute name="NAME" x="-33.909" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D97" gate="G$1" x="-30.48" y="35.56" smashed="yes">
<attribute name="NAME" x="-33.909" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D98" gate="G$1" x="-30.48" y="22.86" smashed="yes">
<attribute name="NAME" x="-33.909" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-28.575" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D99" gate="G$1" x="-20.32" y="99.06" smashed="yes">
<attribute name="NAME" x="-23.749" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D100" gate="G$1" x="-20.32" y="86.36" smashed="yes">
<attribute name="NAME" x="-23.749" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D101" gate="G$1" x="-20.32" y="73.66" smashed="yes">
<attribute name="NAME" x="-23.749" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D102" gate="G$1" x="-20.32" y="60.96" smashed="yes">
<attribute name="NAME" x="-23.749" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D103" gate="G$1" x="-20.32" y="48.26" smashed="yes">
<attribute name="NAME" x="-23.749" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D104" gate="G$1" x="-20.32" y="35.56" smashed="yes">
<attribute name="NAME" x="-23.749" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D105" gate="G$1" x="-20.32" y="22.86" smashed="yes">
<attribute name="NAME" x="-23.749" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-18.415" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D106" gate="G$1" x="-10.16" y="99.06" smashed="yes">
<attribute name="NAME" x="-13.589" y="94.488" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="94.488" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D107" gate="G$1" x="-10.16" y="86.36" smashed="yes">
<attribute name="NAME" x="-13.589" y="81.788" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="81.788" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D108" gate="G$1" x="-10.16" y="73.66" smashed="yes">
<attribute name="NAME" x="-13.589" y="69.088" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="69.088" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D109" gate="G$1" x="-10.16" y="60.96" smashed="yes">
<attribute name="NAME" x="-13.589" y="56.388" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="56.388" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D110" gate="G$1" x="-10.16" y="48.26" smashed="yes">
<attribute name="NAME" x="-13.589" y="43.688" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="43.688" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D111" gate="G$1" x="-10.16" y="35.56" smashed="yes">
<attribute name="NAME" x="-13.589" y="30.988" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="30.988" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D112" gate="G$1" x="-10.16" y="22.86" smashed="yes">
<attribute name="NAME" x="-13.589" y="18.288" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="-8.255" y="18.288" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="SOCKET16" gate="G$1" x="-88.9" y="119.38" smashed="yes">
<attribute name="NAME" x="-93.98" y="130.81" size="1.778" layer="95"/>
<attribute name="NAME" x="-92.71" y="130.81" size="1.778" layer="104"/>
</instance>
<instance part="SOCKET10" gate="PH$1" x="-185.42" y="55.88" smashed="yes" rot="MR270">
<attribute name="NAME" x="-189.23" y="50.8" size="1.778" layer="95" rot="MR270"/>
<attribute name="NAME" x="-193.04" y="59.69" size="1.778" layer="104" rot="MR270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$R1" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<junction x="-162.56" y="93.98"/>
<wire x1="-162.56" y1="93.98" x2="-152.4" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D8" gate="G$1" pin="C"/>
<junction x="-152.4" y="93.98"/>
<wire x1="-152.4" y1="93.98" x2="-142.24" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D15" gate="G$1" pin="C"/>
<junction x="-142.24" y="93.98"/>
<wire x1="-142.24" y1="93.98" x2="-132.08" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D22" gate="G$1" pin="C"/>
<junction x="-132.08" y="93.98"/>
<wire x1="-132.08" y1="93.98" x2="-121.92" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D29" gate="G$1" pin="C"/>
<junction x="-121.92" y="93.98"/>
<wire x1="-121.92" y1="93.98" x2="-111.76" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D36" gate="G$1" pin="C"/>
<junction x="-111.76" y="93.98"/>
<wire x1="-111.76" y1="93.98" x2="-101.6" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D43" gate="G$1" pin="C"/>
<junction x="-101.6" y="93.98"/>
<wire x1="-101.6" y1="93.98" x2="-91.44" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D50" gate="G$1" pin="C"/>
<junction x="-91.44" y="93.98"/>
<wire x1="-91.44" y1="93.98" x2="-81.28" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D57" gate="G$1" pin="C"/>
<junction x="-81.28" y="93.98"/>
<wire x1="-81.28" y1="93.98" x2="-71.12" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D64" gate="G$1" pin="C"/>
<junction x="-71.12" y="93.98"/>
<wire x1="-71.12" y1="93.98" x2="-60.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D71" gate="G$1" pin="C"/>
<junction x="-60.96" y="93.98"/>
<wire x1="-60.96" y1="93.98" x2="-50.8" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D78" gate="G$1" pin="C"/>
<junction x="-50.8" y="93.98"/>
<wire x1="-50.8" y1="93.98" x2="-40.64" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D85" gate="G$1" pin="C"/>
<junction x="-40.64" y="93.98"/>
<wire x1="-40.64" y1="93.98" x2="-30.48" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D92" gate="G$1" pin="C"/>
<junction x="-30.48" y="93.98"/>
<wire x1="-30.48" y1="93.98" x2="-20.32" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D99" gate="G$1" pin="C"/>
<junction x="-20.32" y="93.98"/>
<wire x1="-20.32" y1="93.98" x2="-10.16" y2="93.98" width="0.1524" layer="91"/>
<pinref part="D106" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$1"/>
<wire x1="-190.5" y1="93.98" x2="-162.56" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="59.69" x2="-190.5" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$R2" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="-162.56" y1="81.28" x2="-152.4" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="C"/>
<junction x="-152.4" y="81.28"/>
<wire x1="-152.4" y1="81.28" x2="-142.24" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="C"/>
<junction x="-142.24" y="81.28"/>
<wire x1="-142.24" y1="81.28" x2="-132.08" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D23" gate="G$1" pin="C"/>
<junction x="-132.08" y="81.28"/>
<wire x1="-132.08" y1="81.28" x2="-121.92" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D30" gate="G$1" pin="C"/>
<junction x="-121.92" y="81.28"/>
<wire x1="-121.92" y1="81.28" x2="-111.76" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D37" gate="G$1" pin="C"/>
<junction x="-111.76" y="81.28"/>
<wire x1="-111.76" y1="81.28" x2="-101.6" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D44" gate="G$1" pin="C"/>
<junction x="-101.6" y="81.28"/>
<wire x1="-101.6" y1="81.28" x2="-91.44" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D51" gate="G$1" pin="C"/>
<junction x="-91.44" y="81.28"/>
<wire x1="-91.44" y1="81.28" x2="-81.28" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D58" gate="G$1" pin="C"/>
<junction x="-81.28" y="81.28"/>
<wire x1="-81.28" y1="81.28" x2="-71.12" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D65" gate="G$1" pin="C"/>
<junction x="-71.12" y="81.28"/>
<wire x1="-71.12" y1="81.28" x2="-60.96" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D72" gate="G$1" pin="C"/>
<junction x="-60.96" y="81.28"/>
<wire x1="-60.96" y1="81.28" x2="-50.8" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D79" gate="G$1" pin="C"/>
<junction x="-50.8" y="81.28"/>
<wire x1="-50.8" y1="81.28" x2="-40.64" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D86" gate="G$1" pin="C"/>
<junction x="-40.64" y="81.28"/>
<wire x1="-40.64" y1="81.28" x2="-30.48" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D93" gate="G$1" pin="C"/>
<junction x="-30.48" y="81.28"/>
<wire x1="-30.48" y1="81.28" x2="-20.32" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D100" gate="G$1" pin="C"/>
<junction x="-20.32" y="81.28"/>
<wire x1="-20.32" y1="81.28" x2="-10.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D107" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$2"/>
<wire x1="-187.96" y1="59.69" x2="-187.96" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="81.28" x2="-162.56" y2="81.28" width="0.1524" layer="91"/>
<junction x="-162.56" y="81.28"/>
</segment>
</net>
<net name="N$R3" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="-162.56" y1="68.58" x2="-152.4" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D10" gate="G$1" pin="C"/>
<junction x="-152.4" y="68.58"/>
<wire x1="-152.4" y1="68.58" x2="-142.24" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D17" gate="G$1" pin="C"/>
<junction x="-142.24" y="68.58"/>
<wire x1="-142.24" y1="68.58" x2="-132.08" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D24" gate="G$1" pin="C"/>
<junction x="-132.08" y="68.58"/>
<wire x1="-132.08" y1="68.58" x2="-121.92" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D31" gate="G$1" pin="C"/>
<junction x="-121.92" y="68.58"/>
<wire x1="-121.92" y1="68.58" x2="-111.76" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D38" gate="G$1" pin="C"/>
<junction x="-111.76" y="68.58"/>
<wire x1="-111.76" y1="68.58" x2="-101.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D45" gate="G$1" pin="C"/>
<junction x="-101.6" y="68.58"/>
<wire x1="-101.6" y1="68.58" x2="-91.44" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D52" gate="G$1" pin="C"/>
<junction x="-91.44" y="68.58"/>
<wire x1="-91.44" y1="68.58" x2="-81.28" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D59" gate="G$1" pin="C"/>
<junction x="-81.28" y="68.58"/>
<wire x1="-81.28" y1="68.58" x2="-71.12" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D66" gate="G$1" pin="C"/>
<junction x="-71.12" y="68.58"/>
<wire x1="-71.12" y1="68.58" x2="-60.96" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D73" gate="G$1" pin="C"/>
<junction x="-60.96" y="68.58"/>
<wire x1="-60.96" y1="68.58" x2="-50.8" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D80" gate="G$1" pin="C"/>
<junction x="-50.8" y="68.58"/>
<wire x1="-50.8" y1="68.58" x2="-40.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D87" gate="G$1" pin="C"/>
<junction x="-40.64" y="68.58"/>
<wire x1="-40.64" y1="68.58" x2="-30.48" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D94" gate="G$1" pin="C"/>
<junction x="-30.48" y="68.58"/>
<wire x1="-30.48" y1="68.58" x2="-20.32" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D101" gate="G$1" pin="C"/>
<junction x="-20.32" y="68.58"/>
<wire x1="-20.32" y1="68.58" x2="-10.16" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D108" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$3"/>
<wire x1="-185.42" y1="59.69" x2="-185.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="68.58" x2="-162.56" y2="68.58" width="0.1524" layer="91"/>
<junction x="-162.56" y="68.58"/>
</segment>
</net>
<net name="N$R4" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="-162.56" y1="55.88" x2="-152.4" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="C"/>
<junction x="-152.4" y="55.88"/>
<wire x1="-152.4" y1="55.88" x2="-142.24" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="C"/>
<junction x="-142.24" y="55.88"/>
<wire x1="-142.24" y1="55.88" x2="-132.08" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D25" gate="G$1" pin="C"/>
<junction x="-132.08" y="55.88"/>
<wire x1="-132.08" y1="55.88" x2="-121.92" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D32" gate="G$1" pin="C"/>
<junction x="-121.92" y="55.88"/>
<wire x1="-121.92" y1="55.88" x2="-111.76" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D39" gate="G$1" pin="C"/>
<junction x="-111.76" y="55.88"/>
<wire x1="-111.76" y1="55.88" x2="-101.6" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D46" gate="G$1" pin="C"/>
<junction x="-101.6" y="55.88"/>
<wire x1="-101.6" y1="55.88" x2="-91.44" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D53" gate="G$1" pin="C"/>
<junction x="-91.44" y="55.88"/>
<wire x1="-91.44" y1="55.88" x2="-81.28" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D60" gate="G$1" pin="C"/>
<junction x="-81.28" y="55.88"/>
<wire x1="-81.28" y1="55.88" x2="-71.12" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D67" gate="G$1" pin="C"/>
<junction x="-71.12" y="55.88"/>
<wire x1="-71.12" y1="55.88" x2="-60.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D74" gate="G$1" pin="C"/>
<junction x="-60.96" y="55.88"/>
<wire x1="-60.96" y1="55.88" x2="-50.8" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D81" gate="G$1" pin="C"/>
<junction x="-50.8" y="55.88"/>
<wire x1="-50.8" y1="55.88" x2="-40.64" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D88" gate="G$1" pin="C"/>
<junction x="-40.64" y="55.88"/>
<wire x1="-40.64" y1="55.88" x2="-30.48" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D95" gate="G$1" pin="C"/>
<junction x="-30.48" y="55.88"/>
<wire x1="-30.48" y1="55.88" x2="-20.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D102" gate="G$1" pin="C"/>
<junction x="-20.32" y="55.88"/>
<wire x1="-20.32" y1="55.88" x2="-10.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D109" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$4"/>
<wire x1="-182.88" y1="59.69" x2="-182.88" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="63.5" x2="-170.18" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="63.5" x2="-170.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="55.88" x2="-162.56" y2="55.88" width="0.1524" layer="91"/>
<junction x="-162.56" y="55.88"/>
</segment>
</net>
<net name="N$R5" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="-162.56" y1="43.18" x2="-152.4" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="C"/>
<junction x="-152.4" y="43.18"/>
<wire x1="-152.4" y1="43.18" x2="-142.24" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D19" gate="G$1" pin="C"/>
<junction x="-142.24" y="43.18"/>
<wire x1="-142.24" y1="43.18" x2="-132.08" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D26" gate="G$1" pin="C"/>
<junction x="-132.08" y="43.18"/>
<wire x1="-132.08" y1="43.18" x2="-121.92" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D33" gate="G$1" pin="C"/>
<junction x="-121.92" y="43.18"/>
<wire x1="-121.92" y1="43.18" x2="-111.76" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D40" gate="G$1" pin="C"/>
<junction x="-111.76" y="43.18"/>
<wire x1="-111.76" y1="43.18" x2="-101.6" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D47" gate="G$1" pin="C"/>
<junction x="-101.6" y="43.18"/>
<wire x1="-101.6" y1="43.18" x2="-91.44" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D54" gate="G$1" pin="C"/>
<junction x="-91.44" y="43.18"/>
<wire x1="-91.44" y1="43.18" x2="-81.28" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D61" gate="G$1" pin="C"/>
<junction x="-81.28" y="43.18"/>
<wire x1="-81.28" y1="43.18" x2="-71.12" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D68" gate="G$1" pin="C"/>
<junction x="-71.12" y="43.18"/>
<wire x1="-71.12" y1="43.18" x2="-60.96" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D75" gate="G$1" pin="C"/>
<junction x="-60.96" y="43.18"/>
<wire x1="-60.96" y1="43.18" x2="-50.8" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D82" gate="G$1" pin="C"/>
<junction x="-50.8" y="43.18"/>
<wire x1="-50.8" y1="43.18" x2="-40.64" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D89" gate="G$1" pin="C"/>
<junction x="-40.64" y="43.18"/>
<wire x1="-40.64" y1="43.18" x2="-30.48" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D96" gate="G$1" pin="C"/>
<junction x="-30.48" y="43.18"/>
<wire x1="-30.48" y1="43.18" x2="-20.32" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D103" gate="G$1" pin="C"/>
<junction x="-20.32" y="43.18"/>
<wire x1="-20.32" y1="43.18" x2="-10.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="D110" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$5"/>
<wire x1="-180.34" y1="59.69" x2="-180.34" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="60.96" x2="-175.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="60.96" x2="-175.26" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="43.18" x2="-162.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="-162.56" y="43.18"/>
</segment>
</net>
<net name="N$R6" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="-162.56" y1="30.48" x2="-152.4" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D13" gate="G$1" pin="C"/>
<junction x="-152.4" y="30.48"/>
<wire x1="-152.4" y1="30.48" x2="-142.24" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D20" gate="G$1" pin="C"/>
<junction x="-142.24" y="30.48"/>
<wire x1="-142.24" y1="30.48" x2="-132.08" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D27" gate="G$1" pin="C"/>
<junction x="-132.08" y="30.48"/>
<wire x1="-132.08" y1="30.48" x2="-121.92" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D34" gate="G$1" pin="C"/>
<junction x="-121.92" y="30.48"/>
<wire x1="-121.92" y1="30.48" x2="-111.76" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D41" gate="G$1" pin="C"/>
<junction x="-111.76" y="30.48"/>
<wire x1="-111.76" y1="30.48" x2="-101.6" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D48" gate="G$1" pin="C"/>
<junction x="-101.6" y="30.48"/>
<wire x1="-101.6" y1="30.48" x2="-91.44" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D55" gate="G$1" pin="C"/>
<junction x="-91.44" y="30.48"/>
<wire x1="-91.44" y1="30.48" x2="-81.28" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D62" gate="G$1" pin="C"/>
<junction x="-81.28" y="30.48"/>
<wire x1="-81.28" y1="30.48" x2="-71.12" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D69" gate="G$1" pin="C"/>
<junction x="-71.12" y="30.48"/>
<wire x1="-71.12" y1="30.48" x2="-60.96" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D76" gate="G$1" pin="C"/>
<junction x="-60.96" y="30.48"/>
<wire x1="-60.96" y1="30.48" x2="-50.8" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D83" gate="G$1" pin="C"/>
<junction x="-50.8" y="30.48"/>
<wire x1="-50.8" y1="30.48" x2="-40.64" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D90" gate="G$1" pin="C"/>
<junction x="-40.64" y="30.48"/>
<wire x1="-40.64" y1="30.48" x2="-30.48" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D97" gate="G$1" pin="C"/>
<junction x="-30.48" y="30.48"/>
<wire x1="-30.48" y1="30.48" x2="-20.32" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D104" gate="G$1" pin="C"/>
<junction x="-20.32" y="30.48"/>
<wire x1="-20.32" y1="30.48" x2="-10.16" y2="30.48" width="0.1524" layer="91"/>
<pinref part="D111" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$6"/>
<wire x1="-180.34" y1="52.07" x2="-180.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="30.48" x2="-162.56" y2="30.48" width="0.1524" layer="91"/>
<junction x="-162.56" y="30.48"/>
</segment>
</net>
<net name="N$R7" class="0">
<segment>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="-162.56" y1="17.78" x2="-152.4" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D14" gate="G$1" pin="C"/>
<junction x="-152.4" y="17.78"/>
<wire x1="-152.4" y1="17.78" x2="-142.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D21" gate="G$1" pin="C"/>
<junction x="-142.24" y="17.78"/>
<wire x1="-142.24" y1="17.78" x2="-132.08" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D28" gate="G$1" pin="C"/>
<junction x="-132.08" y="17.78"/>
<wire x1="-132.08" y1="17.78" x2="-121.92" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D35" gate="G$1" pin="C"/>
<junction x="-121.92" y="17.78"/>
<wire x1="-121.92" y1="17.78" x2="-111.76" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D42" gate="G$1" pin="C"/>
<junction x="-111.76" y="17.78"/>
<wire x1="-111.76" y1="17.78" x2="-101.6" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D49" gate="G$1" pin="C"/>
<junction x="-101.6" y="17.78"/>
<wire x1="-101.6" y1="17.78" x2="-91.44" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D56" gate="G$1" pin="C"/>
<junction x="-91.44" y="17.78"/>
<wire x1="-91.44" y1="17.78" x2="-81.28" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D63" gate="G$1" pin="C"/>
<junction x="-81.28" y="17.78"/>
<wire x1="-81.28" y1="17.78" x2="-71.12" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D70" gate="G$1" pin="C"/>
<junction x="-71.12" y="17.78"/>
<wire x1="-71.12" y1="17.78" x2="-60.96" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D77" gate="G$1" pin="C"/>
<junction x="-60.96" y="17.78"/>
<wire x1="-60.96" y1="17.78" x2="-50.8" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D84" gate="G$1" pin="C"/>
<junction x="-50.8" y="17.78"/>
<wire x1="-50.8" y1="17.78" x2="-40.64" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D91" gate="G$1" pin="C"/>
<junction x="-40.64" y="17.78"/>
<wire x1="-40.64" y1="17.78" x2="-30.48" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D98" gate="G$1" pin="C"/>
<junction x="-30.48" y="17.78"/>
<wire x1="-30.48" y1="17.78" x2="-20.32" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D105" gate="G$1" pin="C"/>
<junction x="-20.32" y="17.78"/>
<wire x1="-20.32" y1="17.78" x2="-10.16" y2="17.78" width="0.1524" layer="91"/>
<pinref part="D112" gate="G$1" pin="C"/>
<pinref part="SOCKET10" gate="PH$1" pin="P$7"/>
<wire x1="-182.88" y1="52.07" x2="-182.88" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="17.78" x2="-162.56" y2="17.78" width="0.1524" layer="91"/>
<junction x="-162.56" y="17.78"/>
</segment>
</net>
<net name="N$C1" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="-162.56" y1="101.6" x2="-157.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="101.6" x2="-157.48" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="-157.48" y1="88.9" x2="-157.48" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="76.2" x2="-157.48" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="63.5" x2="-157.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="50.8" x2="-157.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="38.1" x2="-157.48" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="88.9" x2="-162.56" y2="88.9" width="0.1524" layer="91"/>
<junction x="-157.48" y="88.9"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="-157.48" y1="76.2" x2="-162.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="-157.48" y="76.2"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="-157.48" y1="63.5" x2="-162.56" y2="63.5" width="0.1524" layer="91"/>
<junction x="-157.48" y="63.5"/>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="-157.48" y1="50.8" x2="-162.56" y2="50.8" width="0.1524" layer="91"/>
<junction x="-157.48" y="50.8"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="-157.48" y1="38.1" x2="-162.56" y2="38.1" width="0.1524" layer="91"/>
<junction x="-157.48" y="38.1"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="-157.48" y1="25.4" x2="-162.56" y2="25.4" width="0.1524" layer="91"/>
<junction x="-162.56" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$1"/>
<wire x1="-162.56" y1="101.6" x2="-162.56" y2="128.27" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="128.27" x2="-99.06" y2="128.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C2" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="A"/>
<wire x1="-152.4" y1="101.6" x2="-147.32" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="101.6" x2="-147.32" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="-147.32" y1="88.9" x2="-147.32" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="76.2" x2="-147.32" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="63.5" x2="-147.32" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="50.8" x2="-147.32" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="38.1" x2="-147.32" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="88.9" x2="-152.4" y2="88.9" width="0.1524" layer="91"/>
<junction x="-147.32" y="88.9"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="-147.32" y1="76.2" x2="-152.4" y2="76.2" width="0.1524" layer="91"/>
<junction x="-147.32" y="76.2"/>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="-147.32" y1="63.5" x2="-152.4" y2="63.5" width="0.1524" layer="91"/>
<junction x="-147.32" y="63.5"/>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="-147.32" y1="50.8" x2="-152.4" y2="50.8" width="0.1524" layer="91"/>
<junction x="-147.32" y="50.8"/>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="-147.32" y1="38.1" x2="-152.4" y2="38.1" width="0.1524" layer="91"/>
<junction x="-147.32" y="38.1"/>
<pinref part="D14" gate="G$1" pin="A"/>
<wire x1="-147.32" y1="25.4" x2="-152.4" y2="25.4" width="0.1524" layer="91"/>
<junction x="-152.4" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$2"/>
<wire x1="-152.4" y1="101.6" x2="-152.4" y2="125.73" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="125.73" x2="-99.06" y2="125.73" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C3" class="0">
<segment>
<pinref part="D15" gate="G$1" pin="A"/>
<wire x1="-142.24" y1="101.6" x2="-137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="101.6" x2="-137.16" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D16" gate="G$1" pin="A"/>
<wire x1="-137.16" y1="88.9" x2="-137.16" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="76.2" x2="-137.16" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="63.5" x2="-137.16" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="50.8" x2="-137.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="38.1" x2="-137.16" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="88.9" x2="-142.24" y2="88.9" width="0.1524" layer="91"/>
<junction x="-137.16" y="88.9"/>
<pinref part="D17" gate="G$1" pin="A"/>
<wire x1="-137.16" y1="76.2" x2="-142.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="-137.16" y="76.2"/>
<pinref part="D18" gate="G$1" pin="A"/>
<wire x1="-137.16" y1="63.5" x2="-142.24" y2="63.5" width="0.1524" layer="91"/>
<junction x="-137.16" y="63.5"/>
<pinref part="D19" gate="G$1" pin="A"/>
<wire x1="-137.16" y1="50.8" x2="-142.24" y2="50.8" width="0.1524" layer="91"/>
<junction x="-137.16" y="50.8"/>
<pinref part="D20" gate="G$1" pin="A"/>
<wire x1="-137.16" y1="38.1" x2="-142.24" y2="38.1" width="0.1524" layer="91"/>
<junction x="-137.16" y="38.1"/>
<pinref part="D21" gate="G$1" pin="A"/>
<wire x1="-137.16" y1="25.4" x2="-142.24" y2="25.4" width="0.1524" layer="91"/>
<junction x="-142.24" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$3"/>
<wire x1="-142.24" y1="101.6" x2="-142.24" y2="123.19" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="123.19" x2="-99.06" y2="123.19" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C4" class="0">
<segment>
<pinref part="D22" gate="G$1" pin="A"/>
<wire x1="-132.08" y1="101.6" x2="-127" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-127" y1="101.6" x2="-127" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D23" gate="G$1" pin="A"/>
<wire x1="-127" y1="88.9" x2="-127" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-127" y1="76.2" x2="-127" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-127" y1="63.5" x2="-127" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-127" y1="50.8" x2="-127" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-127" y1="38.1" x2="-127" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-127" y1="88.9" x2="-132.08" y2="88.9" width="0.1524" layer="91"/>
<junction x="-127" y="88.9"/>
<pinref part="D24" gate="G$1" pin="A"/>
<wire x1="-127" y1="76.2" x2="-132.08" y2="76.2" width="0.1524" layer="91"/>
<junction x="-127" y="76.2"/>
<pinref part="D25" gate="G$1" pin="A"/>
<wire x1="-127" y1="63.5" x2="-132.08" y2="63.5" width="0.1524" layer="91"/>
<junction x="-127" y="63.5"/>
<pinref part="D26" gate="G$1" pin="A"/>
<wire x1="-127" y1="50.8" x2="-132.08" y2="50.8" width="0.1524" layer="91"/>
<junction x="-127" y="50.8"/>
<pinref part="D27" gate="G$1" pin="A"/>
<wire x1="-127" y1="38.1" x2="-132.08" y2="38.1" width="0.1524" layer="91"/>
<junction x="-127" y="38.1"/>
<pinref part="D28" gate="G$1" pin="A"/>
<wire x1="-127" y1="25.4" x2="-132.08" y2="25.4" width="0.1524" layer="91"/>
<junction x="-132.08" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$4"/>
<wire x1="-132.08" y1="101.6" x2="-132.08" y2="120.65" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="120.65" x2="-99.06" y2="120.65" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C5" class="0">
<segment>
<pinref part="D29" gate="G$1" pin="A"/>
<wire x1="-121.92" y1="101.6" x2="-116.84" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="101.6" x2="-116.84" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D30" gate="G$1" pin="A"/>
<wire x1="-116.84" y1="88.9" x2="-116.84" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="76.2" x2="-116.84" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="63.5" x2="-116.84" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="50.8" x2="-116.84" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="38.1" x2="-116.84" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="88.9" x2="-121.92" y2="88.9" width="0.1524" layer="91"/>
<junction x="-116.84" y="88.9"/>
<pinref part="D31" gate="G$1" pin="A"/>
<wire x1="-116.84" y1="76.2" x2="-121.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="-116.84" y="76.2"/>
<pinref part="D32" gate="G$1" pin="A"/>
<wire x1="-116.84" y1="63.5" x2="-121.92" y2="63.5" width="0.1524" layer="91"/>
<junction x="-116.84" y="63.5"/>
<pinref part="D33" gate="G$1" pin="A"/>
<wire x1="-116.84" y1="50.8" x2="-121.92" y2="50.8" width="0.1524" layer="91"/>
<junction x="-116.84" y="50.8"/>
<pinref part="D34" gate="G$1" pin="A"/>
<wire x1="-116.84" y1="38.1" x2="-121.92" y2="38.1" width="0.1524" layer="91"/>
<junction x="-116.84" y="38.1"/>
<pinref part="D35" gate="G$1" pin="A"/>
<wire x1="-116.84" y1="25.4" x2="-121.92" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="101.6" x2="-121.92" y2="118.11" width="0.1524" layer="91"/>
<junction x="-121.92" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$5"/>
<wire x1="-121.92" y1="118.11" x2="-99.06" y2="118.11" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C6" class="0">
<segment>
<pinref part="D36" gate="G$1" pin="A"/>
<wire x1="-111.76" y1="101.6" x2="-106.68" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="101.6" x2="-106.68" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D37" gate="G$1" pin="A"/>
<wire x1="-106.68" y1="88.9" x2="-106.68" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="76.2" x2="-106.68" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="63.5" x2="-106.68" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="50.8" x2="-106.68" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="38.1" x2="-106.68" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="88.9" x2="-111.76" y2="88.9" width="0.1524" layer="91"/>
<junction x="-106.68" y="88.9"/>
<pinref part="D38" gate="G$1" pin="A"/>
<wire x1="-106.68" y1="76.2" x2="-111.76" y2="76.2" width="0.1524" layer="91"/>
<junction x="-106.68" y="76.2"/>
<pinref part="D39" gate="G$1" pin="A"/>
<wire x1="-106.68" y1="63.5" x2="-111.76" y2="63.5" width="0.1524" layer="91"/>
<junction x="-106.68" y="63.5"/>
<pinref part="D40" gate="G$1" pin="A"/>
<wire x1="-106.68" y1="50.8" x2="-111.76" y2="50.8" width="0.1524" layer="91"/>
<junction x="-106.68" y="50.8"/>
<pinref part="D41" gate="G$1" pin="A"/>
<wire x1="-106.68" y1="38.1" x2="-111.76" y2="38.1" width="0.1524" layer="91"/>
<junction x="-106.68" y="38.1"/>
<pinref part="D42" gate="G$1" pin="A"/>
<wire x1="-106.68" y1="25.4" x2="-111.76" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-111.76" y1="101.6" x2="-111.76" y2="115.57" width="0.1524" layer="91"/>
<junction x="-111.76" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$6"/>
<wire x1="-111.76" y1="115.57" x2="-99.06" y2="115.57" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C7" class="0">
<segment>
<pinref part="D43" gate="G$1" pin="A"/>
<wire x1="-101.6" y1="101.6" x2="-96.52" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="101.6" x2="-96.52" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D44" gate="G$1" pin="A"/>
<wire x1="-96.52" y1="88.9" x2="-96.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="76.2" x2="-96.52" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="63.5" x2="-96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="50.8" x2="-96.52" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="38.1" x2="-96.52" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="88.9" x2="-101.6" y2="88.9" width="0.1524" layer="91"/>
<junction x="-96.52" y="88.9"/>
<pinref part="D45" gate="G$1" pin="A"/>
<wire x1="-96.52" y1="76.2" x2="-101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="-96.52" y="76.2"/>
<pinref part="D46" gate="G$1" pin="A"/>
<wire x1="-96.52" y1="63.5" x2="-101.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="-96.52" y="63.5"/>
<pinref part="D47" gate="G$1" pin="A"/>
<wire x1="-96.52" y1="50.8" x2="-101.6" y2="50.8" width="0.1524" layer="91"/>
<junction x="-96.52" y="50.8"/>
<pinref part="D48" gate="G$1" pin="A"/>
<wire x1="-96.52" y1="38.1" x2="-101.6" y2="38.1" width="0.1524" layer="91"/>
<junction x="-96.52" y="38.1"/>
<pinref part="D49" gate="G$1" pin="A"/>
<wire x1="-96.52" y1="25.4" x2="-101.6" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="101.6" x2="-101.6" y2="113.03" width="0.1524" layer="91"/>
<junction x="-101.6" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$7"/>
<wire x1="-101.6" y1="113.03" x2="-99.06" y2="113.03" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C8" class="0">
<segment>
<pinref part="D50" gate="G$1" pin="A"/>
<wire x1="-91.44" y1="101.6" x2="-86.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="101.6" x2="-86.36" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D51" gate="G$1" pin="A"/>
<wire x1="-86.36" y1="88.9" x2="-86.36" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="76.2" x2="-86.36" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="63.5" x2="-86.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="50.8" x2="-86.36" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="38.1" x2="-86.36" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="88.9" x2="-91.44" y2="88.9" width="0.1524" layer="91"/>
<junction x="-86.36" y="88.9"/>
<pinref part="D52" gate="G$1" pin="A"/>
<wire x1="-86.36" y1="76.2" x2="-91.44" y2="76.2" width="0.1524" layer="91"/>
<junction x="-86.36" y="76.2"/>
<pinref part="D53" gate="G$1" pin="A"/>
<wire x1="-86.36" y1="63.5" x2="-91.44" y2="63.5" width="0.1524" layer="91"/>
<junction x="-86.36" y="63.5"/>
<pinref part="D54" gate="G$1" pin="A"/>
<wire x1="-86.36" y1="50.8" x2="-91.44" y2="50.8" width="0.1524" layer="91"/>
<junction x="-86.36" y="50.8"/>
<pinref part="D55" gate="G$1" pin="A"/>
<wire x1="-86.36" y1="38.1" x2="-91.44" y2="38.1" width="0.1524" layer="91"/>
<junction x="-86.36" y="38.1"/>
<pinref part="D56" gate="G$1" pin="A"/>
<wire x1="-86.36" y1="25.4" x2="-91.44" y2="25.4" width="0.1524" layer="91"/>
<junction x="-91.44" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$8"/>
<wire x1="-99.06" y1="106.68" x2="-99.06" y2="110.49" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="101.6" x2="-91.44" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="106.68" x2="-99.06" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D57" gate="G$1" pin="A"/>
<wire x1="-81.28" y1="101.6" x2="-76.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="101.6" x2="-76.2" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D58" gate="G$1" pin="A"/>
<wire x1="-76.2" y1="88.9" x2="-76.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="76.2" x2="-76.2" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="63.5" x2="-76.2" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="50.8" x2="-76.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="38.1" x2="-76.2" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="88.9" x2="-81.28" y2="88.9" width="0.1524" layer="91"/>
<junction x="-76.2" y="88.9"/>
<pinref part="D59" gate="G$1" pin="A"/>
<wire x1="-76.2" y1="76.2" x2="-81.28" y2="76.2" width="0.1524" layer="91"/>
<junction x="-76.2" y="76.2"/>
<pinref part="D60" gate="G$1" pin="A"/>
<wire x1="-76.2" y1="63.5" x2="-81.28" y2="63.5" width="0.1524" layer="91"/>
<junction x="-76.2" y="63.5"/>
<pinref part="D61" gate="G$1" pin="A"/>
<wire x1="-76.2" y1="50.8" x2="-81.28" y2="50.8" width="0.1524" layer="91"/>
<junction x="-76.2" y="50.8"/>
<pinref part="D62" gate="G$1" pin="A"/>
<wire x1="-76.2" y1="38.1" x2="-81.28" y2="38.1" width="0.1524" layer="91"/>
<junction x="-76.2" y="38.1"/>
<pinref part="D63" gate="G$1" pin="A"/>
<wire x1="-76.2" y1="25.4" x2="-81.28" y2="25.4" width="0.1524" layer="91"/>
<junction x="-81.28" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$9"/>
<wire x1="-78.74" y1="128.27" x2="-76.2" y2="128.27" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="128.27" x2="-76.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="101.6" x2="-81.28" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="106.68" x2="-76.2" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C9" class="0">
<segment>
<pinref part="D64" gate="G$1" pin="A"/>
<wire x1="-71.12" y1="101.6" x2="-66.04" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="101.6" x2="-66.04" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D65" gate="G$1" pin="A"/>
<wire x1="-66.04" y1="88.9" x2="-66.04" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="76.2" x2="-66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="63.5" x2="-66.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="50.8" x2="-66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="38.1" x2="-66.04" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="88.9" x2="-71.12" y2="88.9" width="0.1524" layer="91"/>
<junction x="-66.04" y="88.9"/>
<pinref part="D66" gate="G$1" pin="A"/>
<wire x1="-66.04" y1="76.2" x2="-71.12" y2="76.2" width="0.1524" layer="91"/>
<junction x="-66.04" y="76.2"/>
<pinref part="D67" gate="G$1" pin="A"/>
<wire x1="-66.04" y1="63.5" x2="-71.12" y2="63.5" width="0.1524" layer="91"/>
<junction x="-66.04" y="63.5"/>
<pinref part="D68" gate="G$1" pin="A"/>
<wire x1="-66.04" y1="50.8" x2="-71.12" y2="50.8" width="0.1524" layer="91"/>
<junction x="-66.04" y="50.8"/>
<pinref part="D69" gate="G$1" pin="A"/>
<wire x1="-66.04" y1="38.1" x2="-71.12" y2="38.1" width="0.1524" layer="91"/>
<junction x="-66.04" y="38.1"/>
<pinref part="D70" gate="G$1" pin="A"/>
<wire x1="-66.04" y1="25.4" x2="-71.12" y2="25.4" width="0.1524" layer="91"/>
<junction x="-71.12" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$10"/>
<wire x1="-78.74" y1="125.73" x2="-71.12" y2="125.73" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="125.73" x2="-71.12" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C11" class="0">
<segment>
<pinref part="D71" gate="G$1" pin="A"/>
<wire x1="-60.96" y1="101.6" x2="-55.88" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="101.6" x2="-55.88" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D72" gate="G$1" pin="A"/>
<wire x1="-55.88" y1="88.9" x2="-55.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="76.2" x2="-55.88" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="63.5" x2="-55.88" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="50.8" x2="-55.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="38.1" x2="-55.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="88.9" x2="-60.96" y2="88.9" width="0.1524" layer="91"/>
<junction x="-55.88" y="88.9"/>
<pinref part="D73" gate="G$1" pin="A"/>
<wire x1="-55.88" y1="76.2" x2="-60.96" y2="76.2" width="0.1524" layer="91"/>
<junction x="-55.88" y="76.2"/>
<pinref part="D74" gate="G$1" pin="A"/>
<wire x1="-55.88" y1="63.5" x2="-60.96" y2="63.5" width="0.1524" layer="91"/>
<junction x="-55.88" y="63.5"/>
<pinref part="D75" gate="G$1" pin="A"/>
<wire x1="-55.88" y1="50.8" x2="-60.96" y2="50.8" width="0.1524" layer="91"/>
<junction x="-55.88" y="50.8"/>
<pinref part="D76" gate="G$1" pin="A"/>
<wire x1="-55.88" y1="38.1" x2="-60.96" y2="38.1" width="0.1524" layer="91"/>
<junction x="-55.88" y="38.1"/>
<pinref part="D77" gate="G$1" pin="A"/>
<wire x1="-55.88" y1="25.4" x2="-60.96" y2="25.4" width="0.1524" layer="91"/>
<junction x="-60.96" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$11"/>
<wire x1="-78.74" y1="123.19" x2="-60.96" y2="123.19" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="123.19" x2="-60.96" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C12" class="0">
<segment>
<pinref part="D78" gate="G$1" pin="A"/>
<wire x1="-50.8" y1="101.6" x2="-45.72" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="101.6" x2="-45.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="88.9" x2="-45.72" y2="76.2" width="0.1524" layer="91"/>
<pinref part="D79" gate="G$1" pin="A"/>
<wire x1="-45.72" y1="76.2" x2="-45.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="63.5" x2="-45.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="50.8" x2="-45.72" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="38.1" x2="-45.72" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="88.9" x2="-50.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="-45.72" y="88.9"/>
<pinref part="D80" gate="G$1" pin="A"/>
<wire x1="-45.72" y1="76.2" x2="-50.8" y2="76.2" width="0.1524" layer="91"/>
<junction x="-45.72" y="76.2"/>
<pinref part="D81" gate="G$1" pin="A"/>
<wire x1="-45.72" y1="63.5" x2="-50.8" y2="63.5" width="0.1524" layer="91"/>
<junction x="-45.72" y="63.5"/>
<pinref part="D82" gate="G$1" pin="A"/>
<wire x1="-45.72" y1="50.8" x2="-50.8" y2="50.8" width="0.1524" layer="91"/>
<junction x="-45.72" y="50.8"/>
<pinref part="D83" gate="G$1" pin="A"/>
<wire x1="-45.72" y1="38.1" x2="-50.8" y2="38.1" width="0.1524" layer="91"/>
<junction x="-45.72" y="38.1"/>
<pinref part="D84" gate="G$1" pin="A"/>
<wire x1="-45.72" y1="25.4" x2="-50.8" y2="25.4" width="0.1524" layer="91"/>
<junction x="-50.8" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$12"/>
<wire x1="-78.74" y1="120.65" x2="-50.8" y2="120.65" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="120.65" x2="-50.8" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C13" class="0">
<segment>
<pinref part="D85" gate="G$1" pin="A"/>
<wire x1="-40.64" y1="101.6" x2="-35.56" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="101.6" x2="-35.56" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D86" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="88.9" x2="-35.56" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="76.2" x2="-35.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="63.5" x2="-35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="50.8" x2="-35.56" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="38.1" x2="-35.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="88.9" x2="-40.64" y2="88.9" width="0.1524" layer="91"/>
<junction x="-35.56" y="88.9"/>
<pinref part="D87" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="76.2" x2="-40.64" y2="76.2" width="0.1524" layer="91"/>
<junction x="-35.56" y="76.2"/>
<pinref part="D88" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="63.5" x2="-40.64" y2="63.5" width="0.1524" layer="91"/>
<junction x="-35.56" y="63.5"/>
<pinref part="D89" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="50.8" x2="-40.64" y2="50.8" width="0.1524" layer="91"/>
<junction x="-35.56" y="50.8"/>
<pinref part="D90" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="38.1" x2="-40.64" y2="38.1" width="0.1524" layer="91"/>
<junction x="-35.56" y="38.1"/>
<pinref part="D91" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="25.4" x2="-40.64" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="101.6" x2="-40.64" y2="118.11" width="0.1524" layer="91"/>
<junction x="-40.64" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$13"/>
<wire x1="-78.74" y1="118.11" x2="-40.64" y2="118.11" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C14" class="0">
<segment>
<pinref part="D92" gate="G$1" pin="A"/>
<wire x1="-30.48" y1="101.6" x2="-25.4" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="101.6" x2="-25.4" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D93" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="88.9" x2="-25.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="76.2" x2="-25.4" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="63.5" x2="-25.4" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="50.8" x2="-25.4" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="38.1" x2="-25.4" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="88.9" x2="-30.48" y2="88.9" width="0.1524" layer="91"/>
<junction x="-25.4" y="88.9"/>
<pinref part="D94" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="76.2" x2="-30.48" y2="76.2" width="0.1524" layer="91"/>
<junction x="-25.4" y="76.2"/>
<pinref part="D95" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="63.5" x2="-30.48" y2="63.5" width="0.1524" layer="91"/>
<junction x="-25.4" y="63.5"/>
<pinref part="D96" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="50.8" x2="-30.48" y2="50.8" width="0.1524" layer="91"/>
<junction x="-25.4" y="50.8"/>
<pinref part="D97" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="38.1" x2="-30.48" y2="38.1" width="0.1524" layer="91"/>
<junction x="-25.4" y="38.1"/>
<pinref part="D98" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="25.4" x2="-30.48" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="101.6" x2="-30.48" y2="115.57" width="0.1524" layer="91"/>
<junction x="-30.48" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$14"/>
<wire x1="-78.74" y1="115.57" x2="-30.48" y2="115.57" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C15" class="0">
<segment>
<pinref part="D99" gate="G$1" pin="A"/>
<wire x1="-20.32" y1="101.6" x2="-15.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="101.6" x2="-15.24" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D100" gate="G$1" pin="A"/>
<wire x1="-15.24" y1="88.9" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="76.2" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="63.5" x2="-15.24" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="50.8" x2="-15.24" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="38.1" x2="-15.24" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="88.9" x2="-20.32" y2="88.9" width="0.1524" layer="91"/>
<junction x="-15.24" y="88.9"/>
<pinref part="D101" gate="G$1" pin="A"/>
<wire x1="-15.24" y1="76.2" x2="-20.32" y2="76.2" width="0.1524" layer="91"/>
<junction x="-15.24" y="76.2"/>
<pinref part="D102" gate="G$1" pin="A"/>
<wire x1="-15.24" y1="63.5" x2="-20.32" y2="63.5" width="0.1524" layer="91"/>
<junction x="-15.24" y="63.5"/>
<pinref part="D103" gate="G$1" pin="A"/>
<wire x1="-15.24" y1="50.8" x2="-20.32" y2="50.8" width="0.1524" layer="91"/>
<junction x="-15.24" y="50.8"/>
<pinref part="D104" gate="G$1" pin="A"/>
<wire x1="-15.24" y1="38.1" x2="-20.32" y2="38.1" width="0.1524" layer="91"/>
<junction x="-15.24" y="38.1"/>
<pinref part="D105" gate="G$1" pin="A"/>
<wire x1="-15.24" y1="25.4" x2="-20.32" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="101.6" x2="-20.32" y2="113.03" width="0.1524" layer="91"/>
<junction x="-20.32" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$15"/>
<wire x1="-78.74" y1="113.03" x2="-20.32" y2="113.03" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$C16" class="0">
<segment>
<pinref part="D106" gate="G$1" pin="A"/>
<wire x1="-10.16" y1="101.6" x2="-5.08" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="101.6" x2="-5.08" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D107" gate="G$1" pin="A"/>
<wire x1="-5.08" y1="88.9" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="76.2" x2="-5.08" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="63.5" x2="-5.08" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="50.8" x2="-5.08" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="38.1" x2="-5.08" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="88.9" x2="-10.16" y2="88.9" width="0.1524" layer="91"/>
<junction x="-5.08" y="88.9"/>
<pinref part="D108" gate="G$1" pin="A"/>
<wire x1="-5.08" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<junction x="-5.08" y="76.2"/>
<pinref part="D109" gate="G$1" pin="A"/>
<wire x1="-5.08" y1="63.5" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="-5.08" y="63.5"/>
<pinref part="D110" gate="G$1" pin="A"/>
<wire x1="-5.08" y1="50.8" x2="-10.16" y2="50.8" width="0.1524" layer="91"/>
<junction x="-5.08" y="50.8"/>
<pinref part="D111" gate="G$1" pin="A"/>
<wire x1="-5.08" y1="38.1" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<junction x="-5.08" y="38.1"/>
<pinref part="D112" gate="G$1" pin="A"/>
<wire x1="-5.08" y1="25.4" x2="-10.16" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="101.6" x2="-10.16" y2="110.49" width="0.1524" layer="91"/>
<junction x="-10.16" y="101.6"/>
<pinref part="SOCKET16" gate="G$1" pin="P$16"/>
<wire x1="-78.74" y1="110.49" x2="-10.16" y2="110.49" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
