<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="no"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="UDN2981">
<packages>
<package name="DIP254P762X533-18">
<wire x1="-6.985" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="21.59" x2="-3.5052" y2="21.59" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="21.59" x2="-4.1148" y2="21.59" width="0.1524" layer="21"/>
<wire x1="-4.1148" y1="21.59" x2="-6.477" y2="21.59" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="21.59" x2="-4.1148" y2="21.59" width="0" layer="21" curve="-180"/>
<wire x1="-6.985" y1="19.7612" x2="-6.985" y2="20.8788" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="20.8788" x2="-8.1788" y2="20.8788" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="20.8788" x2="-8.1788" y2="19.7612" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="19.7612" x2="-6.985" y2="19.7612" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="17.2212" x2="-6.985" y2="18.3388" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="18.3388" x2="-8.1788" y2="18.3388" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="18.3388" x2="-8.1788" y2="17.2212" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="17.2212" x2="-6.985" y2="17.2212" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="14.6812" x2="-6.985" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="15.7988" x2="-8.1788" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="15.7988" x2="-8.1788" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="14.6812" x2="-6.985" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="12.1412" x2="-6.985" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="13.2588" x2="-8.1788" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="13.2588" x2="-8.1788" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="12.1412" x2="-6.985" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="9.6012" x2="-6.985" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="10.7188" x2="-8.1788" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="10.7188" x2="-8.1788" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="9.6012" x2="-6.985" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="7.0612" x2="-6.985" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="8.1788" x2="-8.1788" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="8.1788" x2="-8.1788" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="7.0612" x2="-6.985" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="4.5212" x2="-6.985" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="5.6388" x2="-8.1788" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="5.6388" x2="-8.1788" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="4.5212" x2="-6.985" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="1.9812" x2="-6.985" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="3.0988" x2="-8.1788" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="3.0988" x2="-8.1788" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="1.9812" x2="-6.985" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-0.5588" x2="-6.985" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="0.5588" x2="-8.1788" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="0.5588" x2="-8.1788" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="-8.1788" y1="-0.5588" x2="-6.985" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.5588" x2="-0.635" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-0.5588" x2="0.5588" y2="-0.5588" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="-0.5588" x2="0.5588" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="0.5588" x2="-0.635" y2="0.5588" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="3.0988" x2="-0.635" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.9812" x2="0.5588" y2="1.9812" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="1.9812" x2="0.5588" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="3.0988" x2="-0.635" y2="3.0988" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="5.6388" x2="-0.635" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="4.5212" x2="0.5588" y2="4.5212" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="4.5212" x2="0.5588" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="5.6388" x2="-0.635" y2="5.6388" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="8.1788" x2="-0.635" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="7.0612" x2="0.5588" y2="7.0612" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="7.0612" x2="0.5588" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="8.1788" x2="-0.635" y2="8.1788" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="10.7188" x2="-0.635" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="9.6012" x2="0.5588" y2="9.6012" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="9.6012" x2="0.5588" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="10.7188" x2="-0.635" y2="10.7188" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="13.2588" x2="-0.635" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="12.1412" x2="0.5588" y2="12.1412" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="12.1412" x2="0.5588" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="13.2588" x2="-0.635" y2="13.2588" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="15.7988" x2="-0.635" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="14.6812" x2="0.5588" y2="14.6812" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="14.6812" x2="0.5588" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="15.7988" x2="-0.635" y2="15.7988" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="18.3388" x2="-0.635" y2="17.2212" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="17.2212" x2="0.5588" y2="17.2212" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="17.2212" x2="0.5588" y2="18.3388" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="18.3388" x2="-0.635" y2="18.3388" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="20.8788" x2="-0.635" y2="19.7612" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="19.7612" x2="0.5588" y2="19.7612" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="19.7612" x2="0.5588" y2="20.8788" width="0.1524" layer="51"/>
<wire x1="0.5588" y1="20.8788" x2="-0.635" y2="20.8788" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-0.635" y2="21.59" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="21.59" x2="-3.5052" y2="21.59" width="0.1524" layer="51"/>
<wire x1="-3.5052" y1="21.59" x2="-4.1148" y2="21.59" width="0.1524" layer="51"/>
<wire x1="-4.1148" y1="21.59" x2="-6.985" y2="21.59" width="0.1524" layer="51"/>
<wire x1="-6.985" y1="21.59" x2="-6.985" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-3.5052" y1="21.59" x2="-4.1148" y2="21.59" width="0" layer="51" curve="-180"/>
<text x="-8.79005" y="22.9306" size="2.08923125" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-7.27373125" y="-5.08651875" size="2.08546875" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<pad name="1" x="-7.62" y="20.32" drill="1.1176" shape="long"/>
<pad name="2" x="-7.62" y="17.78" drill="1.1176" shape="long"/>
<pad name="3" x="-7.62" y="15.24" drill="1.1176" shape="long"/>
<pad name="4" x="-7.62" y="12.7" drill="1.1176" shape="long"/>
<pad name="5" x="-7.62" y="10.16" drill="1.1176" shape="long"/>
<pad name="6" x="-7.62" y="7.62" drill="1.1176" shape="long"/>
<pad name="7" x="-7.62" y="5.08" drill="1.1176" shape="long"/>
<pad name="8" x="-7.62" y="2.54" drill="1.1176" shape="long"/>
<pad name="9" x="-7.62" y="0" drill="1.1176" shape="long"/>
<pad name="10" x="0" y="0" drill="1.1176" shape="long"/>
<pad name="11" x="0" y="2.54" drill="1.1176" shape="long"/>
<pad name="12" x="0" y="5.08" drill="1.1176" shape="long"/>
<pad name="13" x="0" y="7.62" drill="1.1176" shape="long"/>
<pad name="14" x="0" y="10.16" drill="1.1176" shape="long"/>
<pad name="15" x="0" y="12.7" drill="1.1176" shape="long"/>
<pad name="16" x="0" y="15.24" drill="1.1176" shape="long"/>
<pad name="17" x="0" y="17.78" drill="1.1176" shape="long"/>
<pad name="18" x="0" y="20.32" drill="1.1176" shape="long"/>
</package>
</packages>
<symbols>
<symbol name="UDN2981">
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="12.7" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="15.24" width="0.4064" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.4064" layer="94"/>
<text x="-4.725759375" y="19.2841" size="2.0834" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.24745" y="-28.6062" size="2.088790625" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VS" x="-17.78" y="10.16" length="middle" direction="pwr"/>
<pin name="IN1" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="IN2" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="IN3" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="IN4" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="IN5" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="IN6" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="IN7" x="-17.78" y="-10.16" length="middle" direction="in"/>
<pin name="IN8" x="-17.78" y="-12.7" length="middle" direction="in"/>
<pin name="GND" x="-17.78" y="-17.78" length="middle" direction="pas"/>
<pin name="OUT1" x="17.78" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="OUT2" x="17.78" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="OUT3" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="OUT4" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="OUT5" x="17.78" y="0" length="middle" direction="out" rot="R180"/>
<pin name="OUT6" x="17.78" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="OUT7" x="17.78" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="OUT8" x="17.78" y="-7.62" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UDN2981" prefix="U">
<description>8-Channel Source Drivers</description>
<gates>
<gate name="A" symbol="UDN2981" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIP254P762X533-18">
<connects>
<connect gate="A" pin="GND" pad="10"/>
<connect gate="A" pin="IN1" pad="1"/>
<connect gate="A" pin="IN2" pad="2"/>
<connect gate="A" pin="IN3" pad="3"/>
<connect gate="A" pin="IN4" pad="4"/>
<connect gate="A" pin="IN5" pad="5"/>
<connect gate="A" pin="IN6" pad="6"/>
<connect gate="A" pin="IN7" pad="7"/>
<connect gate="A" pin="IN8" pad="8"/>
<connect gate="A" pin="OUT1" pad="18"/>
<connect gate="A" pin="OUT2" pad="17"/>
<connect gate="A" pin="OUT3" pad="16"/>
<connect gate="A" pin="OUT4" pad="15"/>
<connect gate="A" pin="OUT5" pad="14"/>
<connect gate="A" pin="OUT6" pad="13"/>
<connect gate="A" pin="OUT7" pad="12"/>
<connect gate="A" pin="OUT8" pad="11"/>
<connect gate="A" pin="VS" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" "/>
<attribute name="MF" value="Allegro MicroSystems LLC"/>
<attribute name="MP" value="UDN2981"/>
<attribute name="PACKAGE" value="DIP-18 Allegro MicroSystems LLC"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="voiceled">
<packages>
<package name="DC-BU172752">
<pad name="P$1" x="15.2" y="0" drill="1.1" diameter="3.4" shape="long" rot="R90"/>
<pad name="P$2" x="9.72" y="4.4" drill="1.1" diameter="3"/>
<pad name="P$3" x="9.85" y="-4.4" drill="1.1" diameter="3"/>
<wire x1="0" y1="-4.4" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="4.5" width="0.127" layer="21"/>
<wire x1="0" y1="4.5" x2="13.9" y2="4.5" width="0.127" layer="21"/>
<wire x1="13.9" y1="4.5" x2="13.9" y2="-4.4" width="0.127" layer="21"/>
<wire x1="13.9" y1="-4.4" x2="0" y2="-4.4" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="-0.9" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0.9" y2="0" width="0.127" layer="21"/>
<text x="1.27" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MPE094-1-004">
<pad name="P$1" x="0" y="0" drill="0.9906" shape="long"/>
<pad name="P$2" x="0" y="-2.54" drill="0.9906" shape="long"/>
<pad name="P$3" x="0" y="-5.08" drill="0.9906" shape="long"/>
<pad name="P$4" x="0" y="-7.62" drill="0.9906" shape="long"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-10.7" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.127" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-9" width="0.127" layer="21"/>
<wire x1="1" y1="-9" x2="-1" y2="-9" width="0.127" layer="21"/>
<wire x1="-1" y1="-9" x2="-1" y2="1" width="0.127" layer="21"/>
</package>
<package name="SOT23" urn="urn:adsk.eagle:footprint:8086276/1">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
reflow soldering</description>
<wire x1="-1.973" y1="1.983" x2="1.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-1.983" x2="-1.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-1.983" x2="-1.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="1.983" x2="1.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="1.422" y1="0.66" x2="1.422" y2="-0.66" width="0.1524" layer="51"/>
<wire x1="1.422" y1="-0.66" x2="-1.422" y2="-0.66" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.66" x2="-1.422" y2="0.66" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="0.66" x2="1.422" y2="0.66" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<rectangle x1="-0.5001" y1="-0.3" x2="0.5001" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2512">
<smd name="P$1" x="-3.175" y="0" dx="3.175" dy="1.27" layer="1" rot="R90"/>
<smd name="P$2" x="3.175" y="0" dx="3.175" dy="1.27" layer="1" rot="R90"/>
</package>
<package name="FLAT-HEAD-10">
<pad name="P$1" x="0" y="0" drill="1" diameter="2" shape="square"/>
<pad name="P$6" x="2.54" y="0" drill="1" diameter="2" shape="square"/>
<pad name="P$2" x="0" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$7" x="2.54" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$3" x="0" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$8" x="2.54" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$4" x="0" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$9" x="2.54" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$5" x="0" y="-10.16" drill="1" diameter="2" shape="square"/>
<pad name="P$10" x="2.54" y="-10.16" drill="1" diameter="2" shape="square"/>
<wire x1="-3.81" y1="-15.24" x2="6.35" y2="-15.24" width="0.127" layer="21"/>
<wire x1="6.35" y1="-15.24" x2="6.35" y2="5.08" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.08" x2="-3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-15.24" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="104">&gt;NAME</text>
</package>
<package name="FLAT-HEAD-16">
<pad name="P$1" x="0" y="0" drill="1" diameter="2" shape="square"/>
<pad name="P$2" x="0" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$3" x="0" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$4" x="0" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$5" x="0" y="-10.16" drill="1" diameter="2" shape="square"/>
<pad name="P$6" x="0" y="-12.7" drill="1" diameter="2" shape="square"/>
<pad name="P$7" x="0" y="-15.24" drill="1" diameter="2" shape="square"/>
<pad name="P$8" x="0" y="-17.78" drill="1" diameter="2" shape="square"/>
<pad name="P$9" x="2.54" y="-17.78" drill="1" diameter="2" shape="square"/>
<pad name="P$10" x="2.54" y="-15.24" drill="1" diameter="2" shape="square"/>
<pad name="P$11" x="2.54" y="-12.7" drill="1" diameter="2" shape="square"/>
<pad name="P$12" x="2.54" y="-10.16" drill="1" diameter="2" shape="square"/>
<pad name="P$13" x="2.54" y="-7.62" drill="1" diameter="2" shape="square"/>
<pad name="P$14" x="2.54" y="-5.08" drill="1" diameter="2" shape="square"/>
<pad name="P$15" x="2.54" y="-2.54" drill="1" diameter="2" shape="square"/>
<pad name="P$16" x="2.54" y="0" drill="1" diameter="2" shape="square"/>
<wire x1="-3.81" y1="-22.86" x2="6.35" y2="-22.86" width="0.127" layer="21"/>
<wire x1="6.35" y1="-22.86" x2="6.35" y2="5.08" width="0.127" layer="21"/>
<wire x1="6.35" y1="5.08" x2="-3.81" y2="5.08" width="0.127" layer="21"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-22.86" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="104">&gt;NAME</text>
</package>
<package name="SUPPLY-PADS">
<smd name="P$1" x="0" y="0" dx="5.08" dy="2.54" layer="1"/>
<smd name="P$2" x="0" y="-5.08" dx="5.08" dy="2.54" layer="1"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DC-BU072752">
<pin name="1" x="5.08" y="0" length="point" direction="pwr"/>
<pin name="3" x="5.08" y="-2.54" length="point" direction="pwr"/>
<pin name="2" x="5.08" y="-5.08" length="point" direction="pwr"/>
<text x="-2.794" y="1.778" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.794" y="-8.128" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="3.556" y2="0" width="0.254" layer="94"/>
<wire x1="3.556" y1="0" x2="3.556" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.048" y1="1.27" x2="3.556" y2="0.762" width="0.254" layer="94" curve="-90"/>
<wire x1="3.048" y1="1.27" x2="2.54" y2="0.762" width="0.254" layer="94" curve="90"/>
<wire x1="3.556" y1="0" x2="3.556" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.556" y1="-0.762" x2="3.048" y2="-1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="3.048" y1="-1.27" x2="2.54" y2="-0.762" width="0.254" layer="94" curve="-90"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="-0.508" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.508" x2="2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.508" x2="-2.032" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.508" x2="-2.54" y2="0" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0.508" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.032" y1="0.508" x2="2.54" y2="0.508" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-4.064" width="0.254" layer="94"/>
<wire x1="0.762" y1="-4.064" x2="1.016" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0.762" y1="-4.064" x2="0.508" y2="-3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-1.016" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.778" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-4.318" x2="-1.016" y2="-5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="MPE094-1-004">
<pin name="P$1" x="0" y="0" length="middle"/>
<pin name="P$2" x="0" y="-2.54" length="middle"/>
<pin name="P$3" x="0" y="-5.08" length="middle"/>
<pin name="P$4" x="0" y="-7.62" length="middle"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TSM2302">
<pin name="D" x="0" y="2.8575" length="point"/>
<pin name="G" x="-3.175" y="-1.5875" length="point" rot="R90"/>
<pin name="S" x="0" y="-2.8575" length="point"/>
<text x="-5.08" y="13.97" size="1.27" layer="95">&gt;NAME</text>
<wire x1="-1.905" y1="1.5875" x2="-1.905" y2="-1.5875" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.5875" x2="-1.27" y2="1.27" width="0.508" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.9525" width="0.508" layer="94"/>
<wire x1="-1.27" y1="0.3175" x2="-1.27" y2="0" width="0.508" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.3175" width="0.508" layer="94"/>
<wire x1="-1.27" y1="-0.9525" x2="-1.27" y2="-1.27" width="0.508" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.5875" width="0.508" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.3175" y2="0" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="0.9525" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-1.905" x2="0.9525" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.3175" x2="0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="0.9525" y1="0.3175" x2="0.9525" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.9525" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.3175" x2="0.635" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.3175" x2="0.9525" y2="0.3175" width="0.254" layer="94"/>
<wire x1="0.9525" y1="0.3175" x2="1.27" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.3175" x2="0.9525" y2="-0.3175" width="0.254" layer="94"/>
<circle x="0" y="2.8575" radius="0.3175" width="0.254" layer="94"/>
<circle x="-3.175" y="-1.5875" radius="0.3175" width="0.254" layer="94"/>
<circle x="0" y="-2.8575" radius="0.3175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.5875" x2="-2.8575" y2="-1.5875" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.3175" x2="1.27" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0" x2="-0.3175" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="-0.3175" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.3175" y2="0.3175" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0.3175" x2="-0.3175" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.27" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" length="point"/>
<pin name="2" x="2.54" y="0" length="point"/>
</symbol>
<symbol name="R2512">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="1.27" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="0" length="point"/>
<pin name="2" x="2.54" y="0" length="point"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="FLAT-HEAD-10">
<pin name="P$1" x="-3.81" y="5.08" visible="off" length="short"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<pin name="P$10" x="3.81" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="P$2" x="-3.81" y="2.54" visible="off" length="short"/>
<pin name="P$9" x="3.81" y="2.54" visible="off" length="short" rot="R180"/>
<text x="-3.81" y="7.62" size="1.778" layer="104">&gt;NAME</text>
<pin name="P$3" x="-3.81" y="0" visible="off" length="short"/>
<pin name="P$8" x="3.81" y="0" visible="off" length="short" rot="R180"/>
<pin name="P$4" x="-3.81" y="-2.54" visible="off" length="short"/>
<pin name="P$7" x="3.81" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="P$5" x="-3.81" y="-5.08" visible="off" length="short"/>
<pin name="P$6" x="3.81" y="-5.08" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="FLAT-HEAD-16">
<pin name="P$1" x="-10.16" y="8.89" visible="pin" length="short"/>
<wire x1="-8.89" y1="10.16" x2="8.89" y2="10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="10.16" x2="8.89" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="-8.89" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-10.16" x2="-8.89" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<pin name="P$2" x="-10.16" y="6.35" visible="pin" length="short"/>
<pin name="P$3" x="-10.16" y="3.81" visible="pin" length="short"/>
<pin name="P$4" x="-10.16" y="1.27" visible="pin" length="short"/>
<pin name="P$5" x="-10.16" y="-1.27" visible="pin" length="short"/>
<pin name="P$6" x="-10.16" y="-3.81" visible="pin" length="short"/>
<pin name="P$7" x="-10.16" y="-6.35" visible="pin" length="short"/>
<pin name="P$8" x="-10.16" y="-8.89" visible="pin" length="short"/>
<pin name="P$9" x="10.16" y="8.89" visible="pin" length="short" rot="R180"/>
<pin name="P$10" x="10.16" y="6.35" visible="pin" length="short" rot="R180"/>
<pin name="P$11" x="10.16" y="3.81" visible="pin" length="short" rot="R180"/>
<pin name="P$12" x="10.16" y="1.27" visible="pin" length="short" rot="R180"/>
<pin name="P$13" x="10.16" y="-1.27" visible="pin" length="short" rot="R180"/>
<pin name="P$14" x="10.16" y="-3.81" visible="pin" length="short" rot="R180"/>
<pin name="P$15" x="10.16" y="-6.35" visible="pin" length="short" rot="R180"/>
<pin name="P$16" x="10.16" y="-8.89" visible="pin" length="short" rot="R180"/>
<text x="-3.81" y="11.43" size="1.778" layer="104">&gt;NAME</text>
</symbol>
<symbol name="SUPPLY-PADS">
<rectangle x1="0" y1="-2.54" x2="2.54" y2="0" layer="94"/>
<rectangle x1="0" y1="-7.62" x2="2.54" y2="-5.08" layer="94"/>
<pin name="VCC" x="0" y="-1.27" length="short" direction="pwr"/>
<pin name="GND" x="0" y="-6.35" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DC-BU072752">
<gates>
<gate name="G$1" symbol="DC-BU072752" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DC-BU172752">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$3"/>
<connect gate="G$1" pin="3" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MPE094-1-004" prefix="PIN">
<gates>
<gate name="G$1" symbol="MPE094-1-004" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MPE094-1-004">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TSM2302">
<gates>
<gate name="G$1" symbol="TSM2302" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R1206">
<gates>
<gate name="R$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R1206">
<connects>
<connect gate="R$1" pin="1" pad="1"/>
<connect gate="R$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R2512">
<gates>
<gate name="R$1" symbol="R2512" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R2512">
<connects>
<connect gate="R$1" pin="1" pad="P$1"/>
<connect gate="R$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M3-FH-10">
<gates>
<gate name="PH$1" symbol="FLAT-HEAD-10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FLAT-HEAD-10">
<connects>
<connect gate="PH$1" pin="P$1" pad="P$1"/>
<connect gate="PH$1" pin="P$10" pad="P$6"/>
<connect gate="PH$1" pin="P$2" pad="P$2"/>
<connect gate="PH$1" pin="P$3" pad="P$3"/>
<connect gate="PH$1" pin="P$4" pad="P$4"/>
<connect gate="PH$1" pin="P$5" pad="P$5"/>
<connect gate="PH$1" pin="P$6" pad="P$10"/>
<connect gate="PH$1" pin="P$7" pad="P$9"/>
<connect gate="PH$1" pin="P$8" pad="P$8"/>
<connect gate="PH$1" pin="P$9" pad="P$7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M3-FH-16">
<gates>
<gate name="G$1" symbol="FLAT-HEAD-16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FLAT-HEAD-16">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SUPPLY-PADS">
<gates>
<gate name="G$1" symbol="SUPPLY-PADS" x="-2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="SUPPLY-PADS">
<connects>
<connect gate="G$1" pin="GND" pad="P$2"/>
<connect gate="G$1" pin="VCC" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="i2c">
<description>&lt;b&gt;I²C Bus Devices&lt;/b&gt;
&lt;p&gt;
See &lt;a href="http://www.philipslogic.com/products/i2c/"&gt;http://www.philipslogic.com/products/i2c/&lt;/a&gt; for more devices.
&lt;p&gt;
Version history:&lt;br&gt;
02 Jun 2004 v1.00 Created by Eelco Huininga (&lt;a href="mailto:eelcapone@beer.com"&gt;eelcapone@NOSPAMbeer.com&lt;/a&gt;)&lt;br&gt;
30 Aug 2004 v1.01 Updated by P. Klaja (added PCF8583)&lt;br&gt;
04 Dec 2004 v1.02 Updated by Eelco Huininga (added 82B715)&lt;br&gt;
19 Dec 2004 v1.03 Bugfix: 82B715 package SO08 had pin 6 and 7 swapped&lt;br&gt;</description>
<packages>
<package name="DIL16">
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.81" y="-0.762" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.905" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PCF8574">
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="/INT" x="-10.16" y="10.16" length="short" direction="out" function="dot"/>
<pin name="A0" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="A1" x="-10.16" y="2.54" length="short" direction="in"/>
<pin name="A2" x="-10.16" y="0" length="short" direction="in"/>
<pin name="SCL" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="SDA" x="-10.16" y="-7.62" length="short"/>
<pin name="P0" x="10.16" y="10.16" length="short" rot="R180"/>
<pin name="P1" x="10.16" y="7.62" length="short" rot="R180"/>
<pin name="P2" x="10.16" y="5.08" length="short" rot="R180"/>
<pin name="P3" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="P4" x="10.16" y="0" length="short" rot="R180"/>
<pin name="P5" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="P6" x="10.16" y="-5.08" length="short" rot="R180"/>
<pin name="P7" x="10.16" y="-7.62" length="short" rot="R180"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCF8574">
<description>Remote 8-Bit I/O Expander for I²C Bus</description>
<gates>
<gate name="IC$1" symbol="PCF8574" x="0" y="0"/>
<gate name="PWR" symbol="PWRN" x="0" y="27.94"/>
</gates>
<devices>
<device name="P" package="DIL16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AP" package="DIL16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T" package="SO16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AT" package="SO16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="UH1" library="UDN2981" deviceset="UDN2981" device=""/>
<part name="UH2" library="UDN2981" deviceset="UDN2981" device=""/>
<part name="UH3" library="UDN2981" deviceset="UDN2981" device=""/>
<part name="UH4" library="UDN2981" deviceset="UDN2981" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R1" library="voiceled" deviceset="R1206" device="" value="4k7"/>
<part name="R2" library="voiceled" deviceset="R1206" device="" value="R1206"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R3" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R4" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R5" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R6" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R7" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R8" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R9" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R10" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R11" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R12" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R13" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R14" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R15" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R16" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R17" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R18" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R19" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R20" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R21" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R22" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R23" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R24" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R25" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R26" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R27" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R28" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R29" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R30" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R31" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R32" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R33" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="R34" library="voiceled" deviceset="R2512" device="" value="180"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="DC-BUCHSE" library="voiceled" deviceset="DC-BU072752" device=""/>
<part name="CTRLR" library="voiceled" deviceset="MPE094-1-004" device=""/>
<part name="T1" library="voiceled" deviceset="TSM2302" device=""/>
<part name="T2" library="voiceled" deviceset="TSM2302" device=""/>
<part name="T3" library="voiceled" deviceset="TSM2302" device=""/>
<part name="T4" library="voiceled" deviceset="TSM2302" device=""/>
<part name="T5" library="voiceled" deviceset="TSM2302" device=""/>
<part name="T6" library="voiceled" deviceset="TSM2302" device=""/>
<part name="T7" library="voiceled" deviceset="TSM2302" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="0X70" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="0X72" library="i2c" deviceset="PCF8574" device="AP" value="PCF8574AP"/>
<part name="0X74" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="0X76" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="0X78" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SOCKET10B" library="voiceled" deviceset="M3-FH-10" device=""/>
<part name="SOCKET10A" library="voiceled" deviceset="M3-FH-10" device=""/>
<part name="SOCKET16A" library="voiceled" deviceset="M3-FH-16" device=""/>
<part name="SOCKET16B" library="voiceled" deviceset="M3-FH-16" device=""/>
<part name="U$8" library="voiceled" deviceset="SUPPLY-PADS" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-64.77" y="80.01" size="1.778" layer="91">5V
SDA
SCL
GND</text>
<text x="-15.24" y="88.9" size="1.778" layer="97" rot="R90">4k7 pull-up</text>
<text x="123.19" y="74.93" size="1.778" layer="97">Column Resistors 120Ω</text>
<text x="123.19" y="29.21" size="1.778" layer="97">Column Resistors 120Ω</text>
<text x="123.19" y="-17.78" size="1.778" layer="97">Column Resistors 120Ω</text>
<text x="124.46" y="-62.23" size="1.778" layer="97">Column Resistors 120Ω</text>
<text x="-44.45" y="-13.97" size="1.778" layer="97">P1</text>
<text x="-44.45" y="-24.13" size="1.778" layer="97">P5</text>
<text x="-34.29" y="-24.13" size="1.778" layer="97">P10</text>
<text x="-34.29" y="-21.59" size="1.778" layer="97">P9</text>
<text x="19.05" y="-21.59" size="1.778" layer="97">P9</text>
<text x="19.05" y="-24.13" size="1.778" layer="97">P10</text>
<text x="6.35" y="-24.13" size="1.778" layer="97">P5</text>
<text x="6.35" y="-13.97" size="1.778" layer="97">P1</text>
</plain>
<instances>
<instance part="UH1" gate="A" x="107.95" y="88.9" smashed="yes">
<attribute name="NAME" x="104.494240625" y="100.5641" size="2.0834" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="106.51255" y="67.9138" size="2.088790625" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="UH2" gate="A" x="107.95" y="43.18" smashed="yes">
<attribute name="NAME" x="104.494240625" y="53.5741" size="2.0834" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="106.51255" y="22.1938" size="2.088790625" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="UH3" gate="A" x="107.95" y="-2.54" smashed="yes">
<attribute name="NAME" x="104.494240625" y="7.8541" size="2.0834" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="106.51255" y="-22.2562" size="2.088790625" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="UH4" gate="A" x="107.95" y="-48.26" smashed="yes">
<attribute name="NAME" x="104.494240625" y="-37.8659" size="2.0834" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="106.51255" y="-67.9762" size="2.088790625" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="P+1" gate="VCC" x="8.89" y="105.41" smashed="yes">
<attribute name="VALUE" x="6.35" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="R$1" x="-21.59" y="95.25" smashed="yes" rot="R90">
<attribute name="NAME" x="-23.114" y="100.33" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
</instance>
<instance part="R2" gate="R$1" x="-8.89" y="95.25" smashed="yes" rot="R90">
<attribute name="NAME" x="-10.414" y="100.33" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
</instance>
<instance part="GND2" gate="1" x="86.36" y="68.58" smashed="yes">
<attribute name="VALUE" x="83.82" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="86.36" y="22.86" smashed="yes">
<attribute name="VALUE" x="83.82" y="20.32" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="86.36" y="-22.86" smashed="yes">
<attribute name="VALUE" x="83.82" y="-25.4" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="86.36" y="-68.58" smashed="yes">
<attribute name="VALUE" x="83.82" y="-71.12" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="48.26" y="-53.34" smashed="yes">
<attribute name="VALUE" x="45.72" y="-55.88" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="48.26" y="-10.16" smashed="yes">
<attribute name="VALUE" x="45.72" y="-12.7" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="48.26" y="20.32" smashed="yes">
<attribute name="VALUE" x="45.72" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="48.26" y="60.96" smashed="yes">
<attribute name="VALUE" x="45.72" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="VCC" x="86.36" y="106.68" smashed="yes">
<attribute name="VALUE" x="83.82" y="104.14" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="VCC" x="86.36" y="60.96" smashed="yes">
<attribute name="VALUE" x="83.82" y="58.42" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="VCC" x="86.36" y="15.24" smashed="yes">
<attribute name="VALUE" x="83.82" y="12.7" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+5" gate="VCC" x="86.36" y="-33.02" smashed="yes">
<attribute name="VALUE" x="83.82" y="-35.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+8" gate="VCC" x="48.26" y="-5.08" smashed="yes" rot="R180">
<attribute name="VALUE" x="50.8" y="-2.54" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+9" gate="VCC" x="55.88" y="20.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="58.42" y="22.86" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+10" gate="VCC" x="55.88" y="60.96" smashed="yes" rot="R180">
<attribute name="VALUE" x="58.42" y="63.5" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+11" gate="VCC" x="58.42" y="-55.88" smashed="yes" rot="R180">
<attribute name="VALUE" x="60.96" y="-53.34" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND11" gate="1" x="-44.45" y="69.85" smashed="yes">
<attribute name="VALUE" x="-46.99" y="67.31" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="R$1" x="133.35" y="7.62" smashed="yes">
<attribute name="NAME" x="133.35" y="9.144" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R4" gate="R$1" x="143.51" y="5.08" smashed="yes">
<attribute name="NAME" x="143.51" y="6.604" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R5" gate="R$1" x="133.35" y="2.54" smashed="yes">
<attribute name="NAME" x="133.35" y="4.064" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R6" gate="R$1" x="143.51" y="0" smashed="yes">
<attribute name="NAME" x="143.51" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R7" gate="R$1" x="133.35" y="-2.54" smashed="yes">
<attribute name="NAME" x="133.35" y="-1.016" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R8" gate="R$1" x="143.51" y="-5.08" smashed="yes">
<attribute name="NAME" x="143.51" y="-3.556" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R9" gate="R$1" x="133.35" y="-7.62" smashed="yes">
<attribute name="NAME" x="133.35" y="-6.096" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R10" gate="R$1" x="143.51" y="-10.16" smashed="yes">
<attribute name="NAME" x="143.51" y="-8.636" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R11" gate="R$1" x="133.35" y="-38.1" smashed="yes">
<attribute name="NAME" x="133.35" y="-36.576" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R12" gate="R$1" x="143.51" y="-40.64" smashed="yes">
<attribute name="NAME" x="143.51" y="-39.116" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R13" gate="R$1" x="133.35" y="-43.18" smashed="yes">
<attribute name="NAME" x="133.35" y="-41.656" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R14" gate="R$1" x="143.51" y="-45.72" smashed="yes">
<attribute name="NAME" x="143.51" y="-44.196" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R15" gate="R$1" x="133.35" y="-48.26" smashed="yes">
<attribute name="NAME" x="133.35" y="-46.736" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R16" gate="R$1" x="143.51" y="-50.8" smashed="yes">
<attribute name="NAME" x="143.51" y="-49.276" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R17" gate="R$1" x="133.35" y="-53.34" smashed="yes">
<attribute name="NAME" x="133.35" y="-51.816" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R18" gate="R$1" x="143.51" y="-55.88" smashed="yes">
<attribute name="NAME" x="143.51" y="-54.356" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R19" gate="R$1" x="133.35" y="99.06" smashed="yes">
<attribute name="NAME" x="133.35" y="100.584" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R20" gate="R$1" x="143.51" y="96.52" smashed="yes">
<attribute name="NAME" x="143.51" y="98.044" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R21" gate="R$1" x="133.35" y="93.98" smashed="yes">
<attribute name="NAME" x="133.35" y="95.504" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R22" gate="R$1" x="143.51" y="91.44" smashed="yes">
<attribute name="NAME" x="143.51" y="92.964" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R23" gate="R$1" x="133.35" y="88.9" smashed="yes">
<attribute name="NAME" x="133.35" y="90.424" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R24" gate="R$1" x="143.51" y="86.36" smashed="yes">
<attribute name="NAME" x="143.51" y="87.884" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R25" gate="R$1" x="133.35" y="83.82" smashed="yes">
<attribute name="NAME" x="133.35" y="85.344" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R26" gate="R$1" x="143.51" y="81.28" smashed="yes">
<attribute name="NAME" x="143.51" y="82.804" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R27" gate="R$1" x="133.35" y="53.34" smashed="yes">
<attribute name="NAME" x="133.35" y="54.864" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R28" gate="R$1" x="143.51" y="50.8" smashed="yes">
<attribute name="NAME" x="143.51" y="52.324" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R29" gate="R$1" x="133.35" y="48.26" smashed="yes">
<attribute name="NAME" x="133.35" y="49.784" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R30" gate="R$1" x="143.51" y="45.72" smashed="yes">
<attribute name="NAME" x="143.51" y="47.244" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R31" gate="R$1" x="133.35" y="43.18" smashed="yes">
<attribute name="NAME" x="133.35" y="44.704" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R32" gate="R$1" x="143.51" y="40.64" smashed="yes">
<attribute name="NAME" x="143.51" y="42.164" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R33" gate="R$1" x="133.35" y="38.1" smashed="yes">
<attribute name="NAME" x="133.35" y="39.624" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="R34" gate="R$1" x="143.51" y="35.56" smashed="yes">
<attribute name="NAME" x="143.51" y="37.084" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="GND12" gate="1" x="8.89" y="95.25" smashed="yes">
<attribute name="VALUE" x="6.35" y="92.71" size="1.778" layer="96"/>
</instance>
<instance part="DC-BUCHSE" gate="G$1" x="21.59" y="102.87" smashed="yes" rot="MR0">
<attribute name="NAME" x="21.844" y="104.648" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.304" y="94.742" size="1.778" layer="96"/>
</instance>
<instance part="CTRLR" gate="G$1" x="-44.45" y="87.63" smashed="yes" rot="MR0">
<attribute name="NAME" x="-49.53" y="92.71" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-46.99" y="74.93" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="T1" gate="G$1" x="26.67" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="20.32" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="T2" gate="G$1" x="13.97" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="7.62" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="T3" gate="G$1" x="1.27" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="-5.08" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="T4" gate="G$1" x="-11.43" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="-17.78" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="T5" gate="G$1" x="-24.13" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="-30.48" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="T6" gate="G$1" x="-36.83" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="-43.18" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="T7" gate="G$1" x="-49.53" y="24.13" smashed="yes" rot="MR270">
<attribute name="NAME" x="-55.88" y="26.67" size="1.27" layer="95" rot="MR270"/>
</instance>
<instance part="GND1" gate="1" x="31.75" y="13.97" smashed="yes">
<attribute name="VALUE" x="29.21" y="11.43" size="1.778" layer="96"/>
</instance>
<instance part="0X70" gate="IC$1" x="-33.02" y="54.61" smashed="yes" rot="R270">
<attribute name="NAME" x="-19.05" y="54.61" size="1.778" layer="95"/>
<attribute name="VALUE" x="-45.72" y="59.69" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="0X70" gate="PWR" x="-10.16" y="-43.18" smashed="yes">
<attribute name="NAME" x="-5.715" y="-48.895" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="0X72" gate="IC$1" x="73.66" y="83.82" smashed="yes">
<attribute name="NAME" x="71.12" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="0X72" gate="PWR" x="-2.54" y="-43.18" smashed="yes">
<attribute name="NAME" x="1.905" y="-48.895" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="0X74" gate="IC$1" x="73.66" y="38.1" smashed="yes">
<attribute name="NAME" x="71.12" y="53.34" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="0X74" gate="PWR" x="5.08" y="-43.18" smashed="yes">
<attribute name="NAME" x="9.525" y="-48.895" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="0X76" gate="IC$1" x="73.66" y="-7.62" smashed="yes">
<attribute name="NAME" x="71.12" y="7.62" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="-20.32" size="1.778" layer="96"/>
</instance>
<instance part="0X76" gate="PWR" x="15.24" y="-43.18" smashed="yes">
<attribute name="NAME" x="19.685" y="-48.895" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="0X78" gate="IC$1" x="73.66" y="-53.34" smashed="yes">
<attribute name="NAME" x="71.12" y="-38.1" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="-66.04" size="1.778" layer="96"/>
</instance>
<instance part="0X78" gate="PWR" x="25.4" y="-43.18" smashed="yes">
<attribute name="NAME" x="29.845" y="-48.895" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="P+6" gate="VCC" x="25.4" y="-33.02" smashed="yes">
<attribute name="VALUE" x="22.86" y="-35.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="25.4" y="-53.34" smashed="yes">
<attribute name="VALUE" x="22.86" y="-55.88" size="1.778" layer="96"/>
</instance>
<instance part="SOCKET10B" gate="PH$1" x="13.97" y="-19.05" smashed="yes">
<attribute name="NAME" x="7.62" y="-27.94" size="1.778" layer="95"/>
<attribute name="NAME" x="10.16" y="-11.43" size="1.778" layer="104"/>
</instance>
<instance part="SOCKET10A" gate="PH$1" x="-39.37" y="-19.05" smashed="yes">
<attribute name="NAME" x="-46.99" y="-27.94" size="1.778" layer="95"/>
<attribute name="NAME" x="-43.18" y="-11.43" size="1.778" layer="104"/>
</instance>
<instance part="SOCKET16A" gate="G$1" x="163.83" y="90.17" smashed="yes">
<attribute name="NAME" x="161.29" y="101.6" size="1.778" layer="95"/>
<attribute name="NAME" x="160.02" y="101.6" size="1.778" layer="104"/>
</instance>
<instance part="SOCKET16B" gate="G$1" x="163.83" y="-1.27" smashed="yes">
<attribute name="NAME" x="161.29" y="10.16" size="1.778" layer="95"/>
<attribute name="NAME" x="160.02" y="10.16" size="1.778" layer="104"/>
</instance>
<instance part="U$8" gate="G$1" x="-21.59" y="-40.64" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-44.45" y1="87.63" x2="-44.45" y2="102.87" width="0.1524" layer="91"/>
<wire x1="-44.45" y1="102.87" x2="-21.59" y2="102.87" width="0.1524" layer="91"/>
<pinref part="R2" gate="R$1" pin="2"/>
<wire x1="-21.59" y1="102.87" x2="-8.89" y2="102.87" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="102.87" x2="8.89" y2="102.87" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="97.79" x2="-8.89" y2="102.87" width="0.1524" layer="91"/>
<junction x="-8.89" y="102.87"/>
<pinref part="R1" gate="R$1" pin="2"/>
<wire x1="-21.59" y1="97.79" x2="-21.59" y2="102.87" width="0.1524" layer="91"/>
<junction x="-21.59" y="102.87"/>
<wire x1="16.51" y1="102.87" x2="8.89" y2="102.87" width="0.1524" layer="91"/>
<junction x="8.89" y="102.87"/>
<pinref part="DC-BUCHSE" gate="G$1" pin="1"/>
<pinref part="CTRLR" gate="G$1" pin="P$1"/>
</segment>
<segment>
<pinref part="UH4" gate="A" pin="VS"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="86.36" y1="-38.1" x2="90.17" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-35.56" x2="86.36" y2="-38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="86.36" y1="12.7" x2="86.36" y2="7.62" width="0.1524" layer="91"/>
<pinref part="UH3" gate="A" pin="VS"/>
<wire x1="86.36" y1="7.62" x2="90.17" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="86.36" y1="53.34" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<pinref part="UH2" gate="A" pin="VS"/>
<wire x1="90.17" y1="53.34" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="86.36" y1="99.06" x2="86.36" y2="104.14" width="0.1524" layer="91"/>
<pinref part="UH1" gate="A" pin="VS"/>
<wire x1="86.36" y1="99.06" x2="90.17" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+10" gate="VCC" pin="VCC"/>
<wire x1="55.88" y1="63.5" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="55.88" y1="88.9" x2="63.5" y2="88.9" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="A0"/>
</segment>
<segment>
<pinref part="P+9" gate="VCC" pin="VCC"/>
<wire x1="55.88" y1="22.86" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="55.88" y1="40.64" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="A1"/>
</segment>
<segment>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<wire x1="48.26" y1="-2.54" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-2.54" x2="63.5" y2="-2.54" width="0.1524" layer="91"/>
<junction x="55.88" y="-2.54"/>
<pinref part="0X76" gate="IC$1" pin="A0"/>
<pinref part="0X76" gate="IC$1" pin="A1"/>
<wire x1="55.88" y1="-2.54" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-5.08" x2="63.5" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+11" gate="VCC" pin="VCC"/>
<wire x1="63.5" y1="-53.34" x2="58.42" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="A2"/>
</segment>
<segment>
<pinref part="0X70" gate="PWR" pin="VCC"/>
<pinref part="0X72" gate="PWR" pin="VCC"/>
<wire x1="-10.16" y1="-35.56" x2="-2.54" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="0X74" gate="PWR" pin="VCC"/>
<wire x1="5.08" y1="-35.56" x2="-2.54" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-2.54" y="-35.56"/>
<pinref part="0X76" gate="PWR" pin="VCC"/>
<wire x1="5.08" y1="-35.56" x2="15.24" y2="-35.56" width="0.1524" layer="91"/>
<junction x="5.08" y="-35.56"/>
<pinref part="0X78" gate="PWR" pin="VCC"/>
<wire x1="15.24" y1="-35.56" x2="25.4" y2="-35.56" width="0.1524" layer="91"/>
<junction x="15.24" y="-35.56"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<junction x="25.4" y="-35.56"/>
<pinref part="U$8" gate="G$1" pin="VCC"/>
<wire x1="-21.59" y1="-41.91" x2="-24.13" y2="-41.91" width="0.1524" layer="91"/>
<wire x1="-24.13" y1="-41.91" x2="-24.13" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-24.13" y1="-35.56" x2="-10.16" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-10.16" y="-35.56"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="-44.45" y1="85.09" x2="-36.83" y2="85.09" width="0.1524" layer="91"/>
<wire x1="-36.83" y1="85.09" x2="-21.59" y2="85.09" width="0.1524" layer="91"/>
<wire x1="38.1" y1="85.09" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<wire x1="38.1" y1="76.2" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R1" gate="R$1" pin="1"/>
<wire x1="38.1" y1="30.48" x2="38.1" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-15.24" x2="38.1" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-21.59" y1="92.71" x2="-21.59" y2="85.09" width="0.1524" layer="91"/>
<junction x="-21.59" y="85.09"/>
<pinref part="CTRLR" gate="G$1" pin="P$2"/>
<pinref part="0X72" gate="IC$1" pin="SDA"/>
<wire x1="38.1" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="38.1" y="76.2"/>
<pinref part="0X70" gate="IC$1" pin="SDA"/>
<wire x1="-40.64" y1="64.77" x2="-40.64" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="68.58" x2="-36.83" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-36.83" y1="68.58" x2="-36.83" y2="85.09" width="0.1524" layer="91"/>
<junction x="-36.83" y="85.09"/>
<pinref part="0X74" gate="IC$1" pin="SDA"/>
<wire x1="63.5" y1="30.48" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<junction x="38.1" y="30.48"/>
<pinref part="0X76" gate="IC$1" pin="SDA"/>
<wire x1="63.5" y1="-15.24" x2="38.1" y2="-15.24" width="0.1524" layer="91"/>
<junction x="38.1" y="-15.24"/>
<pinref part="0X78" gate="IC$1" pin="SDA"/>
<wire x1="38.1" y1="-60.96" x2="63.5" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="-21.59" y1="85.09" x2="38.1" y2="85.09" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="-44.45" y1="82.55" x2="-34.29" y2="82.55" width="0.1524" layer="91"/>
<wire x1="-34.29" y1="82.55" x2="-8.89" y2="82.55" width="0.1524" layer="91"/>
<wire x1="35.56" y1="78.74" x2="35.56" y2="82.55" width="0.1524" layer="91"/>
<wire x1="35.56" y1="78.74" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R2" gate="R$1" pin="1"/>
<wire x1="35.56" y1="33.02" x2="35.56" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-12.7" x2="35.56" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="92.71" x2="-8.89" y2="82.55" width="0.1524" layer="91"/>
<junction x="-8.89" y="82.55"/>
<pinref part="CTRLR" gate="G$1" pin="P$3"/>
<pinref part="0X72" gate="IC$1" pin="SCL"/>
<wire x1="63.5" y1="78.74" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="35.56" y="78.74"/>
<pinref part="0X70" gate="IC$1" pin="SCL"/>
<wire x1="-38.1" y1="64.77" x2="-38.1" y2="67.31" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="67.31" x2="-34.29" y2="67.31" width="0.1524" layer="91"/>
<wire x1="-34.29" y1="67.31" x2="-34.29" y2="82.55" width="0.1524" layer="91"/>
<junction x="-34.29" y="82.55"/>
<pinref part="0X74" gate="IC$1" pin="SCL"/>
<wire x1="63.5" y1="33.02" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<junction x="35.56" y="33.02"/>
<pinref part="0X76" gate="IC$1" pin="SCL"/>
<wire x1="63.5" y1="-12.7" x2="35.56" y2="-12.7" width="0.1524" layer="91"/>
<junction x="35.56" y="-12.7"/>
<pinref part="0X78" gate="IC$1" pin="SCL"/>
<wire x1="63.5" y1="-58.42" x2="35.56" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="-8.89" y1="82.55" x2="35.56" y2="82.55" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="UH1" gate="A" pin="GND"/>
<wire x1="90.17" y1="71.12" x2="86.36" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="UH2" gate="A" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="90.17" y1="25.4" x2="86.36" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="UH4" gate="A" pin="GND"/>
<wire x1="86.36" y1="-66.04" x2="90.17" y2="-66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="UH3" gate="A" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="90.17" y1="-20.32" x2="86.36" y2="-20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="0X70" gate="IC$1" pin="A0"/>
<wire x1="-44.45" y1="72.39" x2="-27.94" y2="72.39" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="72.39" x2="-27.94" y2="64.77" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="A1"/>
<wire x1="-30.48" y1="64.77" x2="-27.94" y2="64.77" width="0.1524" layer="91"/>
<junction x="-27.94" y="64.77"/>
<pinref part="0X70" gate="IC$1" pin="A2"/>
<wire x1="-30.48" y1="64.77" x2="-33.02" y2="64.77" width="0.1524" layer="91"/>
<junction x="-30.48" y="64.77"/>
<pinref part="CTRLR" gate="G$1" pin="P$4"/>
<wire x1="-44.45" y1="72.39" x2="-44.45" y2="80.01" width="0.1524" layer="91"/>
<junction x="-44.45" y="72.39"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="48.26" y1="63.5" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<wire x1="48.26" y1="83.82" x2="63.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="63.5" y1="86.36" x2="48.26" y2="86.36" width="0.1524" layer="91"/>
<wire x1="48.26" y1="86.36" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<junction x="48.26" y="83.82"/>
<pinref part="0X72" gate="IC$1" pin="A1"/>
<pinref part="0X72" gate="IC$1" pin="A2"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="48.26" y1="22.86" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="48.26" y1="38.1" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="43.18" x2="48.26" y2="43.18" width="0.1524" layer="91"/>
<wire x1="48.26" y1="43.18" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<junction x="48.26" y="38.1"/>
<pinref part="0X74" gate="IC$1" pin="A0"/>
<pinref part="0X74" gate="IC$1" pin="A2"/>
</segment>
<segment>
<wire x1="63.5" y1="-7.62" x2="48.26" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="0X76" gate="IC$1" pin="A2"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="48.26" y1="-50.8" x2="48.26" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-48.26" x2="63.5" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-50.8" x2="48.26" y2="-50.8" width="0.1524" layer="91"/>
<junction x="48.26" y="-50.8"/>
<pinref part="0X78" gate="IC$1" pin="A0"/>
<pinref part="0X78" gate="IC$1" pin="A1"/>
</segment>
<segment>
<wire x1="16.51" y1="97.79" x2="8.89" y2="97.79" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="DC-BUCHSE" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="T7" gate="G$1" pin="S"/>
<wire x1="-46.6725" y1="24.13" x2="-46.6725" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="-46.6725" y1="18.7325" x2="-44.45" y2="16.51" width="0.1524" layer="91"/>
<wire x1="-44.45" y1="16.51" x2="-31.75" y2="16.51" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-31.75" y1="16.51" x2="-19.05" y2="16.51" width="0.1524" layer="91"/>
<wire x1="-19.05" y1="16.51" x2="-6.35" y2="16.51" width="0.1524" layer="91"/>
<wire x1="-6.35" y1="16.51" x2="6.35" y2="16.51" width="0.1524" layer="91"/>
<wire x1="6.35" y1="16.51" x2="19.05" y2="16.51" width="0.1524" layer="91"/>
<wire x1="19.05" y1="16.51" x2="31.75" y2="16.51" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="S"/>
<wire x1="29.5275" y1="24.13" x2="29.5275" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="29.5275" y1="18.7325" x2="31.75" y2="16.51" width="0.1524" layer="91"/>
<junction x="31.75" y="16.51"/>
<pinref part="T2" gate="G$1" pin="S"/>
<wire x1="16.8275" y1="24.13" x2="16.8275" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="16.8275" y1="18.7325" x2="19.05" y2="16.51" width="0.1524" layer="91"/>
<junction x="19.05" y="16.51"/>
<pinref part="T3" gate="G$1" pin="S"/>
<wire x1="4.1275" y1="24.13" x2="4.1275" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="4.1275" y1="18.7325" x2="6.35" y2="16.51" width="0.1524" layer="91"/>
<junction x="6.35" y="16.51"/>
<pinref part="T4" gate="G$1" pin="S"/>
<wire x1="-8.5725" y1="24.13" x2="-8.5725" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="-8.5725" y1="18.7325" x2="-6.35" y2="16.51" width="0.1524" layer="91"/>
<junction x="-6.35" y="16.51"/>
<pinref part="T5" gate="G$1" pin="S"/>
<wire x1="-21.2725" y1="24.13" x2="-21.2725" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="-21.2725" y1="18.7325" x2="-19.05" y2="16.51" width="0.1524" layer="91"/>
<junction x="-19.05" y="16.51"/>
<pinref part="T6" gate="G$1" pin="S"/>
<wire x1="-33.9725" y1="24.13" x2="-33.9725" y2="18.7325" width="0.1524" layer="91"/>
<wire x1="-33.9725" y1="18.7325" x2="-31.75" y2="16.51" width="0.1524" layer="91"/>
<junction x="-31.75" y="16.51"/>
</segment>
<segment>
<pinref part="0X78" gate="PWR" pin="GND"/>
<pinref part="0X76" gate="PWR" pin="GND"/>
<wire x1="25.4" y1="-50.8" x2="15.24" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="0X74" gate="PWR" pin="GND"/>
<wire x1="15.24" y1="-50.8" x2="5.08" y2="-50.8" width="0.1524" layer="91"/>
<junction x="15.24" y="-50.8"/>
<pinref part="0X72" gate="PWR" pin="GND"/>
<wire x1="5.08" y1="-50.8" x2="-2.54" y2="-50.8" width="0.1524" layer="91"/>
<junction x="5.08" y="-50.8"/>
<pinref part="0X70" gate="PWR" pin="GND"/>
<wire x1="-2.54" y1="-50.8" x2="-10.16" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-2.54" y="-50.8"/>
<pinref part="GND6" gate="1" pin="GND"/>
<junction x="25.4" y="-50.8"/>
<pinref part="U$8" gate="G$1" pin="GND"/>
<wire x1="-21.59" y1="-46.99" x2="-24.13" y2="-46.99" width="0.1524" layer="91"/>
<wire x1="-24.13" y1="-46.99" x2="-24.13" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-24.13" y1="-50.8" x2="-10.16" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-10.16" y="-50.8"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN1"/>
<wire x1="83.82" y1="93.98" x2="90.17" y2="93.98" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P0"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN2"/>
<wire x1="83.82" y1="91.44" x2="90.17" y2="91.44" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN3"/>
<wire x1="83.82" y1="88.9" x2="90.17" y2="88.9" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P2"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN4"/>
<wire x1="83.82" y1="86.36" x2="90.17" y2="86.36" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P3"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN5"/>
<wire x1="90.17" y1="83.82" x2="83.82" y2="83.82" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P4"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN6"/>
<wire x1="83.82" y1="81.28" x2="90.17" y2="81.28" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P5"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN7"/>
<wire x1="90.17" y1="78.74" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P6"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="UH1" gate="A" pin="IN8"/>
<wire x1="83.82" y1="76.2" x2="90.17" y2="76.2" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P7"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN1"/>
<wire x1="83.82" y1="48.26" x2="90.17" y2="48.26" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P0"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN1"/>
<wire x1="83.82" y1="-43.18" x2="90.17" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P0"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN1"/>
<wire x1="83.82" y1="2.54" x2="90.17" y2="2.54" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P0"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN2"/>
<wire x1="90.17" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P1"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN3"/>
<wire x1="83.82" y1="43.18" x2="90.17" y2="43.18" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN4"/>
<wire x1="90.17" y1="40.64" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P3"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN5"/>
<wire x1="83.82" y1="38.1" x2="90.17" y2="38.1" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P4"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN6"/>
<wire x1="90.17" y1="35.56" x2="83.82" y2="35.56" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P5"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN7"/>
<wire x1="83.82" y1="33.02" x2="90.17" y2="33.02" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P6"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="UH2" gate="A" pin="IN8"/>
<wire x1="90.17" y1="30.48" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
<pinref part="0X74" gate="IC$1" pin="P7"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN2"/>
<wire x1="90.17" y1="0" x2="83.82" y2="0" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P1"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN3"/>
<wire x1="83.82" y1="-2.54" x2="90.17" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P2"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN4"/>
<wire x1="90.17" y1="-5.08" x2="83.82" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P3"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN5"/>
<wire x1="83.82" y1="-7.62" x2="90.17" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P4"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN6"/>
<wire x1="90.17" y1="-10.16" x2="83.82" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P5"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN7"/>
<wire x1="83.82" y1="-12.7" x2="90.17" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P6"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="UH3" gate="A" pin="IN8"/>
<wire x1="90.17" y1="-15.24" x2="83.82" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="0X76" gate="IC$1" pin="P7"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN2"/>
<wire x1="83.82" y1="-45.72" x2="90.17" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P1"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN3"/>
<wire x1="90.17" y1="-48.26" x2="83.82" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P2"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN4"/>
<wire x1="83.82" y1="-50.8" x2="90.17" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P3"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN5"/>
<wire x1="90.17" y1="-53.34" x2="83.82" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P4"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN6"/>
<wire x1="83.82" y1="-55.88" x2="90.17" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P5"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN7"/>
<wire x1="90.17" y1="-58.42" x2="83.82" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P6"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="UH4" gate="A" pin="IN8"/>
<wire x1="83.82" y1="-60.96" x2="90.17" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="0X78" gate="IC$1" pin="P7"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="135.89" y1="99.06" x2="153.67" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R19" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="153.67" y1="96.52" x2="146.05" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R20" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<wire x1="135.89" y1="93.98" x2="153.67" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R21" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="153.67" y1="91.44" x2="146.05" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R22" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="135.89" y1="88.9" x2="153.67" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R23" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$5"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT6"/>
<wire x1="140.97" y1="86.36" x2="125.73" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R24" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="135.89" y1="83.82" x2="153.67" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R25" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$7"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<wire x1="153.67" y1="81.28" x2="146.05" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R26" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="R27" gate="R$1" pin="2"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$9"/>
<wire x1="173.99" y1="99.06" x2="191.77" y2="99.06" width="0.1524" layer="91"/>
<wire x1="191.77" y1="99.06" x2="191.77" y2="53.34" width="0.1524" layer="91"/>
<wire x1="191.77" y1="53.34" x2="135.89" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="R28" gate="R$1" pin="2"/>
<wire x1="146.05" y1="50.8" x2="189.23" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$10"/>
<wire x1="189.23" y1="50.8" x2="189.23" y2="96.52" width="0.1524" layer="91"/>
<wire x1="189.23" y1="96.52" x2="173.99" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R29" gate="R$1" pin="2"/>
<wire x1="135.89" y1="48.26" x2="186.69" y2="48.26" width="0.1524" layer="91"/>
<wire x1="186.69" y1="48.26" x2="186.69" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$11"/>
<wire x1="186.69" y1="93.98" x2="173.99" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT4"/>
<wire x1="125.73" y1="45.72" x2="140.97" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R30" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="181.61" y1="43.18" x2="135.89" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R31" gate="R$1" pin="2"/>
<wire x1="181.61" y1="43.18" x2="181.61" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$13"/>
<wire x1="181.61" y1="88.9" x2="173.99" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="R32" gate="R$1" pin="2"/>
<wire x1="146.05" y1="40.64" x2="179.07" y2="40.64" width="0.1524" layer="91"/>
<wire x1="179.07" y1="40.64" x2="179.07" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$14"/>
<wire x1="179.07" y1="86.36" x2="173.99" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="R33" gate="R$1" pin="2"/>
<wire x1="135.89" y1="38.1" x2="176.53" y2="38.1" width="0.1524" layer="91"/>
<wire x1="176.53" y1="38.1" x2="176.53" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$15"/>
<wire x1="176.53" y1="83.82" x2="173.99" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT8"/>
<wire x1="125.73" y1="35.56" x2="140.97" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R34" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="135.89" y1="7.62" x2="153.67" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R3" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="153.67" y1="5.08" x2="146.05" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R4" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="135.89" y1="2.54" x2="153.67" y2="2.54" width="0.1524" layer="91"/>
<pinref part="R5" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$3"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<wire x1="153.67" y1="0" x2="146.05" y2="0" width="0.1524" layer="91"/>
<pinref part="R6" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$4"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="135.89" y1="-2.54" x2="153.67" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R7" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$5"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<wire x1="153.67" y1="-5.08" x2="146.05" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="R8" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="135.89" y1="-7.62" x2="153.67" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="R9" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$7"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="135.89" y1="-38.1" x2="191.77" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="R11" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$9"/>
<wire x1="191.77" y1="-38.1" x2="191.77" y2="7.62" width="0.1524" layer="91"/>
<wire x1="191.77" y1="7.62" x2="173.99" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="R12" gate="R$1" pin="2"/>
<wire x1="146.05" y1="-40.64" x2="189.23" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$10"/>
<wire x1="189.23" y1="-40.64" x2="189.23" y2="5.08" width="0.1524" layer="91"/>
<wire x1="189.23" y1="5.08" x2="173.99" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT4"/>
<wire x1="140.97" y1="-45.72" x2="125.73" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R14" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="135.89" y1="-48.26" x2="181.61" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="R15" gate="R$1" pin="2"/>
<wire x1="181.61" y1="-48.26" x2="181.61" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$13"/>
<wire x1="181.61" y1="-2.54" x2="173.99" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<wire x1="179.07" y1="-50.8" x2="146.05" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R16" gate="R$1" pin="2"/>
<wire x1="179.07" y1="-50.8" x2="179.07" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$14"/>
<wire x1="179.07" y1="-5.08" x2="173.99" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="135.89" y1="-53.34" x2="176.53" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="R17" gate="R$1" pin="2"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$15"/>
<wire x1="176.53" y1="-53.34" x2="176.53" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="176.53" y1="-7.62" x2="173.99" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT8"/>
<wire x1="140.97" y1="-55.88" x2="125.73" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R18" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT1"/>
<wire x1="125.73" y1="7.62" x2="130.81" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R3" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT2"/>
<wire x1="140.97" y1="5.08" x2="125.73" y2="5.08" width="0.1524" layer="91"/>
<pinref part="R4" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT3"/>
<wire x1="125.73" y1="2.54" x2="130.81" y2="2.54" width="0.1524" layer="91"/>
<pinref part="R5" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT4"/>
<wire x1="140.97" y1="0" x2="125.73" y2="0" width="0.1524" layer="91"/>
<pinref part="R6" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT5"/>
<wire x1="125.73" y1="-2.54" x2="130.81" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R7" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT6"/>
<wire x1="140.97" y1="-5.08" x2="125.73" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="R8" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT7"/>
<wire x1="125.73" y1="-7.62" x2="130.81" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="R9" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="UH3" gate="A" pin="OUT8"/>
<wire x1="140.97" y1="-10.16" x2="125.73" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R10" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT1"/>
<wire x1="125.73" y1="-38.1" x2="130.81" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="R11" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT2"/>
<wire x1="140.97" y1="-40.64" x2="125.73" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="R12" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT3"/>
<wire x1="125.73" y1="-43.18" x2="130.81" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="R13" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="R14" gate="R$1" pin="2"/>
<wire x1="146.05" y1="-45.72" x2="184.15" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="184.15" y1="-45.72" x2="184.15" y2="0" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$12"/>
<wire x1="184.15" y1="0" x2="173.99" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT5"/>
<wire x1="125.73" y1="-48.26" x2="130.81" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="R15" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT6"/>
<wire x1="140.97" y1="-50.8" x2="125.73" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R16" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="UH4" gate="A" pin="OUT7"/>
<wire x1="125.73" y1="-53.34" x2="130.81" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="R17" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="R18" gate="R$1" pin="2"/>
<wire x1="146.05" y1="-55.88" x2="173.99" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$16"/>
<wire x1="173.99" y1="-55.88" x2="173.99" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT1"/>
<wire x1="125.73" y1="99.06" x2="130.81" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R19" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT2"/>
<wire x1="140.97" y1="96.52" x2="125.73" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R20" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT3"/>
<wire x1="125.73" y1="93.98" x2="130.81" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R21" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT4"/>
<wire x1="140.97" y1="91.44" x2="125.73" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R22" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT5"/>
<wire x1="125.73" y1="88.9" x2="130.81" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R23" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="R24" gate="R$1" pin="2"/>
<wire x1="146.05" y1="86.36" x2="153.67" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$6"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT7"/>
<wire x1="125.73" y1="83.82" x2="130.81" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R25" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="UH1" gate="A" pin="OUT8"/>
<wire x1="140.97" y1="81.28" x2="125.73" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R26" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT1"/>
<wire x1="125.73" y1="53.34" x2="130.81" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R27" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT2"/>
<wire x1="140.97" y1="50.8" x2="125.73" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R28" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT3"/>
<wire x1="125.73" y1="48.26" x2="130.81" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R29" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="R30" gate="R$1" pin="2"/>
<wire x1="146.05" y1="45.72" x2="184.15" y2="45.72" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$12"/>
<wire x1="184.15" y1="45.72" x2="184.15" y2="91.44" width="0.1524" layer="91"/>
<wire x1="184.15" y1="91.44" x2="173.99" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT5"/>
<wire x1="130.81" y1="43.18" x2="125.73" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R31" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT6"/>
<wire x1="125.73" y1="40.64" x2="140.97" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R32" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="UH2" gate="A" pin="OUT7"/>
<wire x1="130.81" y1="38.1" x2="125.73" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R33" gate="R$1" pin="1"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="R34" gate="R$1" pin="2"/>
<wire x1="146.05" y1="35.56" x2="173.99" y2="35.56" width="0.1524" layer="91"/>
<wire x1="173.99" y1="35.56" x2="173.99" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SOCKET16A" gate="G$1" pin="P$16"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="R10" gate="R$1" pin="2"/>
<wire x1="146.05" y1="-10.16" x2="153.67" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$8"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="R13" gate="R$1" pin="2"/>
<wire x1="135.89" y1="-43.18" x2="186.69" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="186.69" y1="-43.18" x2="186.69" y2="2.54" width="0.1524" layer="91"/>
<pinref part="SOCKET16B" gate="G$1" pin="P$11"/>
<wire x1="186.69" y1="2.54" x2="173.99" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="-1.27" y1="8.89" x2="-49.53" y2="8.89" width="0.1524" layer="91"/>
<pinref part="T3" gate="G$1" pin="D"/>
<wire x1="-1.5875" y1="24.13" x2="-1.27" y2="24.13" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="24.13" x2="-1.27" y2="8.89" width="0.1524" layer="91"/>
<junction x="-1.27" y="8.89"/>
<wire x1="-49.53" y1="8.89" x2="-49.53" y2="-19.05" width="0.1524" layer="91"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$3"/>
<wire x1="-49.53" y1="-19.05" x2="-43.18" y2="-19.05" width="0.1524" layer="91"/>
<pinref part="SOCKET10B" gate="PH$1" pin="P$3"/>
<wire x1="1.27" y1="-19.05" x2="10.16" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="8.89" x2="1.27" y2="8.89" width="0.1524" layer="91"/>
<wire x1="1.27" y1="8.89" x2="1.27" y2="-19.05" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="-57.15" y1="-1.27" x2="-29.21" y2="-1.27" width="0.1524" layer="91"/>
<wire x1="-29.21" y1="-1.27" x2="21.59" y2="-1.27" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="-1.27" x2="-57.15" y2="24.13" width="0.1524" layer="91"/>
<pinref part="T7" gate="G$1" pin="D"/>
<wire x1="-52.3875" y1="24.13" x2="-52.07" y2="24.13" width="0.1524" layer="91"/>
<wire x1="-57.15" y1="24.13" x2="-52.3875" y2="24.13" width="0.1524" layer="91"/>
<junction x="-52.3875" y="24.13"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$7"/>
<wire x1="-35.56" y1="-21.59" x2="-29.21" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-29.21" y1="-21.59" x2="-29.21" y2="-1.27" width="0.1524" layer="91"/>
<junction x="-29.21" y="-1.27"/>
<pinref part="SOCKET10B" gate="PH$1" pin="P$7"/>
<wire x1="17.78" y1="-21.59" x2="21.59" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="21.59" y1="-21.59" x2="21.59" y2="-1.27" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="-39.37" y1="1.27" x2="-26.67" y2="1.27" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="1.27" x2="24.13" y2="1.27" width="0.1524" layer="91"/>
<pinref part="T6" gate="G$1" pin="D"/>
<wire x1="-39.6875" y1="24.13" x2="-39.37" y2="24.13" width="0.1524" layer="91"/>
<wire x1="-39.37" y1="24.13" x2="-39.37" y2="1.27" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="1.27" x2="-26.67" y2="-24.13" width="0.1524" layer="91"/>
<junction x="-26.67" y="1.27"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$6"/>
<wire x1="-26.67" y1="-24.13" x2="-35.56" y2="-24.13" width="0.1524" layer="91"/>
<wire x1="24.13" y1="1.27" x2="24.13" y2="-24.13" width="0.1524" layer="91"/>
<pinref part="SOCKET10B" gate="PH$1" pin="P$6"/>
<wire x1="17.78" y1="-24.13" x2="24.13" y2="-24.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="-46.99" y1="6.35" x2="-13.97" y2="6.35" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="6.35" x2="-1.27" y2="6.35" width="0.1524" layer="91"/>
<pinref part="T4" gate="G$1" pin="D"/>
<wire x1="-14.2875" y1="24.13" x2="-13.97" y2="24.13" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="24.13" x2="-13.97" y2="6.35" width="0.1524" layer="91"/>
<junction x="-13.97" y="6.35"/>
<wire x1="-46.99" y1="6.35" x2="-46.99" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$4"/>
<wire x1="-46.99" y1="-21.59" x2="-43.18" y2="-21.59" width="0.1524" layer="91"/>
<pinref part="SOCKET10B" gate="PH$1" pin="P$4"/>
<wire x1="-1.27" y1="-21.59" x2="10.16" y2="-21.59" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="-21.59" width="0.1524" layer="91"/>
<junction x="-1.27" y="6.35"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="-52.07" y1="11.43" x2="3.81" y2="11.43" width="0.1524" layer="91"/>
<wire x1="3.81" y1="11.43" x2="11.43" y2="11.43" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="D"/>
<wire x1="11.1125" y1="24.13" x2="11.43" y2="24.13" width="0.1524" layer="91"/>
<wire x1="11.43" y1="24.13" x2="11.43" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-52.07" y1="11.43" x2="-52.07" y2="-16.51" width="0.1524" layer="91"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$2"/>
<wire x1="-52.07" y1="-16.51" x2="-43.18" y2="-16.51" width="0.1524" layer="91"/>
<pinref part="SOCKET10B" gate="PH$1" pin="P$2"/>
<wire x1="3.81" y1="-16.51" x2="10.16" y2="-16.51" width="0.1524" layer="91"/>
<wire x1="3.81" y1="11.43" x2="3.81" y2="-16.51" width="0.1524" layer="91"/>
<junction x="3.81" y="11.43"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="-54.61" y1="-13.97" x2="-54.61" y2="13.97" width="0.1524" layer="91"/>
<wire x1="-54.61" y1="13.97" x2="6.35" y2="13.97" width="0.1524" layer="91"/>
<wire x1="6.35" y1="13.97" x2="24.13" y2="13.97" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="D"/>
<wire x1="23.8125" y1="24.13" x2="24.13" y2="24.13" width="0.1524" layer="91"/>
<wire x1="24.13" y1="24.13" x2="24.13" y2="13.97" width="0.1524" layer="91"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$1"/>
<wire x1="-54.61" y1="-13.97" x2="-43.18" y2="-13.97" width="0.1524" layer="91"/>
<pinref part="SOCKET10B" gate="PH$1" pin="P$1"/>
<wire x1="6.35" y1="-13.97" x2="10.16" y2="-13.97" width="0.1524" layer="91"/>
<wire x1="6.35" y1="13.97" x2="6.35" y2="-13.97" width="0.1524" layer="91"/>
<junction x="6.35" y="13.97"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="-22.86" y1="44.45" x2="-22.86" y2="41.91" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="41.91" x2="26.67" y2="41.91" width="0.1524" layer="91"/>
<pinref part="T1" gate="G$1" pin="G"/>
<wire x1="26.67" y1="41.91" x2="28.2575" y2="27.305" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="P0"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="-25.4" y1="44.45" x2="-25.4" y2="39.37" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="39.37" x2="15.24" y2="39.37" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="G"/>
<wire x1="15.24" y1="39.37" x2="15.5575" y2="27.305" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="P1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-27.94" y1="44.45" x2="-27.94" y2="36.83" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="36.83" x2="1.27" y2="36.83" width="0.1524" layer="91"/>
<pinref part="T3" gate="G$1" pin="G"/>
<wire x1="1.27" y1="36.83" x2="2.8575" y2="27.305" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="P2"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="-30.48" y1="44.45" x2="-30.48" y2="34.29" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="34.29" x2="-11.43" y2="34.29" width="0.1524" layer="91"/>
<pinref part="T4" gate="G$1" pin="G"/>
<wire x1="-11.43" y1="34.29" x2="-9.8425" y2="27.305" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="P3"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="-33.02" y1="44.45" x2="-33.02" y2="31.75" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="31.75" x2="-22.86" y2="31.75" width="0.1524" layer="91"/>
<pinref part="T5" gate="G$1" pin="G"/>
<wire x1="-22.86" y1="31.75" x2="-22.5425" y2="27.305" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="P4"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="-35.56" y1="44.45" x2="-35.56" y2="31.75" width="0.1524" layer="91"/>
<pinref part="T6" gate="G$1" pin="G"/>
<wire x1="-35.56" y1="31.75" x2="-35.2425" y2="27.305" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="P5"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="-38.1" y1="44.45" x2="-38.1" y2="31.75" width="0.1524" layer="91"/>
<pinref part="T7" gate="G$1" pin="G"/>
<pinref part="0X70" gate="IC$1" pin="P6"/>
<wire x1="-38.1" y1="31.75" x2="-47.9425" y2="31.75" width="0.1524" layer="91"/>
<wire x1="-47.9425" y1="31.75" x2="-47.9425" y2="27.305" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="SOCKET10B" gate="PH$1" pin="P$5"/>
<wire x1="-3.81" y1="-24.13" x2="10.16" y2="-24.13" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="3.81" x2="-26.67" y2="3.81" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="3.81" x2="-44.45" y2="3.81" width="0.1524" layer="91"/>
<pinref part="T5" gate="G$1" pin="D"/>
<wire x1="-26.9875" y1="24.13" x2="-26.67" y2="24.13" width="0.1524" layer="91"/>
<wire x1="-26.67" y1="24.13" x2="-26.67" y2="3.81" width="0.1524" layer="91"/>
<junction x="-26.67" y="3.81"/>
<wire x1="-44.45" y1="3.81" x2="-44.45" y2="-24.13" width="0.1524" layer="91"/>
<pinref part="SOCKET10A" gate="PH$1" pin="P$5"/>
<wire x1="-44.45" y1="-24.13" x2="-43.18" y2="-24.13" width="0.1524" layer="91"/>
<wire x1="-3.81" y1="-24.13" x2="-3.81" y2="3.81" width="0.1524" layer="91"/>
<junction x="-3.81" y="3.81"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="204,1,10.16,104.14,DC-BUCHSE,3,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
