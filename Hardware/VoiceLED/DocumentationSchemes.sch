<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="no"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED_5MM">
<description>&lt;B&gt;LED 5mm PTH&lt;/B&gt;&lt;p&gt;
5 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED-IR-THRU&lt;/li&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM">
<description>&lt;h3&gt;LED 3MM PTH&lt;/h3&gt;

3 mm, round.

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="22" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="22" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="22" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="22" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="22"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="22"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
</package>
<package name="LED-0603">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.2032" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED_10MM">
<description>&lt;B&gt;LED 10mm PTH&lt;/B&gt;&lt;p&gt;
10 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.2inch&lt;/li&gt;
&lt;li&gt;Diameter: 10mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="22"/>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.715" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="51"/>
</package>
<package name="FKIT-LED-1206">
<description>&lt;B&gt;LED 1206 SMT&lt;/B&gt;&lt;p&gt;
1206, surface mount
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125"x 0.06" &lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="0.3175" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="-0.3175" width="0.2032" layer="51"/>
</package>
<package name="LED_3MM-NS">
<description>&lt;h3&gt;LED 3MM PTH- No Silk&lt;/h3&gt;

3 mm, round, no silk outline of package

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.8923" y1="1.2954" x2="1.8923" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.8923" y1="-1.2954" x2="1.8923" y2="-0.8382" width="0.254" layer="21"/>
</package>
<package name="LED_5MM-KIT">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="LED-1206-BOTTOM">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
</package>
<package name="LED_5MM-KIT-NO-SILK">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="51" curve="-286.260205" cap="flat"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
</package>
<package name="PLCC-2">
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.55764375" width="0.127" layer="21"/>
<circle x="-1.905" y="1.74625" radius="0.0508" width="0.3048" layer="21"/>
<wire x1="1.75" y1="-1.4" x2="1.75" y2="1.4" width="0.2032" layer="51"/>
<wire x1="1.75" y1="1.4" x2="-1.75" y2="1.4" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="1.4" x2="-1.75" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.4" x2="1.75" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="1.4" x2="1.7" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-1.1662" y1="1.3598" x2="-1.7162" y2="0.8098" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="-1.4" x2="1.7" y2="-1.4" width="0.2032" layer="21"/>
<smd name="A" x="1.425" y="0" dx="2.4" dy="1.6" layer="1" rot="R90"/>
<smd name="C" x="-1.425" y="0" dx="2.4" dy="1.6" layer="1" rot="R90"/>
<text x="0" y="1.65995" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.5695" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="0" y="0"/>
<vertex x="0" y="0.5588" curve="90"/>
<vertex x="-0.5588" y="0"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D" uservalue="yes">
<description>&lt;b&gt;LED (Generic)&lt;/b&gt;
&lt;p&gt;Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. Generic LEDs with no color specified.&lt;/p&gt;</description>
<gates>
<gate name="D1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED_5MM">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED_3MM">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED_10MM">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED_3MM-NS">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED_5MM-KIT">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5MM-KIT-NO-SILK" package="LED_5MM-KIT-NO-SILK">
<connects>
<connect gate="D1" pin="A" pad="A"/>
<connect gate="D1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PLCC" package="PLCC-2">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X12">
<description>&lt;h3&gt;Plated Through Hole -12 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_LOCK">
<description>&lt;h3&gt;Plated Through Hole -12 Pin Locking Footprint&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole -12 Pin Locking Footprint with Long Pads&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<wire x1="16.764" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="19.304" y1="0" x2="18.796" y2="0" width="0.2032" layer="21"/>
<wire x1="21.844" y1="0" x2="21.336" y2="0" width="0.2032" layer="21"/>
<wire x1="24.384" y1="0" x2="23.876" y2="0" width="0.2032" layer="21"/>
<wire x1="26.924" y1="0" x2="26.416" y2="0" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0" x2="29.21" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.9906" x2="28.9306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0" x2="29.21" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.9906" x2="28.9306" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0" x2="28.956" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="22.86" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="25.4" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="27.94" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
<rectangle x1="17.4879" y1="-0.2921" x2="18.0721" y2="0.2921" layer="51"/>
<rectangle x1="20.0279" y1="-0.2921" x2="20.6121" y2="0.2921" layer="51"/>
<rectangle x1="22.5679" y1="-0.2921" x2="23.1521" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="25.1079" y1="-0.2921" x2="25.6921" y2="0.2921" layer="51"/>
<rectangle x1="27.6479" y1="-0.2921" x2="28.2321" y2="0.2921" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_MACHINE-PIN-HEADER_LOCK.004">
<description>&lt;h3&gt;Plated Through Hole -12 Pin Machine Pin Headers&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.67" y1="0.635" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="26.67" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.13" y1="0.635" x2="24.765" y2="1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="-1.27" x2="24.13" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="17.145" y2="1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="-1.27" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="2.54" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="5.08" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="7.62" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="10.16" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="12.7" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="15.24" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="17.78" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="20.32" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="22.86" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="25.4" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="27.94" y="0" radius="0.3302" width="0.0254" layer="51"/>
<pad name="1" x="0" y="0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="-0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="-0.1016" drill="0.889" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole -12 Pin Long Pads&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_HOLES_ONLY">
<description>&lt;h3&gt;Holes -12 Pin&lt;/h3&gt;
No plating, no silk
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="15.24" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="17.78" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="20.32" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="22.86" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="25.4" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="27.94" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
<hole x="15.24" y="0" drill="1.4732"/>
<hole x="17.78" y="0" drill="1.4732"/>
<hole x="20.32" y="0" drill="1.4732"/>
<hole x="22.86" y="0" drill="1.4732"/>
<hole x="25.4" y="0" drill="1.4732"/>
<hole x="27.94" y="0" drill="1.4732"/>
</package>
<package name="1X12_NO_SILK_KIT">
<description>&lt;h3&gt;Plated Through Hole -12 Pin No Silk Kit&lt;/h3&gt;
Mask on one side only for soldering kits. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="9" x="20.32" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="10" x="22.86" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<pad name="11" x="25.4" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<circle x="0" y="0" radius="0.508" width="0" layer="29"/>
<circle x="0" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="5.08" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="7.62" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="10.16" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="12.7" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="15.24" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="17.78" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="20.32" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="22.86" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="25.4" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.508" width="0" layer="29"/>
<circle x="5.08" y="0" radius="0.508" width="0" layer="29"/>
<circle x="7.62" y="0" radius="0.508" width="0" layer="29"/>
<circle x="10.16" y="0" radius="0.508" width="0" layer="29"/>
<circle x="12.7" y="0" radius="0.508" width="0" layer="29"/>
<circle x="15.24" y="0" radius="0.508" width="0" layer="29"/>
<circle x="17.78" y="0" radius="0.508" width="0" layer="29"/>
<circle x="20.32" y="0" radius="0.508" width="0" layer="29"/>
<circle x="22.86" y="0" radius="0.508" width="0" layer="29"/>
<circle x="25.4" y="0" radius="0.508" width="0" layer="29"/>
<pad name="12" x="27.94" y="0" drill="1.016" diameter="1.8796" rot="R90" stop="no"/>
<circle x="27.94" y="0" radius="0.9525" width="0" layer="30"/>
<circle x="27.94" y="0" radius="0.508" width="0" layer="29"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole -12 Pin No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_LOCK_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole -12 Pin Locking Footprint No Silk&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_12&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X12_SM_SQ_NOSILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="9" x="20.32" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="10" x="22.86" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="11" x="25.4" y="0" drill="1.016" diameter="1.27" shape="square"/>
<pad name="12" x="27.94" y="0" drill="1.016" diameter="1.27" shape="square"/>
<text x="0" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="1.905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X04">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<text x="2.286" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.286" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="12.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="3.4" x2="12.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="12.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.25" y1="3.15" x2="12.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="3.15" x2="12.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="12.75" y1="2.15" x2="12.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch: 1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.381" y1="-0.889" x2="0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.889" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.889" y2="-0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="-0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="2.159" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="-0.889" x2="2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="-0.889" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.429" y2="-0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="-0.889" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="3.429" y2="0.889" width="0.127" layer="21"/>
<wire x1="3.429" y1="0.889" x2="3.175" y2="0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="0.635" x2="2.921" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.159" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.159" y1="0.889" x2="1.905" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="0.889" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.889" y1="0.889" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.889" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.889" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.889" y1="-0.381" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.889" y1="0.381" x2="-0.889" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.191" y1="0.889" x2="4.699" y2="0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="0.381" x2="4.699" y2="-0.381" width="0.127" layer="21"/>
<wire x1="4.699" y1="-0.381" x2="4.191" y2="-0.889" width="0.127" layer="21"/>
<pad name="4" x="3.81" y="0" drill="0.508" diameter="1"/>
<pad name="3" x="2.54" y="0" drill="0.508" diameter="1"/>
<pad name="2" x="1.27" y="0" drill="0.508" diameter="1"/>
<pad name="1" x="0" y="0" drill="0.508" diameter="1"/>
<text x="-0.508" y="1.016" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Locking Footprint&lt;/h3&gt;
Pins are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads w/ Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center to lock pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.604" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.636" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.1176" x2="8.6106" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.127" x2="8.89" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.8636" x2="8.6106" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
<rectangle x1="7.3279" y1="-0.4191" x2="7.9121" y2="0.1651" layer="51" rot="R90"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X4_LOCK">
<description>&lt;h3&gt;Molex 4-Pin Plated Through-Hole Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<text x="2.667" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.032" y="-3.556" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_MALE">
<description>&lt;h3&gt;SMD - 4 Pin Right Angle Male Header&lt;/h3&gt;
tDocu layer shows pin locations.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
<text x="-4.318" y="6.731" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - 4 Pin No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-4-PTH">
<description>&lt;h3&gt;JST Right Angle 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/ePH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-3" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="0" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="0" drill="0.7" diameter="1.6"/>
<text x="-3.4" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<text x="0.7" y="0.9" size="0.8" layer="51">S</text>
<text x="2.7" y="0.9" size="0.8" layer="51">S</text>
<wire x1="-4.95" y1="-1.6" x2="-4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="6" x2="4.95" y2="6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-1.6" x2="-4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-1.6" x2="4.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.6" x2="-4.3" y2="0" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.6" x2="4.3" y2="0" width="0.2032" layer="21"/>
<text x="-1.397" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-4_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch -4 Pin PTH Locking&lt;/h3&gt;
Holes are offset 0.005" from center to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.3" y1="3.4" x2="12.8" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="3.4" x2="12.8" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="12.8" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="12.8" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="12.8" y1="3.15" x2="13.2" y2="3.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="3.15" x2="13.2" y2="2.15" width="0.2032" layer="51"/>
<wire x1="13.2" y1="2.15" x2="12.8" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="10.5" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="6.8222" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="3.81" y="2.413" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.81" y="1.524" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST04_1MM_RA">
<description>&lt;h3&gt;SMD- 4 Pin Right Angle &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.5" y1="-4.6" x2="1.5" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2" x2="-3" y2="-0.35" width="0.2032" layer="21"/>
<wire x1="2.25" y1="-0.35" x2="3" y2="-0.35" width="0.2032" layer="21"/>
<wire x1="3" y1="-0.35" x2="3" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-0.35" x2="-2.25" y2="-0.35" width="0.2032" layer="21"/>
<circle x="-2.5" y="0.3" radius="0.1016" width="0.2032" layer="21"/>
<smd name="NC2" x="-2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.8" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="0.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="4" x="1.5" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.397" y="-2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_VERTICAL_COMBO">
<description>&lt;h3&gt;SMD - 4 Pin Vertical Connector&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;SMD Pad count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="1.25" x2="8.99" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-0.73" y1="-1.25" x2="-1.37" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.99" y1="-1.25" x2="8.32" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="8.32" y1="1.25" x2="8.99" y2="1.25" width="0.1778" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.73" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.869" y1="-1.29" x2="6.831" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.329" y1="-1.29" x2="4.291" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.789" y1="-1.29" x2="1.751" y2="-1.29" width="0.1778" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.1778" layer="21"/>
<smd name="3" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1-2" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2-2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3-2" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4-2" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-0.508" y="2.921" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.508" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_LONG">
<description>&lt;h3&gt;SMD - 4 Pin w/ Long Solder Pads&lt;/h3&gt;
No silk, but tDocu layer shows pin position. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="5.08" y1="1.25" x2="-5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="1.25" x2="-5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-1.25" x2="-3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="3.81" y2="-1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="5.08" y2="-1.25" width="0.127" layer="51"/>
<wire x1="5.08" y1="-1.25" x2="5.08" y2="1.25" width="0.127" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="-7.25" width="0.127" layer="51"/>
<wire x1="1.27" y1="-1.25" x2="1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="-1.27" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-1.25" x2="-3.81" y2="-7.25" width="0.127" layer="51"/>
<smd name="4" x="3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-1.27" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="5.5" dx="4" dy="1" layer="1" rot="R90"/>
<hole x="-2.54" y="0" drill="1.4"/>
<hole x="2.54" y="0" drill="1.4"/>
</package>
<package name="JST-4-PTH-VERT">
<description>&lt;h3&gt;JST Vertical 4 Pin Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.jst-mfg.com/product/pdf/eng/ePH.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.95" y1="-2.25" x2="-4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="2.25" x2="4.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-4.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="4.95" y1="2.25" x2="4.95" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-3" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="2" x="-1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="3" x="1" y="-0.55" drill="0.7" diameter="1.6"/>
<pad name="4" x="3" y="-0.55" drill="0.7" diameter="1.6"/>
<text x="-1.4" y="0.75" size="1.27" layer="51">+</text>
<text x="0.6" y="0.75" size="1.27" layer="51">-</text>
<text x="2.7" y="0.95" size="0.8" layer="51">Y</text>
<text x="-3.3" y="0.95" size="0.8" layer="51">B</text>
<text x="-1.143" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X04_SMD_RA_FEMALE">
<description>&lt;h3&gt;SMD - 4 Pin Right-Angle Female Header&lt;/h3&gt;
Silk outline shows header location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:4&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.205" y1="4.25" x2="-5.205" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="4.25" x2="-5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="5.205" y1="-4.25" x2="5.205" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-5.205" y1="-4.25" x2="5.205" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-1.59" y1="6.8" x2="-0.95" y2="7.65" layer="51"/>
<rectangle x1="0.95" y1="6.8" x2="1.59" y2="7.65" layer="51"/>
<rectangle x1="-4.13" y1="6.8" x2="-3.49" y2="7.65" layer="51"/>
<smd name="3" x="1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="-1.27" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<rectangle x1="3.49" y1="6.8" x2="4.13" y2="7.65" layer="51"/>
<smd name="4" x="3.81" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_12">
<description>&lt;h3&gt; 12 Pin Connection&lt;/h3&gt;</description>
<wire x1="6.35" y1="-17.78" x2="0" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="5.08" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="5.08" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-17.78" x2="6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="0" y1="15.24" x2="6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="5.08" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="12.7" x2="5.08" y2="12.7" width="0.6096" layer="94"/>
<text x="0" y="-20.066" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="0" y="15.748" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="10.16" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="10.16" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="10.16" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_04">
<description>&lt;h3&gt;4 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_12" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208
&lt;p&gt;&lt;/p&gt;
NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.</description>
<gates>
<gate name="G$1" symbol="CONN_12" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X12_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X12_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MACHINE-PIN_LOCK" package="1X12_MACHINE-PIN-HEADER_LOCK.004">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09704" constant="no"/>
</technology>
</technologies>
</device>
<device name="LONGPADS" package="1X12_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPIN_HOLES_ONLY" package="1X12_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X12_NO_SILK_KIT" package="1X12_NO_SILK_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_SILK" package="1X12_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X12_LOCK_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SM_SQ_NO_SILK" package="1X12_SM_SQ_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_04" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208</description>
<gates>
<gate name="G$1" symbol="CONN_04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="2xCONN-08399" constant="no"/>
<attribute name="SF_ID" value="2xPRT-08084" constant="no"/>
</technology>
</technologies>
</device>
<device name="1.27MM" package="1X04_1.27MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X04_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X04_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08186" constant="no"/>
<attribute name="SF_ID" value="PRT-08231" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD" package="1X04_SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="LONGPADS" package="1X04_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="1X04_NO_SILK" package="1X04_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09696" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH" package="JST-4-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="WIRE-13531" constant="no"/>
<attribute name="SF_ID" value="PRT-09916" constant="no"/>
</technology>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-4_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="JST04_1MM_RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10310" constant="no"/>
<attribute name="SF_ID" value="PRT-10208" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMD_STRAIGHT_COMBO" package="1X04_SMD_VERTICAL_COMBO">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08511"/>
<attribute name="VALUE" value="1X04_SMD_STRAIGHT_COMBO"/>
</technology>
</technologies>
</device>
<device name="SMD_LONG" package="1X04_SMD_LONG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09140" constant="no"/>
<attribute name="SF_ID" value="PRT-12638" constant="no"/>
</technology>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-4-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13251"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X04_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12382" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="AXIAL-0.3">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="AXIAL-0.1">
<description>&lt;h3&gt;AXIAL-0.1&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="21" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1-KIT">
<description>&lt;h3&gt;AXIAL-0.1-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.1 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="100OHM" prefix="R">
<description>&lt;h3&gt;100Ω resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12181" constant="no"/>
<attribute name="VALUE" value="100" constant="no"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12181" constant="no"/>
<attribute name="VALUE" value="100" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12181"/>
<attribute name="VALUE" value="100"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12181" constant="no"/>
<attribute name="VALUE" value="100" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603-1/4W-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12438"/>
<attribute name="VALUE" value="100"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07863"/>
<attribute name="VALUE" value="100"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4.7KOHM" prefix="R">
<description>&lt;h3&gt;4.7kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07857"/>
<attribute name="VALUE" value="4.7k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="i2c">
<description>&lt;b&gt;I²C Bus Devices&lt;/b&gt;
&lt;p&gt;
See &lt;a href="http://www.philipslogic.com/products/i2c/"&gt;http://www.philipslogic.com/products/i2c/&lt;/a&gt; for more devices.
&lt;p&gt;
Version history:&lt;br&gt;
02 Jun 2004 v1.00 Created by Eelco Huininga (&lt;a href="mailto:eelcapone@beer.com"&gt;eelcapone@NOSPAMbeer.com&lt;/a&gt;)&lt;br&gt;
30 Aug 2004 v1.01 Updated by P. Klaja (added PCF8583)&lt;br&gt;
04 Dec 2004 v1.02 Updated by Eelco Huininga (added 82B715)&lt;br&gt;
19 Dec 2004 v1.03 Bugfix: 82B715 package SO08 had pin 6 and 7 swapped&lt;br&gt;</description>
<packages>
<package name="DIL16">
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.81" y="-0.762" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.905" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PCF8574">
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="/INT" x="-10.16" y="10.16" length="short" direction="out" function="dot"/>
<pin name="A0" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="A1" x="-10.16" y="2.54" length="short" direction="in"/>
<pin name="A2" x="-10.16" y="0" length="short" direction="in"/>
<pin name="SCL" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="SDA" x="-10.16" y="-7.62" length="short"/>
<pin name="P0" x="10.16" y="10.16" length="short" rot="R180"/>
<pin name="P1" x="10.16" y="7.62" length="short" rot="R180"/>
<pin name="P2" x="10.16" y="5.08" length="short" rot="R180"/>
<pin name="P3" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="P4" x="10.16" y="0" length="short" rot="R180"/>
<pin name="P5" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="P6" x="10.16" y="-5.08" length="short" rot="R180"/>
<pin name="P7" x="10.16" y="-7.62" length="short" rot="R180"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCF8574">
<description>Remote 8-Bit I/O Expander for I²C Bus</description>
<gates>
<gate name="IC$1" symbol="PCF8574" x="0" y="0"/>
<gate name="PWR" symbol="PWRN" x="0" y="27.94"/>
</gates>
<devices>
<device name="P" package="DIL16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AP" package="DIL16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T" package="SO16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AT" package="SO16">
<connects>
<connect gate="IC$1" pin="/INT" pad="13"/>
<connect gate="IC$1" pin="A0" pad="1"/>
<connect gate="IC$1" pin="A1" pad="2"/>
<connect gate="IC$1" pin="A2" pad="3"/>
<connect gate="IC$1" pin="P0" pad="4"/>
<connect gate="IC$1" pin="P1" pad="5"/>
<connect gate="IC$1" pin="P2" pad="6"/>
<connect gate="IC$1" pin="P3" pad="7"/>
<connect gate="IC$1" pin="P4" pad="9"/>
<connect gate="IC$1" pin="P5" pad="10"/>
<connect gate="IC$1" pin="P6" pad="11"/>
<connect gate="IC$1" pin="P7" pad="12"/>
<connect gate="IC$1" pin="SCL" pad="14"/>
<connect gate="IC$1" pin="SDA" pad="15"/>
<connect gate="PWR" pin="GND" pad="8"/>
<connect gate="PWR" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="uln-udn" urn="urn:adsk.eagle:library:407">
<description>&lt;b&gt;Driver Arrays&lt;/b&gt;&lt;p&gt;
ULN and UDN Series&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL18" urn="urn:adsk.eagle:footprint:30224/1" library_version="1">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="11.43" y1="2.921" x2="-11.43" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-2.921" x2="11.43" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="11.43" y1="2.921" x2="11.43" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="2.921" x2="-11.43" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="-2.921" x2="-11.43" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="1.016" x2="-11.43" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-10.16" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="0" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="10.16" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="10.16" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="0" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="-7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="-10.16" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-11.684" y="-3.048" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-9.525" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO20W" urn="urn:adsk.eagle:footprint:30234/1" library_version="1">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; wide</description>
<wire x1="6.1214" y1="3.7338" x2="-6.1214" y2="3.7338" width="0.1524" layer="21"/>
<wire x1="6.1214" y1="-3.7338" x2="6.5024" y2="-3.3528" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.5024" y1="3.3528" x2="-6.1214" y2="3.7338" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.1214" y1="3.7338" x2="6.5024" y2="3.3528" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.5024" y1="-3.3528" x2="-6.1214" y2="-3.7338" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.1214" y1="-3.7338" x2="6.1214" y2="-3.7338" width="0.1524" layer="21"/>
<wire x1="6.5024" y1="-3.3528" x2="6.5024" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="-6.5024" y1="3.3528" x2="-6.5024" y2="-3.3528" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-3.3782" x2="6.477" y2="-3.3782" width="0.0508" layer="21"/>
<wire x1="-6.5024" y1="1.27" x2="-6.5024" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<smd name="1" x="-5.715" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-4.445" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-3.175" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-1.905" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="-0.635" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="0.635" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="1.905" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="3.175" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="3.175" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="1.905" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="0.635" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-0.635" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="17" x="-1.905" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="18" x="-3.175" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="19" x="-4.445" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="20" x="-5.715" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="5.715" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="4.445" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="5.715" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.826" y="-0.508" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="-3.556" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-5.969" y1="-3.8608" x2="-5.461" y2="-3.7338" layer="21"/>
<rectangle x1="-5.969" y1="-5.334" x2="-5.461" y2="-3.8608" layer="51"/>
<rectangle x1="-4.699" y1="-3.8608" x2="-4.191" y2="-3.7338" layer="21"/>
<rectangle x1="-4.699" y1="-5.334" x2="-4.191" y2="-3.8608" layer="51"/>
<rectangle x1="-3.429" y1="-3.8608" x2="-2.921" y2="-3.7338" layer="21"/>
<rectangle x1="-3.429" y1="-5.334" x2="-2.921" y2="-3.8608" layer="51"/>
<rectangle x1="-2.159" y1="-3.8608" x2="-1.651" y2="-3.7338" layer="21"/>
<rectangle x1="-2.159" y1="-5.334" x2="-1.651" y2="-3.8608" layer="51"/>
<rectangle x1="-0.889" y1="-5.334" x2="-0.381" y2="-3.8608" layer="51"/>
<rectangle x1="-0.889" y1="-3.8608" x2="-0.381" y2="-3.7338" layer="21"/>
<rectangle x1="0.381" y1="-3.8608" x2="0.889" y2="-3.7338" layer="21"/>
<rectangle x1="0.381" y1="-5.334" x2="0.889" y2="-3.8608" layer="51"/>
<rectangle x1="1.651" y1="-3.8608" x2="2.159" y2="-3.7338" layer="21"/>
<rectangle x1="1.651" y1="-5.334" x2="2.159" y2="-3.8608" layer="51"/>
<rectangle x1="2.921" y1="-3.8608" x2="3.429" y2="-3.7338" layer="21"/>
<rectangle x1="2.921" y1="-5.334" x2="3.429" y2="-3.8608" layer="51"/>
<rectangle x1="-5.969" y1="3.8608" x2="-5.461" y2="5.334" layer="51"/>
<rectangle x1="-5.969" y1="3.7338" x2="-5.461" y2="3.8608" layer="21"/>
<rectangle x1="-4.699" y1="3.7338" x2="-4.191" y2="3.8608" layer="21"/>
<rectangle x1="-4.699" y1="3.8608" x2="-4.191" y2="5.334" layer="51"/>
<rectangle x1="-3.429" y1="3.7338" x2="-2.921" y2="3.8608" layer="21"/>
<rectangle x1="-3.429" y1="3.8608" x2="-2.921" y2="5.334" layer="51"/>
<rectangle x1="-2.159" y1="3.7338" x2="-1.651" y2="3.8608" layer="21"/>
<rectangle x1="-2.159" y1="3.8608" x2="-1.651" y2="5.334" layer="51"/>
<rectangle x1="-0.889" y1="3.7338" x2="-0.381" y2="3.8608" layer="21"/>
<rectangle x1="-0.889" y1="3.8608" x2="-0.381" y2="5.334" layer="51"/>
<rectangle x1="0.381" y1="3.7338" x2="0.889" y2="3.8608" layer="21"/>
<rectangle x1="0.381" y1="3.8608" x2="0.889" y2="5.334" layer="51"/>
<rectangle x1="1.651" y1="3.7338" x2="2.159" y2="3.8608" layer="21"/>
<rectangle x1="1.651" y1="3.8608" x2="2.159" y2="5.334" layer="51"/>
<rectangle x1="2.921" y1="3.7338" x2="3.429" y2="3.8608" layer="21"/>
<rectangle x1="2.921" y1="3.8608" x2="3.429" y2="5.334" layer="51"/>
<rectangle x1="4.191" y1="3.7338" x2="4.699" y2="3.8608" layer="21"/>
<rectangle x1="5.461" y1="3.7338" x2="5.969" y2="3.8608" layer="21"/>
<rectangle x1="4.191" y1="3.8608" x2="4.699" y2="5.334" layer="51"/>
<rectangle x1="5.461" y1="3.8608" x2="5.969" y2="5.334" layer="51"/>
<rectangle x1="4.191" y1="-3.8608" x2="4.699" y2="-3.7338" layer="21"/>
<rectangle x1="5.461" y1="-3.8608" x2="5.969" y2="-3.7338" layer="21"/>
<rectangle x1="4.191" y1="-5.334" x2="4.699" y2="-3.8608" layer="51"/>
<rectangle x1="5.461" y1="-5.334" x2="5.969" y2="-3.8608" layer="51"/>
</package>
<package name="SO18W" urn="urn:adsk.eagle:footprint:30235/1" library_version="1">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; wide</description>
<wire x1="5.4864" y1="3.7338" x2="-5.4864" y2="3.7338" width="0.1524" layer="21"/>
<wire x1="5.4864" y1="-3.7338" x2="5.8674" y2="-3.3528" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.8674" y1="3.3528" x2="-5.4864" y2="3.7338" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.4864" y1="3.7338" x2="5.8674" y2="3.3528" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.8674" y1="-3.3528" x2="-5.4864" y2="-3.7338" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.4864" y1="-3.7338" x2="5.4864" y2="-3.7338" width="0.1524" layer="21"/>
<wire x1="5.8674" y1="-3.3528" x2="5.8674" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="-5.8674" y1="3.3528" x2="-5.8674" y2="-3.3528" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-3.3782" x2="5.842" y2="-3.3782" width="0.0508" layer="21"/>
<wire x1="-5.8674" y1="1.27" x2="-5.8674" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<smd name="1" x="-5.08" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.81" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-2.54" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-1.27" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.27" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="2.54" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="3.81" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="3.81" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="2.54" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="1.27" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="0" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-1.27" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-2.54" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="17" x="-3.81" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="18" x="-5.08" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="5.08" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="5.08" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.191" y="-0.508" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.223" y="-3.556" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-5.334" y1="-3.8608" x2="-4.826" y2="-3.7338" layer="21"/>
<rectangle x1="-5.334" y1="-5.334" x2="-4.826" y2="-3.8608" layer="51"/>
<rectangle x1="-4.064" y1="-3.8608" x2="-3.556" y2="-3.7338" layer="21"/>
<rectangle x1="-4.064" y1="-5.334" x2="-3.556" y2="-3.8608" layer="51"/>
<rectangle x1="-2.794" y1="-3.8608" x2="-2.286" y2="-3.7338" layer="21"/>
<rectangle x1="-2.794" y1="-5.334" x2="-2.286" y2="-3.8608" layer="51"/>
<rectangle x1="-1.524" y1="-3.8608" x2="-1.016" y2="-3.7338" layer="21"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-3.8608" layer="51"/>
<rectangle x1="-0.254" y1="-5.334" x2="0.254" y2="-3.8608" layer="51"/>
<rectangle x1="-0.254" y1="-3.8608" x2="0.254" y2="-3.7338" layer="21"/>
<rectangle x1="1.016" y1="-3.8608" x2="1.524" y2="-3.7338" layer="21"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-3.8608" layer="51"/>
<rectangle x1="2.286" y1="-3.8608" x2="2.794" y2="-3.7338" layer="21"/>
<rectangle x1="2.286" y1="-5.334" x2="2.794" y2="-3.8608" layer="51"/>
<rectangle x1="3.556" y1="-3.8608" x2="4.064" y2="-3.7338" layer="21"/>
<rectangle x1="3.556" y1="-5.334" x2="4.064" y2="-3.8608" layer="51"/>
<rectangle x1="-5.334" y1="3.8608" x2="-4.826" y2="5.334" layer="51"/>
<rectangle x1="-5.334" y1="3.7338" x2="-4.826" y2="3.8608" layer="21"/>
<rectangle x1="-4.064" y1="3.7338" x2="-3.556" y2="3.8608" layer="21"/>
<rectangle x1="-4.064" y1="3.8608" x2="-3.556" y2="5.334" layer="51"/>
<rectangle x1="-2.794" y1="3.7338" x2="-2.286" y2="3.8608" layer="21"/>
<rectangle x1="-2.794" y1="3.8608" x2="-2.286" y2="5.334" layer="51"/>
<rectangle x1="-1.524" y1="3.7338" x2="-1.016" y2="3.8608" layer="21"/>
<rectangle x1="-1.524" y1="3.8608" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="-0.254" y1="3.7338" x2="0.254" y2="3.8608" layer="21"/>
<rectangle x1="-0.254" y1="3.8608" x2="0.254" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="3.7338" x2="1.524" y2="3.8608" layer="21"/>
<rectangle x1="1.016" y1="3.8608" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="2.286" y1="3.7338" x2="2.794" y2="3.8608" layer="21"/>
<rectangle x1="2.286" y1="3.8608" x2="2.794" y2="5.334" layer="51"/>
<rectangle x1="3.556" y1="3.7338" x2="4.064" y2="3.8608" layer="21"/>
<rectangle x1="3.556" y1="3.8608" x2="4.064" y2="5.334" layer="51"/>
<rectangle x1="4.826" y1="3.7338" x2="5.334" y2="3.8608" layer="21"/>
<rectangle x1="4.826" y1="3.8608" x2="5.334" y2="5.334" layer="51"/>
<rectangle x1="4.826" y1="-3.8608" x2="5.334" y2="-3.7338" layer="21"/>
<rectangle x1="4.826" y1="-5.334" x2="5.334" y2="-3.8608" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="DIL18" urn="urn:adsk.eagle:package:30237/1" type="box" library_version="1">
<description>Dual In Line Package</description>
<packageinstances>
<packageinstance name="DIL18"/>
</packageinstances>
</package3d>
<package3d name="SO20W" urn="urn:adsk.eagle:package:30246/1" type="box" library_version="1">
<description>Small Outline Package wide</description>
<packageinstances>
<packageinstance name="SO20W"/>
</packageinstances>
</package3d>
<package3d name="SO18W" urn="urn:adsk.eagle:package:30238/1" type="box" library_version="1">
<description>Small Outline Package wide</description>
<packageinstances>
<packageinstance name="SO18W"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="UDN2981A" urn="urn:adsk.eagle:symbol:30233/1" library_version="1">
<wire x1="-7.62" y1="12.7" x2="7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I1" x="-12.7" y="10.16" length="middle" direction="in"/>
<pin name="I2" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="I3" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="I4" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="I5" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="I6" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="I7" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="I8" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="O8" x="12.7" y="-7.62" length="middle" direction="oc" rot="R180"/>
<pin name="O1" x="12.7" y="10.16" length="middle" direction="oc" rot="R180"/>
<pin name="O2" x="12.7" y="7.62" length="middle" direction="oc" rot="R180"/>
<pin name="O3" x="12.7" y="5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O4" x="12.7" y="2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O5" x="12.7" y="0" length="middle" direction="oc" rot="R180"/>
<pin name="O6" x="12.7" y="-2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O7" x="12.7" y="-5.08" length="middle" direction="oc" rot="R180"/>
<pin name="VS" x="-12.7" y="-10.16" length="middle" direction="pas"/>
<pin name="GND" x="12.7" y="-10.16" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UDN298*" urn="urn:adsk.eagle:component:30265/1" prefix="IC" library_version="1">
<description>&lt;b&gt;DRIVER ARRAY&lt;/b&gt;&lt;p&gt;
Source: Allegro Microsystems Inc. / UDN2981-UDN2984.pdf</description>
<gates>
<gate name="1" symbol="UDN2981A" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="DIL18">
<connects>
<connect gate="1" pin="GND" pad="10"/>
<connect gate="1" pin="I1" pad="1"/>
<connect gate="1" pin="I2" pad="2"/>
<connect gate="1" pin="I3" pad="3"/>
<connect gate="1" pin="I4" pad="4"/>
<connect gate="1" pin="I5" pad="5"/>
<connect gate="1" pin="I6" pad="6"/>
<connect gate="1" pin="I7" pad="7"/>
<connect gate="1" pin="I8" pad="8"/>
<connect gate="1" pin="O1" pad="18"/>
<connect gate="1" pin="O2" pad="17"/>
<connect gate="1" pin="O3" pad="16"/>
<connect gate="1" pin="O4" pad="15"/>
<connect gate="1" pin="O5" pad="14"/>
<connect gate="1" pin="O6" pad="13"/>
<connect gate="1" pin="O7" pad="12"/>
<connect gate="1" pin="O8" pad="11"/>
<connect gate="1" pin="VS" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30237/1"/>
</package3dinstances>
<technologies>
<technology name="1"/>
<technology name="2"/>
<technology name="3"/>
<technology name="4"/>
</technologies>
</device>
<device name="SLW" package="SO20W">
<connects>
<connect gate="1" pin="GND" pad="12"/>
<connect gate="1" pin="I1" pad="1"/>
<connect gate="1" pin="I2" pad="2"/>
<connect gate="1" pin="I3" pad="3"/>
<connect gate="1" pin="I4" pad="4"/>
<connect gate="1" pin="I5" pad="5"/>
<connect gate="1" pin="I6" pad="6"/>
<connect gate="1" pin="I7" pad="7"/>
<connect gate="1" pin="I8" pad="8"/>
<connect gate="1" pin="O1" pad="20"/>
<connect gate="1" pin="O2" pad="19"/>
<connect gate="1" pin="O3" pad="18"/>
<connect gate="1" pin="O4" pad="17"/>
<connect gate="1" pin="O5" pad="16"/>
<connect gate="1" pin="O6" pad="15"/>
<connect gate="1" pin="O7" pad="14"/>
<connect gate="1" pin="O8" pad="13"/>
<connect gate="1" pin="VS" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30246/1"/>
</package3dinstances>
<technologies>
<technology name="1"/>
<technology name="2"/>
<technology name="3"/>
<technology name="4"/>
</technologies>
</device>
<device name="LW" package="SO18W">
<connects>
<connect gate="1" pin="GND" pad="10"/>
<connect gate="1" pin="I1" pad="1"/>
<connect gate="1" pin="I2" pad="2"/>
<connect gate="1" pin="I3" pad="3"/>
<connect gate="1" pin="I4" pad="4"/>
<connect gate="1" pin="I5" pad="5"/>
<connect gate="1" pin="I6" pad="6"/>
<connect gate="1" pin="I7" pad="7"/>
<connect gate="1" pin="I8" pad="8"/>
<connect gate="1" pin="O1" pad="18"/>
<connect gate="1" pin="O2" pad="17"/>
<connect gate="1" pin="O3" pad="16"/>
<connect gate="1" pin="O4" pad="15"/>
<connect gate="1" pin="O5" pad="14"/>
<connect gate="1" pin="O6" pad="13"/>
<connect gate="1" pin="O7" pad="12"/>
<connect gate="1" pin="O8" pad="11"/>
<connect gate="1" pin="VS" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30238/1"/>
</package3dinstances>
<technologies>
<technology name="1"/>
<technology name="2"/>
<technology name="3"/>
<technology name="4"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D1" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D2" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D3" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D4" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D5" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D6" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D7" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D8" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D9" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D10" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D11" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D12" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D13" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D14" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D15" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D16" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D17" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D18" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D19" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D20" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D21" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D22" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D23" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D24" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D25" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D26" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D27" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D28" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D29" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D30" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D31" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D32" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D33" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D34" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D35" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="R1" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R2" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R3" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R4" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R5" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R6" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R7" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="J2" library="SparkFun-Connectors" deviceset="CONN_12" device=""/>
<part name="D36" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D37" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D38" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D39" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D40" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D41" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D42" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D43" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D44" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D45" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D46" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D47" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D48" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D49" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D50" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D51" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D52" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D53" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D54" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D55" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D56" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D57" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D58" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D59" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D60" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D61" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D62" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D63" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D64" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D65" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D66" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D67" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D68" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D69" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D70" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="R8" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R9" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R10" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R11" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R12" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R13" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R14" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="J1" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R15" library="SparkFun-Resistors" deviceset="4.7KOHM" device="-0603-1/10W-1%" value="4.7k"/>
<part name="R16" library="SparkFun-Resistors" deviceset="4.7KOHM" device="-0603-1/10W-1%" value="4.7k"/>
<part name="0X72" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="0X70" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="D71" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D72" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D73" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D74" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D75" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D76" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D77" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D78" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D79" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D80" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D81" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D82" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D83" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D84" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D85" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D86" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D87" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D88" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D89" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D90" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D91" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D92" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D93" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D94" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D95" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D96" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D97" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D98" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D99" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D100" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D101" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D102" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D103" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D104" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="D105" library="SparkFun-LED" deviceset="LED" device="3MM"/>
<part name="R17" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R18" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R19" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R20" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R21" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R22" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="R23" library="SparkFun-Resistors" deviceset="100OHM" device="-0603-1/4W-5%" value="100"/>
<part name="J3" library="SparkFun-Connectors" deviceset="CONN_04" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R24" library="SparkFun-Resistors" deviceset="4.7KOHM" device="-0603-1/10W-1%" value="4.7k"/>
<part name="R25" library="SparkFun-Resistors" deviceset="4.7KOHM" device="-0603-1/10W-1%" value="4.7k"/>
<part name="0X1" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="0X2" library="i2c" deviceset="PCF8574" device="AP"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="IC1" library="uln-udn" library_urn="urn:adsk.eagle:library:407" deviceset="UDN298*" device="A" package3d_urn="urn:adsk.eagle:package:30237/1" technology="1"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="10.16" y="83.82" size="1.778" layer="97">C1</text>
<text x="20.32" y="83.82" size="1.778" layer="97">C2</text>
<text x="30.48" y="83.82" size="1.778" layer="97">C3</text>
<text x="40.64" y="83.82" size="1.778" layer="97">C4</text>
<text x="50.8" y="83.82" size="1.778" layer="97">C5</text>
<text x="-2.54" y="76.2" size="1.778" layer="97">R1</text>
<text x="-2.54" y="63.5" size="1.778" layer="97">R2</text>
<text x="-2.54" y="50.8" size="1.778" layer="97">R3</text>
<text x="-2.54" y="38.1" size="1.778" layer="97">R4</text>
<text x="-2.54" y="25.4" size="1.778" layer="97">R5</text>
<text x="-2.54" y="12.7" size="1.778" layer="97">R6</text>
<text x="-2.54" y="0" size="1.778" layer="97">R7</text>
<text x="-58.42" y="30.48" size="1.778" layer="97">STM32-F411
GPIO</text>
<text x="10.16" y="-27.94" size="1.778" layer="97">C1</text>
<text x="20.32" y="-27.94" size="1.778" layer="97">C2</text>
<text x="30.48" y="-27.94" size="1.778" layer="97">C3</text>
<text x="40.64" y="-27.94" size="1.778" layer="97">C4</text>
<text x="50.8" y="-27.94" size="1.778" layer="97">C5</text>
<text x="-2.54" y="-35.56" size="1.778" layer="97">R1</text>
<text x="-2.54" y="-48.26" size="1.778" layer="97">R2</text>
<text x="-2.54" y="-60.96" size="1.778" layer="97">R3</text>
<text x="-2.54" y="-73.66" size="1.778" layer="97">R4</text>
<text x="-2.54" y="-86.36" size="1.778" layer="97">R5</text>
<text x="-2.54" y="-99.06" size="1.778" layer="97">R6</text>
<text x="-2.54" y="-111.76" size="1.778" layer="97">R7</text>
<text x="-91.44" y="-109.22" size="1.778" layer="97" rot="R180">STM32-F411</text>
<text x="-104.14" y="-105.41" size="1.778" layer="97">VCC
SDA
SCL
GND</text>
<text x="-55.88" y="-81.28" size="1.778" layer="97" rot="R90">PCF Power</text>
<text x="195.58" y="-30.48" size="1.778" layer="97">C1</text>
<text x="205.74" y="-30.48" size="1.778" layer="97">C2</text>
<text x="215.9" y="-30.48" size="1.778" layer="97">C3</text>
<text x="226.06" y="-30.48" size="1.778" layer="97">C4</text>
<text x="236.22" y="-30.48" size="1.778" layer="97">C5</text>
<text x="182.88" y="-38.1" size="1.778" layer="97">R1</text>
<text x="182.88" y="-50.8" size="1.778" layer="97">R2</text>
<text x="182.88" y="-63.5" size="1.778" layer="97">R3</text>
<text x="182.88" y="-76.2" size="1.778" layer="97">R4</text>
<text x="182.88" y="-88.9" size="1.778" layer="97">R5</text>
<text x="182.88" y="-101.6" size="1.778" layer="97">R6</text>
<text x="182.88" y="-114.3" size="1.778" layer="97">R7</text>
<text x="91.44" y="-111.76" size="1.778" layer="97" rot="R180">STM32-F411</text>
<text x="78.74" y="-107.95" size="1.778" layer="97">VCC
SDA
SCL
GND</text>
<text x="129.54" y="-86.36" size="1.778" layer="97" rot="R90">PCF Power</text>
</plain>
<instances>
<instance part="D1" gate="D1" x="10.16" y="78.74" smashed="yes">
<attribute name="NAME" x="6.731" y="74.168" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="74.168" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D2" gate="D1" x="10.16" y="66.04" smashed="yes">
<attribute name="NAME" x="6.731" y="61.468" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="61.468" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D3" gate="D1" x="10.16" y="53.34" smashed="yes">
<attribute name="NAME" x="6.731" y="48.768" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="48.768" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D4" gate="D1" x="10.16" y="40.64" smashed="yes">
<attribute name="NAME" x="6.731" y="36.068" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="36.068" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D5" gate="D1" x="10.16" y="27.94" smashed="yes">
<attribute name="NAME" x="6.731" y="23.368" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="23.368" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D6" gate="D1" x="10.16" y="15.24" smashed="yes">
<attribute name="NAME" x="6.731" y="10.668" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="10.668" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D7" gate="D1" x="10.16" y="2.54" smashed="yes">
<attribute name="NAME" x="6.731" y="-2.032" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="12.065" y="-2.032" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D8" gate="D1" x="20.32" y="78.74" smashed="yes">
<attribute name="NAME" x="16.891" y="74.168" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="74.168" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D9" gate="D1" x="20.32" y="66.04" smashed="yes">
<attribute name="NAME" x="16.891" y="61.468" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="61.468" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D10" gate="D1" x="20.32" y="53.34" smashed="yes">
<attribute name="NAME" x="16.891" y="48.768" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="48.768" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D11" gate="D1" x="20.32" y="40.64" smashed="yes">
<attribute name="NAME" x="16.891" y="36.068" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="36.068" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D12" gate="D1" x="20.32" y="27.94" smashed="yes">
<attribute name="NAME" x="16.891" y="23.368" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="23.368" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D13" gate="D1" x="20.32" y="15.24" smashed="yes">
<attribute name="NAME" x="16.891" y="10.668" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="10.668" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D14" gate="D1" x="20.32" y="2.54" smashed="yes">
<attribute name="NAME" x="16.891" y="-2.032" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="22.225" y="-2.032" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D15" gate="D1" x="30.48" y="78.74" smashed="yes">
<attribute name="NAME" x="27.051" y="74.168" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="74.168" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D16" gate="D1" x="30.48" y="66.04" smashed="yes">
<attribute name="NAME" x="27.051" y="61.468" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="61.468" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D17" gate="D1" x="30.48" y="53.34" smashed="yes">
<attribute name="NAME" x="27.051" y="48.768" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="48.768" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D18" gate="D1" x="30.48" y="40.64" smashed="yes">
<attribute name="NAME" x="27.051" y="36.068" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="36.068" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D19" gate="D1" x="30.48" y="27.94" smashed="yes">
<attribute name="NAME" x="27.051" y="23.368" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="23.368" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D20" gate="D1" x="30.48" y="15.24" smashed="yes">
<attribute name="NAME" x="27.051" y="10.668" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="10.668" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D21" gate="D1" x="30.48" y="2.54" smashed="yes">
<attribute name="NAME" x="27.051" y="-2.032" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="32.385" y="-2.032" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D22" gate="D1" x="40.64" y="78.74" smashed="yes">
<attribute name="NAME" x="37.211" y="74.168" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="74.168" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D23" gate="D1" x="40.64" y="66.04" smashed="yes">
<attribute name="NAME" x="37.211" y="61.468" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="61.468" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D24" gate="D1" x="40.64" y="53.34" smashed="yes">
<attribute name="NAME" x="37.211" y="48.768" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="48.768" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D25" gate="D1" x="40.64" y="40.64" smashed="yes">
<attribute name="NAME" x="37.211" y="36.068" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="36.068" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D26" gate="D1" x="40.64" y="27.94" smashed="yes">
<attribute name="NAME" x="37.211" y="23.368" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="23.368" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D27" gate="D1" x="40.64" y="15.24" smashed="yes">
<attribute name="NAME" x="37.211" y="10.668" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="10.668" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D28" gate="D1" x="40.64" y="2.54" smashed="yes">
<attribute name="NAME" x="37.211" y="-2.032" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="42.545" y="-2.032" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D29" gate="D1" x="50.8" y="78.74" smashed="yes">
<attribute name="NAME" x="47.371" y="74.168" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="74.168" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D30" gate="D1" x="50.8" y="66.04" smashed="yes">
<attribute name="NAME" x="47.371" y="61.468" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="61.468" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D31" gate="D1" x="50.8" y="53.34" smashed="yes">
<attribute name="NAME" x="47.371" y="48.768" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="48.768" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D32" gate="D1" x="50.8" y="40.64" smashed="yes">
<attribute name="NAME" x="47.371" y="36.068" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="36.068" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D33" gate="D1" x="50.8" y="27.94" smashed="yes">
<attribute name="NAME" x="47.371" y="23.368" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="23.368" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D34" gate="D1" x="50.8" y="15.24" smashed="yes">
<attribute name="NAME" x="47.371" y="10.668" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="10.668" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D35" gate="D1" x="50.8" y="2.54" smashed="yes">
<attribute name="NAME" x="47.371" y="-2.032" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="52.705" y="-2.032" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="R1" gate="G$1" x="-7.62" y="73.66" smashed="yes">
<attribute name="NAME" x="-7.62" y="75.184" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="72.136" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R2" gate="G$1" x="-7.62" y="60.96" smashed="yes">
<attribute name="NAME" x="-7.62" y="62.484" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="59.436" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="-7.62" y="48.26" smashed="yes">
<attribute name="NAME" x="-7.62" y="49.784" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="46.736" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="-7.62" y="35.56" smashed="yes">
<attribute name="NAME" x="-7.62" y="37.084" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="34.036" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R5" gate="G$1" x="-7.62" y="22.86" smashed="yes">
<attribute name="NAME" x="-7.62" y="24.384" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="21.336" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="-7.62" y="10.16" smashed="yes">
<attribute name="NAME" x="-7.62" y="11.684" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="8.636" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R7" gate="G$1" x="-7.62" y="-2.54" smashed="yes">
<attribute name="NAME" x="-7.62" y="-1.016" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="-7.62" y="-4.064" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="J2" gate="G$1" x="-60.96" y="12.7" smashed="yes">
<attribute name="VALUE" x="-60.96" y="-7.366" size="1.778" layer="96" font="vector"/>
<attribute name="NAME" x="-60.96" y="28.448" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="D36" gate="D1" x="10.16" y="-33.02" smashed="yes">
<attribute name="VALUE" x="12.065" y="-37.592" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D37" gate="D1" x="10.16" y="-45.72" smashed="yes">
<attribute name="VALUE" x="12.065" y="-50.292" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D38" gate="D1" x="10.16" y="-58.42" smashed="yes">
<attribute name="VALUE" x="12.065" y="-62.992" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D39" gate="D1" x="10.16" y="-71.12" smashed="yes">
<attribute name="VALUE" x="12.065" y="-75.692" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D40" gate="D1" x="10.16" y="-83.82" smashed="yes">
<attribute name="VALUE" x="12.065" y="-88.392" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D41" gate="D1" x="10.16" y="-96.52" smashed="yes">
<attribute name="VALUE" x="12.065" y="-101.092" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D42" gate="D1" x="10.16" y="-109.22" smashed="yes">
<attribute name="VALUE" x="12.065" y="-113.792" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D43" gate="D1" x="20.32" y="-33.02" smashed="yes">
<attribute name="VALUE" x="22.225" y="-37.592" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D44" gate="D1" x="20.32" y="-45.72" smashed="yes">
<attribute name="VALUE" x="22.225" y="-50.292" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D45" gate="D1" x="20.32" y="-58.42" smashed="yes">
<attribute name="VALUE" x="22.225" y="-62.992" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D46" gate="D1" x="20.32" y="-71.12" smashed="yes">
<attribute name="VALUE" x="22.225" y="-75.692" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D47" gate="D1" x="20.32" y="-83.82" smashed="yes">
<attribute name="VALUE" x="22.225" y="-88.392" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D48" gate="D1" x="20.32" y="-96.52" smashed="yes">
<attribute name="VALUE" x="22.225" y="-101.092" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D49" gate="D1" x="20.32" y="-109.22" smashed="yes">
<attribute name="VALUE" x="22.225" y="-113.792" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D50" gate="D1" x="30.48" y="-33.02" smashed="yes">
<attribute name="VALUE" x="32.385" y="-37.592" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D51" gate="D1" x="30.48" y="-45.72" smashed="yes">
<attribute name="VALUE" x="32.385" y="-50.292" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D52" gate="D1" x="30.48" y="-58.42" smashed="yes">
<attribute name="VALUE" x="32.385" y="-62.992" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D53" gate="D1" x="30.48" y="-71.12" smashed="yes">
<attribute name="VALUE" x="32.385" y="-75.692" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D54" gate="D1" x="30.48" y="-83.82" smashed="yes">
<attribute name="VALUE" x="32.385" y="-88.392" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D55" gate="D1" x="30.48" y="-96.52" smashed="yes">
<attribute name="VALUE" x="32.385" y="-101.092" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D56" gate="D1" x="30.48" y="-109.22" smashed="yes">
<attribute name="VALUE" x="32.385" y="-113.792" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D57" gate="D1" x="40.64" y="-33.02" smashed="yes">
<attribute name="VALUE" x="42.545" y="-37.592" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D58" gate="D1" x="40.64" y="-45.72" smashed="yes">
<attribute name="VALUE" x="42.545" y="-50.292" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D59" gate="D1" x="40.64" y="-58.42" smashed="yes">
<attribute name="VALUE" x="42.545" y="-62.992" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D60" gate="D1" x="40.64" y="-71.12" smashed="yes">
<attribute name="VALUE" x="42.545" y="-75.692" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D61" gate="D1" x="40.64" y="-83.82" smashed="yes">
<attribute name="VALUE" x="42.545" y="-88.392" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D62" gate="D1" x="40.64" y="-96.52" smashed="yes">
<attribute name="VALUE" x="42.545" y="-101.092" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D63" gate="D1" x="40.64" y="-109.22" smashed="yes">
<attribute name="VALUE" x="42.545" y="-113.792" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D64" gate="D1" x="50.8" y="-33.02" smashed="yes">
<attribute name="VALUE" x="52.705" y="-37.592" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D65" gate="D1" x="50.8" y="-45.72" smashed="yes">
<attribute name="VALUE" x="52.705" y="-50.292" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D66" gate="D1" x="50.8" y="-58.42" smashed="yes">
<attribute name="VALUE" x="52.705" y="-62.992" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D67" gate="D1" x="50.8" y="-71.12" smashed="yes">
<attribute name="VALUE" x="52.705" y="-75.692" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D68" gate="D1" x="50.8" y="-83.82" smashed="yes">
<attribute name="VALUE" x="52.705" y="-88.392" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D69" gate="D1" x="50.8" y="-96.52" smashed="yes">
<attribute name="VALUE" x="52.705" y="-101.092" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D70" gate="D1" x="50.8" y="-109.22" smashed="yes">
<attribute name="VALUE" x="52.705" y="-113.792" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="R8" gate="G$1" x="-7.62" y="-38.1" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-39.624" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R9" gate="G$1" x="-7.62" y="-50.8" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-52.324" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R10" gate="G$1" x="-7.62" y="-63.5" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-65.024" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R11" gate="G$1" x="-7.62" y="-76.2" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-77.724" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R12" gate="G$1" x="-7.62" y="-88.9" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-90.424" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R13" gate="G$1" x="-7.62" y="-101.6" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-103.124" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R14" gate="G$1" x="-7.62" y="-114.3" smashed="yes">
<attribute name="VALUE" x="-7.62" y="-115.824" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="J1" gate="G$1" x="-91.44" y="-101.6" smashed="yes">
<attribute name="VALUE" x="-96.52" y="-108.966" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND1" gate="1" x="-86.36" y="-111.76" smashed="yes">
<attribute name="VALUE" x="-88.9" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="VCC" x="-86.36" y="-81.28" smashed="yes">
<attribute name="VALUE" x="-88.9" y="-83.82" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R15" gate="G$1" x="-76.2" y="-91.44" smashed="yes" rot="R90">
<attribute name="VALUE" x="-74.676" y="-91.44" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R16" gate="G$1" x="-81.28" y="-91.44" smashed="yes" rot="R90">
<attribute name="VALUE" x="-79.756" y="-91.44" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="0X72" gate="IC$1" x="-45.72" y="-27.94" smashed="yes">
<attribute name="NAME" x="-48.26" y="-12.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-50.8" y="-40.64" size="1.778" layer="96"/>
</instance>
<instance part="0X72" gate="PWR" x="-43.18" y="-73.66" smashed="yes"/>
<instance part="0X70" gate="IC$1" x="-45.72" y="-109.22" smashed="yes">
<attribute name="NAME" x="-48.26" y="-93.98" size="1.778" layer="95"/>
<attribute name="VALUE" x="-50.8" y="-121.92" size="1.778" layer="96"/>
</instance>
<instance part="0X70" gate="PWR" x="-50.8" y="-73.66" smashed="yes"/>
<instance part="P+2" gate="VCC" x="-50.8" y="-63.5" smashed="yes">
<attribute name="VALUE" x="-53.34" y="-66.04" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="-50.8" y="-83.82" smashed="yes">
<attribute name="VALUE" x="-53.34" y="-86.36" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="-63.5" y="-30.48" smashed="yes">
<attribute name="VALUE" x="-66.04" y="-33.02" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="-63.5" y="-111.76" smashed="yes">
<attribute name="VALUE" x="-66.04" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="VCC" x="-66.04" y="-22.86" smashed="yes">
<attribute name="VALUE" x="-68.58" y="-25.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D71" gate="D1" x="195.58" y="-35.56" smashed="yes">
<attribute name="VALUE" x="197.485" y="-40.132" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D72" gate="D1" x="195.58" y="-48.26" smashed="yes">
<attribute name="VALUE" x="197.485" y="-52.832" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D73" gate="D1" x="195.58" y="-60.96" smashed="yes">
<attribute name="VALUE" x="197.485" y="-65.532" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D74" gate="D1" x="195.58" y="-73.66" smashed="yes">
<attribute name="VALUE" x="197.485" y="-78.232" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D75" gate="D1" x="195.58" y="-86.36" smashed="yes">
<attribute name="VALUE" x="197.485" y="-90.932" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D76" gate="D1" x="195.58" y="-99.06" smashed="yes">
<attribute name="VALUE" x="197.485" y="-103.632" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D77" gate="D1" x="195.58" y="-111.76" smashed="yes">
<attribute name="VALUE" x="197.485" y="-116.332" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D78" gate="D1" x="205.74" y="-35.56" smashed="yes">
<attribute name="VALUE" x="207.645" y="-40.132" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D79" gate="D1" x="205.74" y="-48.26" smashed="yes">
<attribute name="VALUE" x="207.645" y="-52.832" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D80" gate="D1" x="205.74" y="-60.96" smashed="yes">
<attribute name="VALUE" x="207.645" y="-65.532" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D81" gate="D1" x="205.74" y="-73.66" smashed="yes">
<attribute name="VALUE" x="207.645" y="-78.232" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D82" gate="D1" x="205.74" y="-86.36" smashed="yes">
<attribute name="VALUE" x="207.645" y="-90.932" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D83" gate="D1" x="205.74" y="-99.06" smashed="yes">
<attribute name="VALUE" x="207.645" y="-103.632" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D84" gate="D1" x="205.74" y="-111.76" smashed="yes">
<attribute name="VALUE" x="207.645" y="-116.332" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D85" gate="D1" x="215.9" y="-35.56" smashed="yes">
<attribute name="VALUE" x="217.805" y="-40.132" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D86" gate="D1" x="215.9" y="-48.26" smashed="yes">
<attribute name="VALUE" x="217.805" y="-52.832" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D87" gate="D1" x="215.9" y="-60.96" smashed="yes">
<attribute name="VALUE" x="217.805" y="-65.532" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D88" gate="D1" x="215.9" y="-73.66" smashed="yes">
<attribute name="VALUE" x="217.805" y="-78.232" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D89" gate="D1" x="215.9" y="-86.36" smashed="yes">
<attribute name="VALUE" x="217.805" y="-90.932" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D90" gate="D1" x="215.9" y="-99.06" smashed="yes">
<attribute name="VALUE" x="217.805" y="-103.632" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D91" gate="D1" x="215.9" y="-111.76" smashed="yes">
<attribute name="VALUE" x="217.805" y="-116.332" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D92" gate="D1" x="226.06" y="-35.56" smashed="yes">
<attribute name="VALUE" x="227.965" y="-40.132" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D93" gate="D1" x="226.06" y="-48.26" smashed="yes">
<attribute name="VALUE" x="227.965" y="-52.832" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D94" gate="D1" x="226.06" y="-60.96" smashed="yes">
<attribute name="VALUE" x="227.965" y="-65.532" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D95" gate="D1" x="226.06" y="-73.66" smashed="yes">
<attribute name="VALUE" x="227.965" y="-78.232" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D96" gate="D1" x="226.06" y="-86.36" smashed="yes">
<attribute name="VALUE" x="227.965" y="-90.932" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D97" gate="D1" x="226.06" y="-99.06" smashed="yes">
<attribute name="VALUE" x="227.965" y="-103.632" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D98" gate="D1" x="226.06" y="-111.76" smashed="yes">
<attribute name="VALUE" x="227.965" y="-116.332" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D99" gate="D1" x="236.22" y="-35.56" smashed="yes">
<attribute name="VALUE" x="238.125" y="-40.132" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D100" gate="D1" x="236.22" y="-48.26" smashed="yes">
<attribute name="VALUE" x="238.125" y="-52.832" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D101" gate="D1" x="236.22" y="-60.96" smashed="yes">
<attribute name="VALUE" x="238.125" y="-65.532" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D102" gate="D1" x="236.22" y="-73.66" smashed="yes">
<attribute name="VALUE" x="238.125" y="-78.232" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D103" gate="D1" x="236.22" y="-86.36" smashed="yes">
<attribute name="VALUE" x="238.125" y="-90.932" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D104" gate="D1" x="236.22" y="-99.06" smashed="yes">
<attribute name="VALUE" x="238.125" y="-103.632" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="D105" gate="D1" x="236.22" y="-111.76" smashed="yes">
<attribute name="VALUE" x="238.125" y="-116.332" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
</instance>
<instance part="R17" gate="G$1" x="177.8" y="-40.64" smashed="yes">
<attribute name="VALUE" x="177.8" y="-42.164" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R18" gate="G$1" x="177.8" y="-53.34" smashed="yes">
<attribute name="VALUE" x="177.8" y="-54.864" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R19" gate="G$1" x="177.8" y="-66.04" smashed="yes">
<attribute name="VALUE" x="177.8" y="-67.564" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R20" gate="G$1" x="177.8" y="-78.74" smashed="yes">
<attribute name="VALUE" x="177.8" y="-80.264" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R21" gate="G$1" x="177.8" y="-91.44" smashed="yes">
<attribute name="VALUE" x="177.8" y="-92.964" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R22" gate="G$1" x="177.8" y="-104.14" smashed="yes">
<attribute name="VALUE" x="177.8" y="-105.664" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R23" gate="G$1" x="177.8" y="-116.84" smashed="yes">
<attribute name="VALUE" x="177.8" y="-118.364" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="J3" gate="G$1" x="91.44" y="-104.14" smashed="yes">
<attribute name="VALUE" x="86.36" y="-111.506" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND5" gate="1" x="96.52" y="-114.3" smashed="yes">
<attribute name="VALUE" x="93.98" y="-116.84" size="1.778" layer="96"/>
</instance>
<instance part="P+4" gate="VCC" x="96.52" y="-83.82" smashed="yes">
<attribute name="VALUE" x="93.98" y="-86.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R24" gate="G$1" x="106.68" y="-93.98" smashed="yes" rot="R90">
<attribute name="VALUE" x="108.204" y="-93.98" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R25" gate="G$1" x="101.6" y="-93.98" smashed="yes" rot="R90">
<attribute name="VALUE" x="103.124" y="-93.98" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="0X1" gate="IC$1" x="124.46" y="-30.48" smashed="yes">
<attribute name="NAME" x="121.92" y="-15.24" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="-43.18" size="1.778" layer="96"/>
</instance>
<instance part="0X1" gate="PWR" x="142.24" y="-78.74" smashed="yes"/>
<instance part="0X2" gate="IC$1" x="139.7" y="-111.76" smashed="yes">
<attribute name="NAME" x="137.16" y="-96.52" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.62" y="-124.46" size="1.778" layer="96"/>
</instance>
<instance part="0X2" gate="PWR" x="134.62" y="-78.74" smashed="yes"/>
<instance part="P+5" gate="VCC" x="134.62" y="-68.58" smashed="yes">
<attribute name="VALUE" x="132.08" y="-71.12" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="134.62" y="-88.9" smashed="yes">
<attribute name="VALUE" x="132.08" y="-91.44" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="106.68" y="-33.02" smashed="yes">
<attribute name="VALUE" x="104.14" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="121.92" y="-114.3" smashed="yes">
<attribute name="VALUE" x="119.38" y="-116.84" size="1.778" layer="96"/>
</instance>
<instance part="P+6" gate="VCC" x="104.14" y="-25.4" smashed="yes">
<attribute name="VALUE" x="101.6" y="-27.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC1" gate="1" x="149.86" y="-30.48" smashed="yes">
<attribute name="NAME" x="149.86" y="-14.478" size="1.778" layer="95"/>
<attribute name="VALUE" x="142.24" y="-45.72" size="1.778" layer="96"/>
</instance>
<instance part="P+7" gate="VCC" x="137.16" y="-45.72" smashed="yes" rot="R180">
<attribute name="VALUE" x="134.62" y="-48.26" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="162.56" y="-45.72" smashed="yes">
<attribute name="VALUE" x="160.02" y="-48.26" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="D1" gate="D1" pin="A"/>
<wire x1="10.16" y1="81.28" x2="12.7" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D7" gate="D1" pin="A"/>
<wire x1="12.7" y1="81.28" x2="12.7" y2="68.58" width="0.1524" layer="91"/>
<wire x1="12.7" y1="68.58" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="12.7" y1="55.88" x2="12.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="12.7" y1="43.18" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<wire x1="12.7" y1="30.48" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="5.08" width="0.1524" layer="91"/>
<wire x1="12.7" y1="5.08" x2="10.16" y2="5.08" width="0.1524" layer="91"/>
<pinref part="D6" gate="D1" pin="A"/>
<wire x1="10.16" y1="17.78" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
<junction x="12.7" y="17.78"/>
<pinref part="D5" gate="D1" pin="A"/>
<wire x1="10.16" y1="30.48" x2="12.7" y2="30.48" width="0.1524" layer="91"/>
<junction x="12.7" y="30.48"/>
<pinref part="D4" gate="D1" pin="A"/>
<wire x1="10.16" y1="43.18" x2="12.7" y2="43.18" width="0.1524" layer="91"/>
<junction x="12.7" y="43.18"/>
<pinref part="D3" gate="D1" pin="A"/>
<wire x1="10.16" y1="55.88" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
<junction x="12.7" y="55.88"/>
<pinref part="D2" gate="D1" pin="A"/>
<wire x1="10.16" y1="68.58" x2="12.7" y2="68.58" width="0.1524" layer="91"/>
<junction x="12.7" y="68.58"/>
<wire x1="12.7" y1="81.28" x2="12.7" y2="83.82" width="0.1524" layer="91"/>
<junction x="12.7" y="81.28"/>
<wire x1="-50.8" y1="15.24" x2="-27.94" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="15.24" x2="-27.94" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="83.82" x2="12.7" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D8" gate="D1" pin="A"/>
<wire x1="20.32" y1="81.28" x2="22.86" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D14" gate="D1" pin="A"/>
<wire x1="22.86" y1="81.28" x2="22.86" y2="68.58" width="0.1524" layer="91"/>
<wire x1="22.86" y1="68.58" x2="22.86" y2="55.88" width="0.1524" layer="91"/>
<wire x1="22.86" y1="55.88" x2="22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="22.86" y1="43.18" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="30.48" x2="22.86" y2="17.78" width="0.1524" layer="91"/>
<wire x1="22.86" y1="17.78" x2="22.86" y2="5.08" width="0.1524" layer="91"/>
<wire x1="22.86" y1="5.08" x2="20.32" y2="5.08" width="0.1524" layer="91"/>
<pinref part="D9" gate="D1" pin="A"/>
<wire x1="20.32" y1="68.58" x2="22.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="22.86" y="68.58"/>
<pinref part="D10" gate="D1" pin="A"/>
<wire x1="20.32" y1="55.88" x2="22.86" y2="55.88" width="0.1524" layer="91"/>
<junction x="22.86" y="55.88"/>
<pinref part="D11" gate="D1" pin="A"/>
<wire x1="20.32" y1="43.18" x2="22.86" y2="43.18" width="0.1524" layer="91"/>
<junction x="22.86" y="43.18"/>
<pinref part="D12" gate="D1" pin="A"/>
<wire x1="20.32" y1="30.48" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
<junction x="22.86" y="30.48"/>
<pinref part="D13" gate="D1" pin="A"/>
<wire x1="20.32" y1="17.78" x2="22.86" y2="17.78" width="0.1524" layer="91"/>
<junction x="22.86" y="17.78"/>
<junction x="22.86" y="81.28"/>
<wire x1="-50.8" y1="17.78" x2="-30.48" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="17.78" x2="-30.48" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="86.36" x2="22.86" y2="86.36" width="0.1524" layer="91"/>
<wire x1="22.86" y1="86.36" x2="22.86" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="9"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="D15" gate="D1" pin="A"/>
<wire x1="30.48" y1="81.28" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D21" gate="D1" pin="A"/>
<wire x1="33.02" y1="81.28" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="68.58" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<wire x1="33.02" y1="55.88" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<wire x1="33.02" y1="43.18" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<wire x1="33.02" y1="30.48" x2="33.02" y2="17.78" width="0.1524" layer="91"/>
<wire x1="33.02" y1="17.78" x2="33.02" y2="5.08" width="0.1524" layer="91"/>
<wire x1="33.02" y1="5.08" x2="30.48" y2="5.08" width="0.1524" layer="91"/>
<pinref part="D20" gate="D1" pin="A"/>
<wire x1="30.48" y1="17.78" x2="33.02" y2="17.78" width="0.1524" layer="91"/>
<junction x="33.02" y="17.78"/>
<pinref part="D19" gate="D1" pin="A"/>
<wire x1="30.48" y1="30.48" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<junction x="33.02" y="30.48"/>
<pinref part="D18" gate="D1" pin="A"/>
<wire x1="30.48" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<junction x="33.02" y="43.18"/>
<pinref part="D17" gate="D1" pin="A"/>
<wire x1="30.48" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<junction x="33.02" y="55.88"/>
<pinref part="D16" gate="D1" pin="A"/>
<wire x1="30.48" y1="68.58" x2="33.02" y2="68.58" width="0.1524" layer="91"/>
<junction x="33.02" y="68.58"/>
<junction x="33.02" y="81.28"/>
<wire x1="-50.8" y1="20.32" x2="-33.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="20.32" x2="-33.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="88.9" x2="33.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="33.02" y1="88.9" x2="33.02" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="10"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D22" gate="D1" pin="A"/>
<wire x1="40.64" y1="81.28" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="43.18" y1="81.28" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
<pinref part="D28" gate="D1" pin="A"/>
<wire x1="43.18" y1="68.58" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="43.18" y1="55.88" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<wire x1="43.18" y1="43.18" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="30.48" x2="43.18" y2="17.78" width="0.1524" layer="91"/>
<wire x1="43.18" y1="17.78" x2="43.18" y2="5.08" width="0.1524" layer="91"/>
<wire x1="43.18" y1="5.08" x2="40.64" y2="5.08" width="0.1524" layer="91"/>
<pinref part="D23" gate="D1" pin="A"/>
<wire x1="40.64" y1="68.58" x2="43.18" y2="68.58" width="0.1524" layer="91"/>
<junction x="43.18" y="68.58"/>
<pinref part="D24" gate="D1" pin="A"/>
<wire x1="40.64" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<junction x="43.18" y="55.88"/>
<pinref part="D25" gate="D1" pin="A"/>
<wire x1="40.64" y1="43.18" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<junction x="43.18" y="43.18"/>
<pinref part="D26" gate="D1" pin="A"/>
<wire x1="40.64" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<junction x="43.18" y="30.48"/>
<pinref part="D27" gate="D1" pin="A"/>
<wire x1="40.64" y1="17.78" x2="43.18" y2="17.78" width="0.1524" layer="91"/>
<junction x="43.18" y="17.78"/>
<junction x="43.18" y="81.28"/>
<wire x1="-50.8" y1="22.86" x2="-35.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="22.86" x2="-35.56" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="43.18" y1="91.44" x2="43.18" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="11"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D29" gate="D1" pin="A"/>
<wire x1="50.8" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D35" gate="D1" pin="A"/>
<wire x1="53.34" y1="81.28" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="68.58" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="55.88" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="43.18" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<wire x1="53.34" y1="17.78" x2="53.34" y2="5.08" width="0.1524" layer="91"/>
<wire x1="53.34" y1="5.08" x2="50.8" y2="5.08" width="0.1524" layer="91"/>
<pinref part="D30" gate="D1" pin="A"/>
<wire x1="50.8" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<junction x="53.34" y="68.58"/>
<pinref part="D31" gate="D1" pin="A"/>
<wire x1="50.8" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="53.34" y="55.88"/>
<pinref part="D32" gate="D1" pin="A"/>
<wire x1="50.8" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<junction x="53.34" y="43.18"/>
<pinref part="D33" gate="D1" pin="A"/>
<wire x1="50.8" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<junction x="53.34" y="30.48"/>
<pinref part="D34" gate="D1" pin="A"/>
<wire x1="50.8" y1="17.78" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<junction x="53.34" y="17.78"/>
<junction x="53.34" y="81.28"/>
<wire x1="-50.8" y1="25.4" x2="-38.1" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="25.4" x2="-38.1" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="93.98" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="93.98" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="12"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D1" gate="D1" pin="C"/>
<pinref part="D8" gate="D1" pin="C"/>
<wire x1="10.16" y1="73.66" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<pinref part="D15" gate="D1" pin="C"/>
<wire x1="30.48" y1="73.66" x2="20.32" y2="73.66" width="0.1524" layer="91"/>
<junction x="20.32" y="73.66"/>
<pinref part="D22" gate="D1" pin="C"/>
<wire x1="30.48" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<junction x="30.48" y="73.66"/>
<pinref part="D29" gate="D1" pin="C"/>
<wire x1="40.64" y1="73.66" x2="50.8" y2="73.66" width="0.1524" layer="91"/>
<junction x="40.64" y="73.66"/>
<wire x1="10.16" y1="73.66" x2="-2.54" y2="73.66" width="0.1524" layer="91"/>
<junction x="10.16" y="73.66"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D2" gate="D1" pin="C"/>
<pinref part="D9" gate="D1" pin="C"/>
<wire x1="10.16" y1="60.96" x2="20.32" y2="60.96" width="0.1524" layer="91"/>
<pinref part="D16" gate="D1" pin="C"/>
<wire x1="20.32" y1="60.96" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<junction x="20.32" y="60.96"/>
<pinref part="D23" gate="D1" pin="C"/>
<wire x1="30.48" y1="60.96" x2="40.64" y2="60.96" width="0.1524" layer="91"/>
<junction x="30.48" y="60.96"/>
<pinref part="D30" gate="D1" pin="C"/>
<wire x1="40.64" y1="60.96" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<junction x="40.64" y="60.96"/>
<wire x1="10.16" y1="60.96" x2="-2.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="10.16" y="60.96"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D3" gate="D1" pin="C"/>
<pinref part="D10" gate="D1" pin="C"/>
<wire x1="10.16" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<pinref part="D17" gate="D1" pin="C"/>
<wire x1="20.32" y1="48.26" x2="30.48" y2="48.26" width="0.1524" layer="91"/>
<junction x="20.32" y="48.26"/>
<pinref part="D24" gate="D1" pin="C"/>
<wire x1="30.48" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
<junction x="30.48" y="48.26"/>
<pinref part="D31" gate="D1" pin="C"/>
<wire x1="40.64" y1="48.26" x2="50.8" y2="48.26" width="0.1524" layer="91"/>
<junction x="40.64" y="48.26"/>
<wire x1="10.16" y1="48.26" x2="-2.54" y2="48.26" width="0.1524" layer="91"/>
<junction x="10.16" y="48.26"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="D32" gate="D1" pin="C"/>
<pinref part="D25" gate="D1" pin="C"/>
<wire x1="50.8" y1="35.56" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<pinref part="D18" gate="D1" pin="C"/>
<wire x1="40.64" y1="35.56" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<junction x="40.64" y="35.56"/>
<pinref part="D11" gate="D1" pin="C"/>
<wire x1="30.48" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<junction x="30.48" y="35.56"/>
<pinref part="D4" gate="D1" pin="C"/>
<wire x1="20.32" y1="35.56" x2="10.16" y2="35.56" width="0.1524" layer="91"/>
<junction x="20.32" y="35.56"/>
<wire x1="10.16" y1="35.56" x2="-2.54" y2="35.56" width="0.1524" layer="91"/>
<junction x="10.16" y="35.56"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D5" gate="D1" pin="C"/>
<pinref part="D12" gate="D1" pin="C"/>
<wire x1="10.16" y1="22.86" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
<pinref part="D19" gate="D1" pin="C"/>
<wire x1="20.32" y1="22.86" x2="30.48" y2="22.86" width="0.1524" layer="91"/>
<junction x="20.32" y="22.86"/>
<pinref part="D26" gate="D1" pin="C"/>
<wire x1="30.48" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<junction x="30.48" y="22.86"/>
<pinref part="D33" gate="D1" pin="C"/>
<wire x1="40.64" y1="22.86" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
<junction x="40.64" y="22.86"/>
<wire x1="10.16" y1="22.86" x2="-2.54" y2="22.86" width="0.1524" layer="91"/>
<junction x="10.16" y="22.86"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="D6" gate="D1" pin="C"/>
<pinref part="D13" gate="D1" pin="C"/>
<wire x1="10.16" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="91"/>
<pinref part="D20" gate="D1" pin="C"/>
<wire x1="20.32" y1="10.16" x2="30.48" y2="10.16" width="0.1524" layer="91"/>
<junction x="20.32" y="10.16"/>
<pinref part="D27" gate="D1" pin="C"/>
<wire x1="30.48" y1="10.16" x2="40.64" y2="10.16" width="0.1524" layer="91"/>
<junction x="30.48" y="10.16"/>
<pinref part="D34" gate="D1" pin="C"/>
<wire x1="40.64" y1="10.16" x2="50.8" y2="10.16" width="0.1524" layer="91"/>
<junction x="40.64" y="10.16"/>
<wire x1="10.16" y1="10.16" x2="-2.54" y2="10.16" width="0.1524" layer="91"/>
<junction x="10.16" y="10.16"/>
<pinref part="R6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D7" gate="D1" pin="C"/>
<pinref part="D14" gate="D1" pin="C"/>
<wire x1="10.16" y1="-2.54" x2="20.32" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="D21" gate="D1" pin="C"/>
<wire x1="20.32" y1="-2.54" x2="30.48" y2="-2.54" width="0.1524" layer="91"/>
<junction x="20.32" y="-2.54"/>
<pinref part="D28" gate="D1" pin="C"/>
<wire x1="30.48" y1="-2.54" x2="40.64" y2="-2.54" width="0.1524" layer="91"/>
<junction x="30.48" y="-2.54"/>
<pinref part="D35" gate="D1" pin="C"/>
<wire x1="40.64" y1="-2.54" x2="50.8" y2="-2.54" width="0.1524" layer="91"/>
<junction x="40.64" y="-2.54"/>
<wire x1="10.16" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="91"/>
<junction x="10.16" y="-2.54"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-2.54" x2="-50.8" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="0" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="0" x2="-50.8" y2="0" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="-50.8" y1="2.54" x2="-15.24" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="22.86" x2="-12.7" y2="22.86" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="-50.8" y1="5.08" x2="-17.78" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="5.08" x2="-17.78" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="35.56" x2="-12.7" y2="35.56" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="-50.8" y1="7.62" x2="-20.32" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="7.62" x2="-20.32" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="48.26" x2="-12.7" y2="48.26" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="-50.8" y1="10.16" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="10.16" x2="-22.86" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="60.96" x2="-12.7" y2="60.96" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="-50.8" y1="12.7" x2="-25.4" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="12.7" x2="-25.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="73.66" x2="-12.7" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D36" gate="D1" pin="A"/>
<wire x1="10.16" y1="-30.48" x2="12.7" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="D42" gate="D1" pin="A"/>
<wire x1="12.7" y1="-30.48" x2="12.7" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-43.18" x2="12.7" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-55.88" x2="12.7" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-68.58" x2="12.7" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-81.28" x2="12.7" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-93.98" x2="12.7" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-106.68" x2="10.16" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="D41" gate="D1" pin="A"/>
<wire x1="10.16" y1="-93.98" x2="12.7" y2="-93.98" width="0.1524" layer="91"/>
<junction x="12.7" y="-93.98"/>
<pinref part="D40" gate="D1" pin="A"/>
<wire x1="10.16" y1="-81.28" x2="12.7" y2="-81.28" width="0.1524" layer="91"/>
<junction x="12.7" y="-81.28"/>
<pinref part="D39" gate="D1" pin="A"/>
<wire x1="10.16" y1="-68.58" x2="12.7" y2="-68.58" width="0.1524" layer="91"/>
<junction x="12.7" y="-68.58"/>
<pinref part="D38" gate="D1" pin="A"/>
<wire x1="10.16" y1="-55.88" x2="12.7" y2="-55.88" width="0.1524" layer="91"/>
<junction x="12.7" y="-55.88"/>
<pinref part="D37" gate="D1" pin="A"/>
<wire x1="10.16" y1="-43.18" x2="12.7" y2="-43.18" width="0.1524" layer="91"/>
<junction x="12.7" y="-43.18"/>
<junction x="12.7" y="-30.48"/>
<pinref part="0X72" gate="IC$1" pin="P0"/>
<wire x1="-35.56" y1="-17.78" x2="12.7" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D43" gate="D1" pin="A"/>
<wire x1="20.32" y1="-30.48" x2="22.86" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="D49" gate="D1" pin="A"/>
<wire x1="22.86" y1="-30.48" x2="22.86" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-43.18" x2="22.86" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-55.88" x2="22.86" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-68.58" x2="22.86" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-81.28" x2="22.86" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-93.98" x2="22.86" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-106.68" x2="20.32" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="D44" gate="D1" pin="A"/>
<wire x1="20.32" y1="-43.18" x2="22.86" y2="-43.18" width="0.1524" layer="91"/>
<junction x="22.86" y="-43.18"/>
<pinref part="D45" gate="D1" pin="A"/>
<wire x1="20.32" y1="-55.88" x2="22.86" y2="-55.88" width="0.1524" layer="91"/>
<junction x="22.86" y="-55.88"/>
<pinref part="D46" gate="D1" pin="A"/>
<wire x1="20.32" y1="-68.58" x2="22.86" y2="-68.58" width="0.1524" layer="91"/>
<junction x="22.86" y="-68.58"/>
<pinref part="D47" gate="D1" pin="A"/>
<wire x1="20.32" y1="-81.28" x2="22.86" y2="-81.28" width="0.1524" layer="91"/>
<junction x="22.86" y="-81.28"/>
<pinref part="D48" gate="D1" pin="A"/>
<wire x1="20.32" y1="-93.98" x2="22.86" y2="-93.98" width="0.1524" layer="91"/>
<junction x="22.86" y="-93.98"/>
<junction x="22.86" y="-30.48"/>
<pinref part="0X72" gate="IC$1" pin="P1"/>
<wire x1="-35.56" y1="-20.32" x2="22.86" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-20.32" x2="22.86" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="D50" gate="D1" pin="A"/>
<wire x1="30.48" y1="-30.48" x2="33.02" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="D56" gate="D1" pin="A"/>
<wire x1="33.02" y1="-30.48" x2="33.02" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-43.18" x2="33.02" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-55.88" x2="33.02" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-68.58" x2="33.02" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-81.28" x2="33.02" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-93.98" x2="33.02" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-106.68" x2="30.48" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="D55" gate="D1" pin="A"/>
<wire x1="30.48" y1="-93.98" x2="33.02" y2="-93.98" width="0.1524" layer="91"/>
<junction x="33.02" y="-93.98"/>
<pinref part="D54" gate="D1" pin="A"/>
<wire x1="30.48" y1="-81.28" x2="33.02" y2="-81.28" width="0.1524" layer="91"/>
<junction x="33.02" y="-81.28"/>
<pinref part="D53" gate="D1" pin="A"/>
<wire x1="30.48" y1="-68.58" x2="33.02" y2="-68.58" width="0.1524" layer="91"/>
<junction x="33.02" y="-68.58"/>
<pinref part="D52" gate="D1" pin="A"/>
<wire x1="30.48" y1="-55.88" x2="33.02" y2="-55.88" width="0.1524" layer="91"/>
<junction x="33.02" y="-55.88"/>
<pinref part="D51" gate="D1" pin="A"/>
<wire x1="30.48" y1="-43.18" x2="33.02" y2="-43.18" width="0.1524" layer="91"/>
<junction x="33.02" y="-43.18"/>
<junction x="33.02" y="-30.48"/>
<wire x1="33.02" y1="-22.86" x2="33.02" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="P2"/>
<wire x1="-35.56" y1="-22.86" x2="33.02" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="D57" gate="D1" pin="A"/>
<wire x1="40.64" y1="-30.48" x2="43.18" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-30.48" x2="43.18" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="D63" gate="D1" pin="A"/>
<wire x1="43.18" y1="-43.18" x2="43.18" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-55.88" x2="43.18" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-68.58" x2="43.18" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-81.28" x2="43.18" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-93.98" x2="43.18" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-106.68" x2="40.64" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="D58" gate="D1" pin="A"/>
<wire x1="40.64" y1="-43.18" x2="43.18" y2="-43.18" width="0.1524" layer="91"/>
<junction x="43.18" y="-43.18"/>
<pinref part="D59" gate="D1" pin="A"/>
<wire x1="40.64" y1="-55.88" x2="43.18" y2="-55.88" width="0.1524" layer="91"/>
<junction x="43.18" y="-55.88"/>
<pinref part="D60" gate="D1" pin="A"/>
<wire x1="40.64" y1="-68.58" x2="43.18" y2="-68.58" width="0.1524" layer="91"/>
<junction x="43.18" y="-68.58"/>
<pinref part="D61" gate="D1" pin="A"/>
<wire x1="40.64" y1="-81.28" x2="43.18" y2="-81.28" width="0.1524" layer="91"/>
<junction x="43.18" y="-81.28"/>
<pinref part="D62" gate="D1" pin="A"/>
<wire x1="40.64" y1="-93.98" x2="43.18" y2="-93.98" width="0.1524" layer="91"/>
<junction x="43.18" y="-93.98"/>
<junction x="43.18" y="-30.48"/>
<pinref part="0X72" gate="IC$1" pin="P3"/>
<wire x1="43.18" y1="-25.4" x2="43.18" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-25.4" x2="43.18" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="D64" gate="D1" pin="A"/>
<wire x1="50.8" y1="-30.48" x2="53.34" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="D70" gate="D1" pin="A"/>
<wire x1="53.34" y1="-30.48" x2="53.34" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-43.18" x2="53.34" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-55.88" x2="53.34" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-68.58" x2="53.34" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-81.28" x2="53.34" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-93.98" x2="53.34" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-106.68" x2="50.8" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="D65" gate="D1" pin="A"/>
<wire x1="50.8" y1="-43.18" x2="53.34" y2="-43.18" width="0.1524" layer="91"/>
<junction x="53.34" y="-43.18"/>
<pinref part="D66" gate="D1" pin="A"/>
<wire x1="50.8" y1="-55.88" x2="53.34" y2="-55.88" width="0.1524" layer="91"/>
<junction x="53.34" y="-55.88"/>
<pinref part="D67" gate="D1" pin="A"/>
<wire x1="50.8" y1="-68.58" x2="53.34" y2="-68.58" width="0.1524" layer="91"/>
<junction x="53.34" y="-68.58"/>
<pinref part="D68" gate="D1" pin="A"/>
<wire x1="50.8" y1="-81.28" x2="53.34" y2="-81.28" width="0.1524" layer="91"/>
<junction x="53.34" y="-81.28"/>
<pinref part="D69" gate="D1" pin="A"/>
<wire x1="50.8" y1="-93.98" x2="53.34" y2="-93.98" width="0.1524" layer="91"/>
<junction x="53.34" y="-93.98"/>
<junction x="53.34" y="-30.48"/>
<pinref part="0X72" gate="IC$1" pin="P4"/>
<wire x1="53.34" y1="-27.94" x2="53.34" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-27.94" x2="53.34" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="D36" gate="D1" pin="C"/>
<pinref part="D43" gate="D1" pin="C"/>
<wire x1="10.16" y1="-38.1" x2="20.32" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="D50" gate="D1" pin="C"/>
<wire x1="30.48" y1="-38.1" x2="20.32" y2="-38.1" width="0.1524" layer="91"/>
<junction x="20.32" y="-38.1"/>
<pinref part="D57" gate="D1" pin="C"/>
<wire x1="30.48" y1="-38.1" x2="40.64" y2="-38.1" width="0.1524" layer="91"/>
<junction x="30.48" y="-38.1"/>
<pinref part="D64" gate="D1" pin="C"/>
<wire x1="40.64" y1="-38.1" x2="50.8" y2="-38.1" width="0.1524" layer="91"/>
<junction x="40.64" y="-38.1"/>
<wire x1="10.16" y1="-38.1" x2="-2.54" y2="-38.1" width="0.1524" layer="91"/>
<junction x="10.16" y="-38.1"/>
<pinref part="R8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D37" gate="D1" pin="C"/>
<pinref part="D44" gate="D1" pin="C"/>
<wire x1="10.16" y1="-50.8" x2="20.32" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="D51" gate="D1" pin="C"/>
<wire x1="20.32" y1="-50.8" x2="30.48" y2="-50.8" width="0.1524" layer="91"/>
<junction x="20.32" y="-50.8"/>
<pinref part="D58" gate="D1" pin="C"/>
<wire x1="30.48" y1="-50.8" x2="40.64" y2="-50.8" width="0.1524" layer="91"/>
<junction x="30.48" y="-50.8"/>
<pinref part="D65" gate="D1" pin="C"/>
<wire x1="40.64" y1="-50.8" x2="50.8" y2="-50.8" width="0.1524" layer="91"/>
<junction x="40.64" y="-50.8"/>
<wire x1="10.16" y1="-50.8" x2="-2.54" y2="-50.8" width="0.1524" layer="91"/>
<junction x="10.16" y="-50.8"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D38" gate="D1" pin="C"/>
<pinref part="D45" gate="D1" pin="C"/>
<wire x1="10.16" y1="-63.5" x2="20.32" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="D52" gate="D1" pin="C"/>
<wire x1="20.32" y1="-63.5" x2="30.48" y2="-63.5" width="0.1524" layer="91"/>
<junction x="20.32" y="-63.5"/>
<pinref part="D59" gate="D1" pin="C"/>
<wire x1="30.48" y1="-63.5" x2="40.64" y2="-63.5" width="0.1524" layer="91"/>
<junction x="30.48" y="-63.5"/>
<pinref part="D66" gate="D1" pin="C"/>
<wire x1="40.64" y1="-63.5" x2="50.8" y2="-63.5" width="0.1524" layer="91"/>
<junction x="40.64" y="-63.5"/>
<wire x1="10.16" y1="-63.5" x2="-2.54" y2="-63.5" width="0.1524" layer="91"/>
<junction x="10.16" y="-63.5"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D67" gate="D1" pin="C"/>
<pinref part="D60" gate="D1" pin="C"/>
<wire x1="50.8" y1="-76.2" x2="40.64" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="D53" gate="D1" pin="C"/>
<wire x1="40.64" y1="-76.2" x2="30.48" y2="-76.2" width="0.1524" layer="91"/>
<junction x="40.64" y="-76.2"/>
<pinref part="D46" gate="D1" pin="C"/>
<wire x1="30.48" y1="-76.2" x2="20.32" y2="-76.2" width="0.1524" layer="91"/>
<junction x="30.48" y="-76.2"/>
<pinref part="D39" gate="D1" pin="C"/>
<wire x1="20.32" y1="-76.2" x2="10.16" y2="-76.2" width="0.1524" layer="91"/>
<junction x="20.32" y="-76.2"/>
<wire x1="10.16" y1="-76.2" x2="-2.54" y2="-76.2" width="0.1524" layer="91"/>
<junction x="10.16" y="-76.2"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="D40" gate="D1" pin="C"/>
<pinref part="D47" gate="D1" pin="C"/>
<wire x1="10.16" y1="-88.9" x2="20.32" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="D54" gate="D1" pin="C"/>
<wire x1="20.32" y1="-88.9" x2="30.48" y2="-88.9" width="0.1524" layer="91"/>
<junction x="20.32" y="-88.9"/>
<pinref part="D61" gate="D1" pin="C"/>
<wire x1="30.48" y1="-88.9" x2="40.64" y2="-88.9" width="0.1524" layer="91"/>
<junction x="30.48" y="-88.9"/>
<pinref part="D68" gate="D1" pin="C"/>
<wire x1="40.64" y1="-88.9" x2="50.8" y2="-88.9" width="0.1524" layer="91"/>
<junction x="40.64" y="-88.9"/>
<wire x1="10.16" y1="-88.9" x2="-2.54" y2="-88.9" width="0.1524" layer="91"/>
<junction x="10.16" y="-88.9"/>
<pinref part="R12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D41" gate="D1" pin="C"/>
<pinref part="D48" gate="D1" pin="C"/>
<wire x1="10.16" y1="-101.6" x2="20.32" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="D55" gate="D1" pin="C"/>
<wire x1="20.32" y1="-101.6" x2="30.48" y2="-101.6" width="0.1524" layer="91"/>
<junction x="20.32" y="-101.6"/>
<pinref part="D62" gate="D1" pin="C"/>
<wire x1="30.48" y1="-101.6" x2="40.64" y2="-101.6" width="0.1524" layer="91"/>
<junction x="30.48" y="-101.6"/>
<pinref part="D69" gate="D1" pin="C"/>
<wire x1="40.64" y1="-101.6" x2="50.8" y2="-101.6" width="0.1524" layer="91"/>
<junction x="40.64" y="-101.6"/>
<wire x1="10.16" y1="-101.6" x2="-2.54" y2="-101.6" width="0.1524" layer="91"/>
<junction x="10.16" y="-101.6"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="D42" gate="D1" pin="C"/>
<pinref part="D49" gate="D1" pin="C"/>
<wire x1="10.16" y1="-114.3" x2="20.32" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="D56" gate="D1" pin="C"/>
<wire x1="20.32" y1="-114.3" x2="30.48" y2="-114.3" width="0.1524" layer="91"/>
<junction x="20.32" y="-114.3"/>
<pinref part="D63" gate="D1" pin="C"/>
<wire x1="30.48" y1="-114.3" x2="40.64" y2="-114.3" width="0.1524" layer="91"/>
<junction x="30.48" y="-114.3"/>
<pinref part="D70" gate="D1" pin="C"/>
<wire x1="40.64" y1="-114.3" x2="50.8" y2="-114.3" width="0.1524" layer="91"/>
<junction x="40.64" y="-114.3"/>
<wire x1="10.16" y1="-114.3" x2="-2.54" y2="-114.3" width="0.1524" layer="91"/>
<junction x="10.16" y="-114.3"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-86.36" y1="-96.52" x2="-86.36" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="-86.36" x2="-86.36" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-86.36" x2="-86.36" y2="-86.36" width="0.1524" layer="91"/>
<junction x="-86.36" y="-86.36"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="-86.36" x2="-76.2" y2="-86.36" width="0.1524" layer="91"/>
<junction x="-81.28" y="-86.36"/>
</segment>
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<pinref part="0X70" gate="PWR" pin="VCC"/>
<pinref part="0X72" gate="PWR" pin="VCC"/>
<wire x1="-43.18" y1="-66.04" x2="-50.8" y2="-66.04" width="0.1524" layer="91"/>
<junction x="-50.8" y="-66.04"/>
</segment>
<segment>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<pinref part="0X72" gate="IC$1" pin="A1"/>
<wire x1="-66.04" y1="-25.4" x2="-55.88" y2="-25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="4"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="96.52" y1="-99.06" x2="96.52" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-88.9" x2="96.52" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-88.9" x2="96.52" y2="-88.9" width="0.1524" layer="91"/>
<junction x="96.52" y="-88.9"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="101.6" y1="-88.9" x2="106.68" y2="-88.9" width="0.1524" layer="91"/>
<junction x="101.6" y="-88.9"/>
</segment>
<segment>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<pinref part="0X2" gate="PWR" pin="VCC"/>
<pinref part="0X1" gate="PWR" pin="VCC"/>
<wire x1="142.24" y1="-71.12" x2="134.62" y2="-71.12" width="0.1524" layer="91"/>
<junction x="134.62" y="-71.12"/>
</segment>
<segment>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<pinref part="0X1" gate="IC$1" pin="A1"/>
<wire x1="104.14" y1="-27.94" x2="114.3" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="1" pin="VS"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="137.16" y1="-40.64" x2="137.16" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-86.36" y1="-109.22" x2="-86.36" y2="-104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="0X70" gate="PWR" pin="GND"/>
<pinref part="0X72" gate="PWR" pin="GND"/>
<wire x1="-50.8" y1="-81.28" x2="-43.18" y2="-81.28" width="0.1524" layer="91"/>
<junction x="-50.8" y="-81.28"/>
</segment>
<segment>
<pinref part="0X72" gate="IC$1" pin="A2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-55.88" y1="-27.94" x2="-63.5" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="0X72" gate="IC$1" pin="A0"/>
<wire x1="-55.88" y1="-22.86" x2="-63.5" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-22.86" x2="-63.5" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-63.5" y="-27.94"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="0X70" gate="IC$1" pin="A2"/>
<wire x1="-63.5" y1="-109.22" x2="-55.88" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="A1"/>
<wire x1="-55.88" y1="-106.68" x2="-55.88" y2="-109.22" width="0.1524" layer="91"/>
<junction x="-55.88" y="-109.22"/>
<pinref part="0X70" gate="IC$1" pin="A0"/>
<wire x1="-55.88" y1="-106.68" x2="-55.88" y2="-104.14" width="0.1524" layer="91"/>
<junction x="-55.88" y="-106.68"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="96.52" y1="-111.76" x2="96.52" y2="-106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="0X2" gate="PWR" pin="GND"/>
<pinref part="0X1" gate="PWR" pin="GND"/>
<wire x1="134.62" y1="-86.36" x2="142.24" y2="-86.36" width="0.1524" layer="91"/>
<junction x="134.62" y="-86.36"/>
</segment>
<segment>
<pinref part="0X1" gate="IC$1" pin="A2"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="114.3" y1="-30.48" x2="106.68" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="0X1" gate="IC$1" pin="A0"/>
<wire x1="114.3" y1="-25.4" x2="106.68" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-25.4" x2="106.68" y2="-30.48" width="0.1524" layer="91"/>
<junction x="106.68" y="-30.48"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="0X2" gate="IC$1" pin="A2"/>
<wire x1="121.92" y1="-111.76" x2="129.54" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="0X2" gate="IC$1" pin="A1"/>
<wire x1="129.54" y1="-109.22" x2="129.54" y2="-111.76" width="0.1524" layer="91"/>
<junction x="129.54" y="-111.76"/>
<pinref part="0X2" gate="IC$1" pin="A0"/>
<wire x1="129.54" y1="-109.22" x2="129.54" y2="-106.68" width="0.1524" layer="91"/>
<junction x="129.54" y="-109.22"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="IC1" gate="1" pin="GND"/>
<wire x1="162.56" y1="-43.18" x2="162.56" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="-86.36" y1="-99.06" x2="-81.28" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="-99.06" x2="-81.28" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-99.06" x2="-71.12" y2="-99.06" width="0.1524" layer="91"/>
<junction x="-81.28" y="-99.06"/>
<wire x1="-71.12" y1="-99.06" x2="-71.12" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="SDA"/>
<wire x1="-71.12" y1="-116.84" x2="-55.88" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-99.06" x2="-71.12" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-71.12" y="-99.06"/>
<pinref part="0X72" gate="IC$1" pin="SDA"/>
<wire x1="-71.12" y1="-35.56" x2="-55.88" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="-86.36" y1="-101.6" x2="-76.2" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-76.2" y1="-101.6" x2="-76.2" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-101.6" x2="-68.58" y2="-101.6" width="0.1524" layer="91"/>
<junction x="-76.2" y="-101.6"/>
<wire x1="-68.58" y1="-101.6" x2="-68.58" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="0X70" gate="IC$1" pin="SCL"/>
<wire x1="-68.58" y1="-114.3" x2="-55.88" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-101.6" x2="-68.58" y2="-33.02" width="0.1524" layer="91"/>
<junction x="-68.58" y="-101.6"/>
<pinref part="0X72" gate="IC$1" pin="SCL"/>
<wire x1="-68.58" y1="-33.02" x2="-55.88" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P6"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="-114.3" x2="-12.7" y2="-114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P5"/>
<wire x1="-35.56" y1="-111.76" x2="-12.7" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-111.76" x2="-12.7" y2="-101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P4"/>
<wire x1="-35.56" y1="-109.22" x2="-15.24" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-109.22" x2="-15.24" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="-88.9" x2="-12.7" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P3"/>
<wire x1="-35.56" y1="-106.68" x2="-17.78" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-106.68" x2="-17.78" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-76.2" x2="-12.7" y2="-76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P2"/>
<wire x1="-35.56" y1="-104.14" x2="-20.32" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-104.14" x2="-20.32" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-63.5" x2="-12.7" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P1"/>
<wire x1="-35.56" y1="-101.6" x2="-22.86" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="-101.6" x2="-22.86" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-50.8" x2="-12.7" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="0X70" gate="IC$1" pin="P0"/>
<wire x1="-35.56" y1="-99.06" x2="-25.4" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-99.06" x2="-25.4" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="-38.1" x2="-12.7" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="D71" gate="D1" pin="A"/>
<wire x1="195.58" y1="-33.02" x2="198.12" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D77" gate="D1" pin="A"/>
<wire x1="198.12" y1="-33.02" x2="198.12" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-45.72" x2="198.12" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-58.42" x2="198.12" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-71.12" x2="198.12" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-83.82" x2="198.12" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-96.52" x2="198.12" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-109.22" x2="195.58" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="D76" gate="D1" pin="A"/>
<wire x1="195.58" y1="-96.52" x2="198.12" y2="-96.52" width="0.1524" layer="91"/>
<junction x="198.12" y="-96.52"/>
<pinref part="D75" gate="D1" pin="A"/>
<wire x1="195.58" y1="-83.82" x2="198.12" y2="-83.82" width="0.1524" layer="91"/>
<junction x="198.12" y="-83.82"/>
<pinref part="D74" gate="D1" pin="A"/>
<wire x1="195.58" y1="-71.12" x2="198.12" y2="-71.12" width="0.1524" layer="91"/>
<junction x="198.12" y="-71.12"/>
<pinref part="D73" gate="D1" pin="A"/>
<wire x1="195.58" y1="-58.42" x2="198.12" y2="-58.42" width="0.1524" layer="91"/>
<junction x="198.12" y="-58.42"/>
<pinref part="D72" gate="D1" pin="A"/>
<wire x1="195.58" y1="-45.72" x2="198.12" y2="-45.72" width="0.1524" layer="91"/>
<junction x="198.12" y="-45.72"/>
<junction x="198.12" y="-33.02"/>
<wire x1="198.12" y1="-20.32" x2="198.12" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="O1"/>
<wire x1="162.56" y1="-20.32" x2="198.12" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="D78" gate="D1" pin="A"/>
<wire x1="205.74" y1="-33.02" x2="208.28" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D84" gate="D1" pin="A"/>
<wire x1="208.28" y1="-33.02" x2="208.28" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-45.72" x2="208.28" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-58.42" x2="208.28" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-71.12" x2="208.28" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-83.82" x2="208.28" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-96.52" x2="208.28" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="208.28" y1="-109.22" x2="205.74" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="D79" gate="D1" pin="A"/>
<wire x1="205.74" y1="-45.72" x2="208.28" y2="-45.72" width="0.1524" layer="91"/>
<junction x="208.28" y="-45.72"/>
<pinref part="D80" gate="D1" pin="A"/>
<wire x1="205.74" y1="-58.42" x2="208.28" y2="-58.42" width="0.1524" layer="91"/>
<junction x="208.28" y="-58.42"/>
<pinref part="D81" gate="D1" pin="A"/>
<wire x1="205.74" y1="-71.12" x2="208.28" y2="-71.12" width="0.1524" layer="91"/>
<junction x="208.28" y="-71.12"/>
<pinref part="D82" gate="D1" pin="A"/>
<wire x1="205.74" y1="-83.82" x2="208.28" y2="-83.82" width="0.1524" layer="91"/>
<junction x="208.28" y="-83.82"/>
<pinref part="D83" gate="D1" pin="A"/>
<wire x1="205.74" y1="-96.52" x2="208.28" y2="-96.52" width="0.1524" layer="91"/>
<junction x="208.28" y="-96.52"/>
<junction x="208.28" y="-33.02"/>
<wire x1="208.28" y1="-22.86" x2="208.28" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="O2"/>
<wire x1="162.56" y1="-22.86" x2="208.28" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D85" gate="D1" pin="A"/>
<wire x1="215.9" y1="-33.02" x2="218.44" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D91" gate="D1" pin="A"/>
<wire x1="218.44" y1="-33.02" x2="218.44" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-45.72" x2="218.44" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-58.42" x2="218.44" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-71.12" x2="218.44" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-83.82" x2="218.44" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-96.52" x2="218.44" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-109.22" x2="215.9" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="D90" gate="D1" pin="A"/>
<wire x1="215.9" y1="-96.52" x2="218.44" y2="-96.52" width="0.1524" layer="91"/>
<junction x="218.44" y="-96.52"/>
<pinref part="D89" gate="D1" pin="A"/>
<wire x1="215.9" y1="-83.82" x2="218.44" y2="-83.82" width="0.1524" layer="91"/>
<junction x="218.44" y="-83.82"/>
<pinref part="D88" gate="D1" pin="A"/>
<wire x1="215.9" y1="-71.12" x2="218.44" y2="-71.12" width="0.1524" layer="91"/>
<junction x="218.44" y="-71.12"/>
<pinref part="D87" gate="D1" pin="A"/>
<wire x1="215.9" y1="-58.42" x2="218.44" y2="-58.42" width="0.1524" layer="91"/>
<junction x="218.44" y="-58.42"/>
<pinref part="D86" gate="D1" pin="A"/>
<wire x1="215.9" y1="-45.72" x2="218.44" y2="-45.72" width="0.1524" layer="91"/>
<junction x="218.44" y="-45.72"/>
<junction x="218.44" y="-33.02"/>
<wire x1="218.44" y1="-25.4" x2="218.44" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="O3"/>
<wire x1="162.56" y1="-25.4" x2="218.44" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="D92" gate="D1" pin="A"/>
<wire x1="226.06" y1="-33.02" x2="228.6" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-33.02" x2="228.6" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="D98" gate="D1" pin="A"/>
<wire x1="228.6" y1="-45.72" x2="228.6" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-58.42" x2="228.6" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-71.12" x2="228.6" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-83.82" x2="228.6" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-96.52" x2="228.6" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-109.22" x2="226.06" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="D93" gate="D1" pin="A"/>
<wire x1="226.06" y1="-45.72" x2="228.6" y2="-45.72" width="0.1524" layer="91"/>
<junction x="228.6" y="-45.72"/>
<pinref part="D94" gate="D1" pin="A"/>
<wire x1="226.06" y1="-58.42" x2="228.6" y2="-58.42" width="0.1524" layer="91"/>
<junction x="228.6" y="-58.42"/>
<pinref part="D95" gate="D1" pin="A"/>
<wire x1="226.06" y1="-71.12" x2="228.6" y2="-71.12" width="0.1524" layer="91"/>
<junction x="228.6" y="-71.12"/>
<pinref part="D96" gate="D1" pin="A"/>
<wire x1="226.06" y1="-83.82" x2="228.6" y2="-83.82" width="0.1524" layer="91"/>
<junction x="228.6" y="-83.82"/>
<pinref part="D97" gate="D1" pin="A"/>
<wire x1="226.06" y1="-96.52" x2="228.6" y2="-96.52" width="0.1524" layer="91"/>
<junction x="228.6" y="-96.52"/>
<junction x="228.6" y="-33.02"/>
<wire x1="228.6" y1="-27.94" x2="228.6" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="O4"/>
<wire x1="162.56" y1="-27.94" x2="228.6" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="D99" gate="D1" pin="A"/>
<wire x1="236.22" y1="-33.02" x2="238.76" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="D105" gate="D1" pin="A"/>
<wire x1="238.76" y1="-33.02" x2="238.76" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-45.72" x2="238.76" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-58.42" x2="238.76" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-71.12" x2="238.76" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-83.82" x2="238.76" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-96.52" x2="238.76" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-109.22" x2="236.22" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="D100" gate="D1" pin="A"/>
<wire x1="236.22" y1="-45.72" x2="238.76" y2="-45.72" width="0.1524" layer="91"/>
<junction x="238.76" y="-45.72"/>
<pinref part="D101" gate="D1" pin="A"/>
<wire x1="236.22" y1="-58.42" x2="238.76" y2="-58.42" width="0.1524" layer="91"/>
<junction x="238.76" y="-58.42"/>
<pinref part="D102" gate="D1" pin="A"/>
<wire x1="236.22" y1="-71.12" x2="238.76" y2="-71.12" width="0.1524" layer="91"/>
<junction x="238.76" y="-71.12"/>
<pinref part="D103" gate="D1" pin="A"/>
<wire x1="236.22" y1="-83.82" x2="238.76" y2="-83.82" width="0.1524" layer="91"/>
<junction x="238.76" y="-83.82"/>
<pinref part="D104" gate="D1" pin="A"/>
<wire x1="236.22" y1="-96.52" x2="238.76" y2="-96.52" width="0.1524" layer="91"/>
<junction x="238.76" y="-96.52"/>
<junction x="238.76" y="-33.02"/>
<wire x1="238.76" y1="-30.48" x2="238.76" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="1" pin="O5"/>
<wire x1="162.56" y1="-30.48" x2="238.76" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="D71" gate="D1" pin="C"/>
<pinref part="D78" gate="D1" pin="C"/>
<wire x1="195.58" y1="-40.64" x2="205.74" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="D85" gate="D1" pin="C"/>
<wire x1="215.9" y1="-40.64" x2="205.74" y2="-40.64" width="0.1524" layer="91"/>
<junction x="205.74" y="-40.64"/>
<pinref part="D92" gate="D1" pin="C"/>
<wire x1="215.9" y1="-40.64" x2="226.06" y2="-40.64" width="0.1524" layer="91"/>
<junction x="215.9" y="-40.64"/>
<pinref part="D99" gate="D1" pin="C"/>
<wire x1="226.06" y1="-40.64" x2="236.22" y2="-40.64" width="0.1524" layer="91"/>
<junction x="226.06" y="-40.64"/>
<wire x1="195.58" y1="-40.64" x2="182.88" y2="-40.64" width="0.1524" layer="91"/>
<junction x="195.58" y="-40.64"/>
<pinref part="R17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="D72" gate="D1" pin="C"/>
<pinref part="D79" gate="D1" pin="C"/>
<wire x1="195.58" y1="-53.34" x2="205.74" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="D86" gate="D1" pin="C"/>
<wire x1="205.74" y1="-53.34" x2="215.9" y2="-53.34" width="0.1524" layer="91"/>
<junction x="205.74" y="-53.34"/>
<pinref part="D93" gate="D1" pin="C"/>
<wire x1="215.9" y1="-53.34" x2="226.06" y2="-53.34" width="0.1524" layer="91"/>
<junction x="215.9" y="-53.34"/>
<pinref part="D100" gate="D1" pin="C"/>
<wire x1="226.06" y1="-53.34" x2="236.22" y2="-53.34" width="0.1524" layer="91"/>
<junction x="226.06" y="-53.34"/>
<wire x1="195.58" y1="-53.34" x2="182.88" y2="-53.34" width="0.1524" layer="91"/>
<junction x="195.58" y="-53.34"/>
<pinref part="R18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="D73" gate="D1" pin="C"/>
<pinref part="D80" gate="D1" pin="C"/>
<wire x1="195.58" y1="-66.04" x2="205.74" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="D87" gate="D1" pin="C"/>
<wire x1="205.74" y1="-66.04" x2="215.9" y2="-66.04" width="0.1524" layer="91"/>
<junction x="205.74" y="-66.04"/>
<pinref part="D94" gate="D1" pin="C"/>
<wire x1="215.9" y1="-66.04" x2="226.06" y2="-66.04" width="0.1524" layer="91"/>
<junction x="215.9" y="-66.04"/>
<pinref part="D101" gate="D1" pin="C"/>
<wire x1="226.06" y1="-66.04" x2="236.22" y2="-66.04" width="0.1524" layer="91"/>
<junction x="226.06" y="-66.04"/>
<wire x1="195.58" y1="-66.04" x2="182.88" y2="-66.04" width="0.1524" layer="91"/>
<junction x="195.58" y="-66.04"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="D102" gate="D1" pin="C"/>
<pinref part="D95" gate="D1" pin="C"/>
<wire x1="236.22" y1="-78.74" x2="226.06" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="D88" gate="D1" pin="C"/>
<wire x1="226.06" y1="-78.74" x2="215.9" y2="-78.74" width="0.1524" layer="91"/>
<junction x="226.06" y="-78.74"/>
<pinref part="D81" gate="D1" pin="C"/>
<wire x1="215.9" y1="-78.74" x2="205.74" y2="-78.74" width="0.1524" layer="91"/>
<junction x="215.9" y="-78.74"/>
<pinref part="D74" gate="D1" pin="C"/>
<wire x1="205.74" y1="-78.74" x2="195.58" y2="-78.74" width="0.1524" layer="91"/>
<junction x="205.74" y="-78.74"/>
<wire x1="195.58" y1="-78.74" x2="182.88" y2="-78.74" width="0.1524" layer="91"/>
<junction x="195.58" y="-78.74"/>
<pinref part="R20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="D75" gate="D1" pin="C"/>
<pinref part="D82" gate="D1" pin="C"/>
<wire x1="195.58" y1="-91.44" x2="205.74" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="D89" gate="D1" pin="C"/>
<wire x1="205.74" y1="-91.44" x2="215.9" y2="-91.44" width="0.1524" layer="91"/>
<junction x="205.74" y="-91.44"/>
<pinref part="D96" gate="D1" pin="C"/>
<wire x1="215.9" y1="-91.44" x2="226.06" y2="-91.44" width="0.1524" layer="91"/>
<junction x="215.9" y="-91.44"/>
<pinref part="D103" gate="D1" pin="C"/>
<wire x1="226.06" y1="-91.44" x2="236.22" y2="-91.44" width="0.1524" layer="91"/>
<junction x="226.06" y="-91.44"/>
<wire x1="195.58" y1="-91.44" x2="182.88" y2="-91.44" width="0.1524" layer="91"/>
<junction x="195.58" y="-91.44"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="D76" gate="D1" pin="C"/>
<pinref part="D83" gate="D1" pin="C"/>
<wire x1="195.58" y1="-104.14" x2="205.74" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="D90" gate="D1" pin="C"/>
<wire x1="205.74" y1="-104.14" x2="215.9" y2="-104.14" width="0.1524" layer="91"/>
<junction x="205.74" y="-104.14"/>
<pinref part="D97" gate="D1" pin="C"/>
<wire x1="215.9" y1="-104.14" x2="226.06" y2="-104.14" width="0.1524" layer="91"/>
<junction x="215.9" y="-104.14"/>
<pinref part="D104" gate="D1" pin="C"/>
<wire x1="226.06" y1="-104.14" x2="236.22" y2="-104.14" width="0.1524" layer="91"/>
<junction x="226.06" y="-104.14"/>
<wire x1="195.58" y1="-104.14" x2="182.88" y2="-104.14" width="0.1524" layer="91"/>
<junction x="195.58" y="-104.14"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="D77" gate="D1" pin="C"/>
<pinref part="D84" gate="D1" pin="C"/>
<wire x1="195.58" y1="-116.84" x2="205.74" y2="-116.84" width="0.1524" layer="91"/>
<pinref part="D91" gate="D1" pin="C"/>
<wire x1="205.74" y1="-116.84" x2="215.9" y2="-116.84" width="0.1524" layer="91"/>
<junction x="205.74" y="-116.84"/>
<pinref part="D98" gate="D1" pin="C"/>
<wire x1="215.9" y1="-116.84" x2="226.06" y2="-116.84" width="0.1524" layer="91"/>
<junction x="215.9" y="-116.84"/>
<pinref part="D105" gate="D1" pin="C"/>
<wire x1="226.06" y1="-116.84" x2="236.22" y2="-116.84" width="0.1524" layer="91"/>
<junction x="226.06" y="-116.84"/>
<wire x1="195.58" y1="-116.84" x2="182.88" y2="-116.84" width="0.1524" layer="91"/>
<junction x="195.58" y="-116.84"/>
<pinref part="R23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="3"/>
<wire x1="96.52" y1="-101.6" x2="101.6" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-101.6" x2="101.6" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-101.6" x2="114.3" y2="-101.6" width="0.1524" layer="91"/>
<junction x="101.6" y="-101.6"/>
<wire x1="114.3" y1="-101.6" x2="114.3" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="0X2" gate="IC$1" pin="SDA"/>
<wire x1="114.3" y1="-119.38" x2="129.54" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-101.6" x2="114.3" y2="-38.1" width="0.1524" layer="91"/>
<junction x="114.3" y="-101.6"/>
<pinref part="0X1" gate="IC$1" pin="SDA"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-104.14" x2="106.68" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="106.68" y1="-104.14" x2="106.68" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-104.14" x2="111.76" y2="-104.14" width="0.1524" layer="91"/>
<junction x="106.68" y="-104.14"/>
<pinref part="0X2" gate="IC$1" pin="SCL"/>
<wire x1="111.76" y1="-116.84" x2="129.54" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-104.14" x2="111.76" y2="-35.56" width="0.1524" layer="91"/>
<junction x="111.76" y="-104.14"/>
<pinref part="0X1" gate="IC$1" pin="SCL"/>
<wire x1="111.76" y1="-35.56" x2="114.3" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-104.14" x2="111.76" y2="-116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P6"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="149.86" y1="-116.84" x2="172.72" y2="-116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P5"/>
<wire x1="149.86" y1="-114.3" x2="172.72" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="172.72" y1="-114.3" x2="172.72" y2="-104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P4"/>
<wire x1="149.86" y1="-111.76" x2="170.18" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-111.76" x2="170.18" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="170.18" y1="-91.44" x2="172.72" y2="-91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P3"/>
<wire x1="149.86" y1="-109.22" x2="167.64" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-109.22" x2="167.64" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="167.64" y1="-78.74" x2="172.72" y2="-78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P2"/>
<wire x1="149.86" y1="-106.68" x2="165.1" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-106.68" x2="165.1" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-66.04" x2="172.72" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P1"/>
<wire x1="149.86" y1="-104.14" x2="162.56" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="162.56" y1="-104.14" x2="162.56" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-53.34" x2="172.72" y2="-53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="0X2" gate="IC$1" pin="P0"/>
<wire x1="149.86" y1="-101.6" x2="160.02" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-101.6" x2="160.02" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-50.8" x2="172.72" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="172.72" y1="-50.8" x2="172.72" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="0X1" gate="IC$1" pin="P0"/>
<pinref part="IC1" gate="1" pin="I1"/>
<wire x1="134.62" y1="-20.32" x2="137.16" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="0X1" gate="IC$1" pin="P1"/>
<pinref part="IC1" gate="1" pin="I2"/>
<wire x1="134.62" y1="-22.86" x2="137.16" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="0X1" gate="IC$1" pin="P2"/>
<pinref part="IC1" gate="1" pin="I3"/>
<wire x1="134.62" y1="-25.4" x2="137.16" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="0X1" gate="IC$1" pin="P3"/>
<pinref part="IC1" gate="1" pin="I4"/>
<wire x1="134.62" y1="-27.94" x2="137.16" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="0X1" gate="IC$1" pin="P4"/>
<pinref part="IC1" gate="1" pin="I5"/>
<wire x1="134.62" y1="-30.48" x2="137.16" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
